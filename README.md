# Christel

Christel is a software application with the purpose to help radio amateurs
with the design and implementation of crystal filters. It's aim is to automate
the process from measurement of crystal parameters to their selection for
use in filters. It is able to measure crystals using a miniVNA Pro and store
their characteristics on disk. The characteristics of multiple crystals
can be organized in so called ``stocks''. With manual or automatic selection
an optimum of christal combinations can be choosen to design an optimal crystal
filter.
![ChristelScreenshot](Screenshots/Screenshot_2022_02_15.png "Screenshot")

## Project status
Work in progress: not ready for production use.

## Dependencies
A provisional (library) dependency list:
- glibmm
- gtkmm
- pango
- cairo
- pangopairo
- sigc++
- uuid
- lmdb
- cuda
- java runtime environment
- the vnaJ command line tool (vnaJ-hl.3.3.3.jar)

## Build
For building you need
- the dev packages of the items in the above dependencies list
- gcc
- g++
- NVIDIAs' cuda compiler
- cmake
- ninja

The vnaJ command line tool (vnaJ-hl.3.3.3.jar) is automatically downloaded
during build. The build configuration that uses cmake allows for building
outside the source directory.

Configure with:
`$ cmake -G Ninja -DCMAKE_BUILD_TYPE=Release [your source directory]`

Build with:
`$ ninja -j8`

Run from within the top level build directory
`$ Gui/christel-gui`

## Authors and acknowledgment
P. de Jonge (PA1PDY)

## License
Christel is released under the terms of the GNU General Public License,
version 2, at your option, any later version, as published by the Free
Software Foundation. Please, see the COPYING file for further information.

