/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef CHRISTEL_CUDA_H
#define CHRISTEL_CUDA_H


#define CHRISTEL_CUDA_PI 3.141592653589793

/**
 *  @file
 *  @version 0.0.1
 *  @date    March 2018
 *  @author  Peter de Jonge <peter.dejonge@matroos.nu>
 *  @brief  
 */
extern "C" unsigned int christelCudaComputeCrystalImpedanceVsFrequency (double        L_m,
									double        C_m,
									double        R_m,
									double        C_p,
									double*       frequencies,
									unsigned int  n_frequencies,
									double**      impedances) ;


extern "C" unsigned int  christelCudaComputeCrystalTransmissionLoss (double**     computed_losses,
								     unsigned int n_samples,
								     double*      impedances,
								     double       system_impedance) ;


extern "C" double christelCudaComputeCumulativeLossCurveDiff (unsigned int n_samples,
							      double*      measured_losses,
							      double*      computed_losses) ;

/**
 *  @file
 *  @version 0.0.1
 *  @date    March 2018
 *  @author  Peter de Jonge <peter.dejonge@matroos.nu>
 *  @brief  
 */
/* extern "C" void christel_cuda_compute_optimum_crystal_filter_sets (unsigned int* Ids, */
/* 								   double*       L_ms, */
/* 								   double*       C_ms, */
/* 								   double*       R_ms, */
/* 								   double*       C_ps, */
/* 								   unsigned int  n_crystals, */
/* 								   unsigned int  n_crystals_in_filter, */
/* 								   double        filter_bandwidth) ; */


#endif // CHRISTEL_CUDA_H
