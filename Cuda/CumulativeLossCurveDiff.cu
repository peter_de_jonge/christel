/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include "Cuda.h"
#include <math.h>
#include <stdio.h>
#include <iostream>

#define BLOCKSIZE 256

// Source: https://developer.download.nvidia.com/assets/cuda/files/reduction.pdf


/*****************************************************************************
 *  Function: _christelCudaDiffReduce
 ****************************************************************************/
template <unsigned int blockSize>
__device__ void warpReduce(volatile double *sdata, unsigned int tid) {
  if (blockSize >= 64) sdata[tid] += sdata[tid + 32];
  if (blockSize >= 32) sdata[tid] += sdata[tid + 16];
  if (blockSize >= 16) sdata[tid] += sdata[tid + 8];
  if (blockSize >= 8) sdata[tid] += sdata[tid + 4];
  if (blockSize >= 4) sdata[tid] += sdata[tid + 2];
  if (blockSize >= 2) sdata[tid] += sdata[tid + 1];
}

/*****************************************************************************
 *  Function: _christelCudaComputeCumulativeLossCurveDiff
 ****************************************************************************/
template <unsigned int blockSize>
__global__ void _christelCudaComputeCumulativeLossCurveDiff (double*      measured_losses,
							     double*      computed_losses,
							     double*      work_data,
							     unsigned int n_samples) {
  extern __shared__ double sdata[];
  unsigned int tid      = threadIdx.x ;
  unsigned int i        = blockIdx.x * (blockSize*2) + tid ;
  sdata[tid] = 0;
  while (i < n_samples) {
    sdata[tid] += (measured_losses[i] - measured_losses[i]) + (measured_losses[i + blockSize] - measured_losses[i + blockSize]) ;
  }
  __syncthreads();
  if (blockSize >= 512) { if (tid < 256) { sdata[tid] += sdata[tid + 256]; } __syncthreads(); }
  if (blockSize >= 256) { if (tid < 128) { sdata[tid] += sdata[tid + 128]; } __syncthreads(); }
  if (blockSize >= 128) { if (tid < 64) { sdata[tid] += sdata[tid + 64]; } __syncthreads(); }
  if (tid < 32) warpReduce<blockSize>(sdata, tid);
  if (tid == 0) work_data[blockIdx.x] = sdata[0] ;
}		  


/*****************************************************************************
 *  Function: christelCudaComputeCumulativeLossCurveDiff
 ****************************************************************************/
double christelCudaComputeCumulativeLossCurveDiff (unsigned int n_samples,
						   double*      measured_losses,
						   double*      computed_losses) {

  unsigned int  block_size                 = BLOCKSIZE ;
  unsigned int  grid_size                  = (int) ceil ((double)n_samples / (double) block_size) ;
  unsigned int  measured_losses_size       = n_samples * sizeof(double) ;
  unsigned int  computed_losses_size       = measured_losses_size ;
  unsigned int  work_data_size             = measured_losses_size ;
  double        cumulative_loss_curve_diff = 0 ;
  double*       d_measured_losses ;
  double*       d_computed_losses ;
  double*       d_work_data ;

  // Allocate host memory ;
  double* work_data = (double*) malloc (grid_size * sizeof (double)) ;

  // Allocate device memory ;
  cudaMalloc (&d_measured_losses, measured_losses_size) ;
  cudaMalloc (&d_computed_losses, computed_losses_size) ;
  cudaMalloc (&d_work_data, work_data_size) ;
  
  // Copy vectors from host memory to device memory
  cudaMemcpy (d_measured_losses,  measured_losses, measured_losses_size,  cudaMemcpyHostToDevice) ;
  cudaMemcpy (d_computed_losses,  computed_losses, computed_losses_size,  cudaMemcpyHostToDevice) ;

  // Invoke kernel
  _christelCudaComputeCumulativeLossCurveDiff<BLOCKSIZE><<<grid_size,block_size >>> (d_measured_losses,
										     d_computed_losses,
										     d_work_data,
										     n_samples) ;
									  
  
  cudaMemcpy (work_data, d_work_data, grid_size * sizeof(double), cudaMemcpyDeviceToHost);

  for(int i = 0; i < grid_size; i++) {
    printf ("%f\n", work_data[i]) ;
    cumulative_loss_curve_diff += work_data[i] ;
  }
  
  // Free device memory
  cudaFree (d_measured_losses) ;
  cudaFree (d_computed_losses) ;
  cudaFree (d_work_data) ;

  return n_samples ;
}

