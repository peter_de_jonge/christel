/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include "Cuda.h"
#include <math.h>
#include <stdio.h>
#include <iostream>

/*****************************************************************************
 *  Function: christelCudaComputeCrystalTransmissionLoss
 ****************************************************************************/
__global__
void _christelCudaComputeCrystalTransmissionLoss (double*      computed_losses,
						  unsigned int n_samples,
						  double*      impedances,
						  double       system_impedance) {
  int frequency_index = blockIdx.x * blockDim.x + threadIdx.x ;
  int impedance_index = frequency_index * 2 ;
  double X_X          = impedances[impedance_index + 1] ;
  double R_X          = impedances[impedance_index] ;
  double Z_S          = system_impedance ;
  double numerator_real ; // teller in Dutch
  double numerator_imag ; // teller in Dutch
  double denominator ;    // noemer in Dutch

  if (frequency_index < n_samples) {
    numerator_real = Z_S * (Z_S + R_X) ;
    numerator_imag = - Z_S * X_X ;
    denominator = pow((Z_S + R_X),2) + pow (X_X,2) ;
    computed_losses[frequency_index] = 20 * log10 (sqrt (pow((numerator_real / denominator),2) + pow((numerator_imag / denominator),2))) ;
  }
}
/*****************************************************************************
 *  Function: christelCudaComputeCrystalTransmissionLoss
 ****************************************************************************/
unsigned int  christelCudaComputeCrystalTransmissionLoss (double**     computed_losses,
							  unsigned int n_samples,
							  double*      impedances,
							  double       system_impedance) {
  
  // Compute loss magnitude is absolute (real), so not interleaved
  unsigned int  computed_losses_size = n_samples * sizeof(double) ;
  // Impedance vector is interleaved with real and imaginary values
  unsigned int  impedances_size = computed_losses_size * 2 ;
  double*       d_computed_losses ;
  double*       d_impedances ;
 

  // Allocate host memory
  *computed_losses = (double*) malloc (computed_losses_size) ;

  // Allocate device memory ;
  cudaMalloc (&d_computed_losses, computed_losses_size) ;
  cudaMalloc (&d_impedances, impedances_size) ;

  
  // Copy vectors from host memory to device memory
  cudaMemcpy (d_impedances,  impedances,  impedances_size,  cudaMemcpyHostToDevice) ;
  
   // Invoke kernel
  int threadsPerBlock = 256 ;
  int numberOfBlocks = (int) ceil ((double)n_samples / (double)threadsPerBlock) ;
  
  _christelCudaComputeCrystalTransmissionLoss<<<numberOfBlocks, threadsPerBlock>>> (d_computed_losses,
										    n_samples,
										    d_impedances,
										    system_impedance) ;
  // Copy result from device memory to host memory
  // h_C contains the result in host memory
  cudaMemcpy (*computed_losses, d_computed_losses, computed_losses_size, cudaMemcpyDeviceToHost) ;
  
  
  // Free device memory
  cudaFree (d_impedances) ;
  cudaFree (d_computed_losses) ;
    
  return n_samples ;

  
}

