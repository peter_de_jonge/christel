/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#include <CairoAdapter.hpp>
#include <cairomm/matrix.h>
#include <pangomm.h>
#include <iostream>

namespace Plotmm
{
  /*************************************************************************
   * Contructor: CairoAdapter::CairoAdapter
   ************************************************************************/
  CairoAdapter::CairoAdapter (const Cairo::RefPtr<Cairo::Context>& cr,
			      double                               allocated_x,
			      double                               allocated_y,
			      double                               allocated_width,
			      double                               allocated_height) :
    _cr (cr) ,
    _allocated_x (allocated_x),
    _allocated_y (allocated_y),
    _allocated_width (allocated_width),
    _allocated_height (allocated_height) {
    _y_flip_matrix = Cairo::Matrix (1.0D, 0.0D, 0.0D, -1.0D, _allocated_x, _allocated_height + _allocated_y) ;
    _cr->set_matrix (_y_flip_matrix) ;
  }
	
  /*************************************************************************
   * Destructor: CairoAdapter::~CairoAdapter
   ************************************************************************/
  CairoAdapter::~CairoAdapter () {
  }
	
  /*************************************************************************
   * Method: CairoAdapter::clear
   ************************************************************************/
  void CairoAdapter::clear () {
    d2_move_to (0.0D,             0.0D) ;
    d2_line_to (_allocated_width, 0.0D) ;
    d2_line_to (_allocated_width, _allocated_height) ;
    d2_line_to (0.0D,             _allocated_height) ;
    close_path () ;
    fill () ;
  }

  /*************************************************************************
   * Method: CairoAdapter::close_path
   ************************************************************************/
  void CairoAdapter::close_path () {
    _cr->close_path () ;
  }

  /*************************************************************************
   * Method: CairoAdapter::move_to_2d
   ************************************************************************/
  void CairoAdapter::d2_move_to (double x, double y) {
    _cr->move_to (x, y) ;
  }
	
  /*************************************************************************
   * Method: CairoAdapter::line_to_2d
   ************************************************************************/
  void CairoAdapter::d2_line_to (double x, double y)  {
    _cr->line_to (x, y) ;
  }
	
  /*************************************************************************
   * Method: CairoAdapter::d2_set_matrix
   ************************************************************************/
  void CairoAdapter::d2_set_matrix (double  	xx,
				    double  	yx,
				    double  	xy,
				    double  	yy,
				    double  	x0,
				    double  	y0) {
    Cairo::Matrix _matrix (xx, yx, xy, yy, x0, y0) ;
    _cr->set_matrix (_y_flip_matrix) ;
    _cr->transform (_matrix) ;
  }
	
  /*************************************************************************
   * Method: CairoAdapter::d2_text
   ************************************************************************/
  void CairoAdapter::d2_text (std::string& text, double x, double y) {
    Glib::RefPtr<Pango::Layout> layout =    Pango::Layout::create (_cr) ;
    Pango::FontDescription      font_desc = Pango::FontDescription ("Sans 12") ;
    Cairo::Matrix               _matrix_flip_and_offset (1.0D, 0.0D, 0.0D, -1.0D, 0.0D, 0.0D) ;
    Cairo::Matrix               _matrix_bak ;
    int                         width ;
    int                         height ;
    _cr->get_matrix (_matrix_bak) ;
    layout->set_alignment (Pango::ALIGN_CENTER) ;
    layout->set_font_description (font_desc) ;
    layout->set_text (text) ;
    layout->get_size (width, height) ;
    d2_move_to (x - width / 2 / PANGO_SCALE, y + height / 2 / PANGO_SCALE) ;
    _cr->transform (_matrix_flip_and_offset) ;		
    layout->show_in_cairo_context (_cr) ;
    _cr->set_matrix (_matrix_bak) ;
  }
	
  /*************************************************************************
   * Method: CairoAdapter::d2_transform
   ************************************************************************/
  void CairoAdapter::d2_transform (double  	xx,
				   double  	yx,
				   double  	xy,
				   double  	yy,
				   double  	x0,
				   double  	y0)  {
    Cairo::Matrix _matrix (xx, yx, xy, yy, x0, y0) ;
    _cr->transform (_matrix) ;
  }

	
  /*************************************************************************
   * Method: CairoAdapter::fill_preserve
   ************************************************************************/
  void CairoAdapter::fill_preserve () {
    _cr->fill_preserve () ;
  }
	
  /*************************************************************************
   * Method: CairoAdapter::fill
   ************************************************************************/
  void CairoAdapter::fill () {
    _cr->fill () ;
  }
	
  /*************************************************************************
   * Method: CairoAdapter::set_line_width
   ************************************************************************/
  void CairoAdapter::set_line_width (double line_width) {
    _cr->set_line_width (line_width) ;
  }
	
  /*************************************************************************
   * Method: CairoAdapter::set_line_cap
   ************************************************************************/
  void CairoAdapter::set_line_cap (LineCap line_cap) {
    switch (line_cap) {
    case LINE_CAP_BUTT:   _cr->set_line_cap (Cairo::LINE_CAP_BUTT) ; return ;
    case LINE_CAP_ROUND:  _cr->set_line_cap (Cairo::LINE_CAP_ROUND) ; return ;
    case LINE_CAP_SQUARE: _cr->set_line_cap (Cairo::LINE_CAP_SQUARE) ; return ;
    }
  }
	
  /*************************************************************************
   * Method: CairoAdapter::set_source_rgba
   ************************************************************************/
  void CairoAdapter::set_source_rgba (double red, double green, double blue, double alpha) {
    _cr->set_source_rgba (red, green, blue, alpha) ;
  }
	
  /*************************************************************************
   * Method: CairoAdapter::stroke
   ************************************************************************/
  void CairoAdapter::stroke () {
    _cr->stroke () ;
  }
	
} // namespace Plotmm
