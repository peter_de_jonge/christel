/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#ifndef LIBPLOTMM_LINEPLOT2D_HPP
#define LIBPLOTMM_LINEPLOT2D_HPP

#include <DataSet.hpp>
#include <PeakSet.hpp>
#include <Axis.hpp>
#include <Plot.hpp>
#include <vector>

namespace Plotmm
{
  class D2LinePlot : public Plot {
  public:
    D2LinePlot (Axis* x_axis, Axis* y_axis) ;
    D2LinePlot (Axis* x_axis, Axis* y_axis, DataSet* data_set) ;
    D2LinePlot (Axis* x_axis, Axis* y_axis, std::vector<DataSet*> data_set) ;
    D2LinePlot (Axis* x_axis, Axis* y_axis, PeakSet* peak_set) ;
    D2LinePlot (Axis* x_axis, Axis* y_axis, DataSet* data_set, PeakSet* peak_set) ;
    D2LinePlot (Axis* x_axis, Axis* y_axis, std::vector<DataSet*> data_set, PeakSet* peak_set) ;

    virtual void render (RenderAdapter& adapter, double allocated_width, double allocated_height) ;
    void addDataSet (DataSet* dataset) ;
    
  private:
    std::vector<DataSet*> _data_sets ;
    PeakSet*              _peak_set ;
    Axis*                 _x_axis ;
    Axis*                 _y_axis ;
    
    void _renderDataSet (RenderAdapter& adapter,
			 DataSet*       data_set,
			 double         origin_x,
			 double         origin_y,
			 double         x_scale,
			 double         y_scale,
			 double         x_axis_major_tic_start,
			 double         x_axis_major_tic_end,
			 double         y_axis_major_tic_start,
			 double         y_axis_major_tic_end) ;
	
  } ;
} // namespace Plotmm

#endif // LIBPLOTMM_LINEPLOT_HPP
