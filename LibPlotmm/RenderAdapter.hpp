/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBPLOTMM_RENDERADAPTER_HPP
#define LIBPLOTMM_RENDERADAPTER_HPP

#include <string>

namespace Plotmm
{
	typedef enum {
        LINE_CAP_BUTT   = 0,
	    LINE_CAP_ROUND  = 1,
	    LINE_CAP_SQUARE = 2
	} LineCap ;
						  
	
	class RenderAdapter {
	public:
		RenderAdapter () {} ;
		~RenderAdapter ()  {} ;
		virtual void clear () = 0 ;
		virtual void close_path () = 0 ;

		virtual void d2_line_to (double x, double y) = 0 ;
		virtual void d2_move_to (double x, double y) = 0 ;
		virtual void d2_set_matrix (double  	xx,
									double  	yx,
									double  	xy,
									double  	yy,
									double  	x0,
									double  	y0) = 0 ;
		virtual void d2_text (std::string& text, double x, double y) = 0 ;
		virtual void d2_transform (double  	xx,
								   double  	yx,
								   double  	xy,
								   double  	yy,
								   double  	x0,
								   double  	y0) = 0 ;
		virtual void fill () = 0 ;
		virtual void fill_preserve () = 0 ;
		virtual void set_line_cap (LineCap line_cap) = 0 ;
		virtual void set_line_width (double line_width) = 0 ;
	    virtual void set_source_rgba (double red, double green, double blue, double alpha) = 0 ;
		virtual void stroke () = 0 ;
	} ;

} // namespace LibPlotMM

#endif // LIBPLOTMM_RENDERADAPTER_HPP
