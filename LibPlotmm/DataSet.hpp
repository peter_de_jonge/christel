/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#ifndef LIBPLOTMM_DATASET_HPP
#define LIBPLOTMM_DATASET_HPP

namespace Plotmm
{
	class DataSet {
	public:
		DataSet (double* x, double* y, int n_points) ;
		void get_data (double*& x,
					   double*& y,
					   int&     n_points) ;

		void get_color (double& color_red,
						double& color_green,
						double& color_blue) ;
		void set_color (double color_red,
						double color_green,
						double color_blue) ;
	private:
		double* _x ;
		double* _y ;
		double  _color_red ;
		double  _color_green ;
		double  _color_blue ;
		int     _n_points ;
	} ;  

} // namespace Plotmm

#endif // LIBPLOTMM_DATASET_HPP
