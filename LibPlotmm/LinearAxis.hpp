/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBPLOTMM_LINEAIRAXIS_HPP
#define LIBPLOTMM_LINEAIRAXIS_HPP

#include <Axis.hpp>

namespace Plotmm
{   
    class LinearAxis : public Axis {
	public:
		LinearAxis (AxisDir direction,
					double  major_tic_start,
					double  major_tic_increase,
					double  major_tic_end,
					double  major_tic_label_base,
					int     major_tic_label_n_decimals,
					int     n_minor_tics_per_major_interval) ;
		~LinearAxis () ;
		virtual void render (RenderAdapter& adapter,
							 double x,
							 double y,
							 double l) ;
	private:
		void _render_x_axis (RenderAdapter& adapter,
							 double x,
							 double y,
							 double l) ;
		void _render_x_tics (RenderAdapter& ad,
							 double x,
							 double y,
							 double da,
							 double dm) ;
		void _render_x_tic_label (RenderAdapter& ad,
								  double x,
								  double y,
								  double label) ;
		void _render_y_axis (RenderAdapter& adapter,
							 double x,
							 double y,
							 double l) ;
		void _render_y_tics (RenderAdapter& ad,
							 double x,
							 double y,
							 double da,
							 double dm) ;
		void _render_y_tic_label (RenderAdapter& ad,
								  double x,
								  double y,
								  double label) ;

		virtual void _resolve_n_major_tic_labels (double* labels) ;
		
	} ;

} // namespace Plotmm

#endif // LIBPLOTMM_LINEAIRAXIS_HPP
