/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <CairoAdapter.hpp>
#include "PlotArea.hpp"
#include <iostream>
#include <cstring>


namespace Plotmm
{
  /*************************************************************************
   * Constructor: PlotArea::PlotArea
   ************************************************************************/
  PlotArea::PlotArea () :
    Gtk::Widget (),
    //The GType name will actually be gtkmm__CustomObject_mywidget
    Glib::ObjectBase ("plotarea"),
    _plot(NULL) {
		
    set_has_window (true) ;
		
    // Set the widget name to use in the CSS file.
    set_name ("plot-area") ;
  }

  /*************************************************************************
   * Destructor: PlotArea::~PlotArea
   ************************************************************************/
  PlotArea::~PlotArea () {

  }

  /*************************************************************************
   * Method: PlotArea::set_plot
   ************************************************************************/
  void  PlotArea::set_plot (Plot* plot) {
    _plot = plot ;
    queue_draw () ;
  }

  /*************************************************************************
   * Method: PlotArea::get_request_mode_vfunc
   ************************************************************************/
  Gtk::SizeRequestMode PlotArea::get_request_mode_vfunc () const {
    //Accept the default value supplied by the base class.
    return Gtk::Widget::get_request_mode_vfunc () ;
  }

  //Discover the total amount of minimum space and natural space needed by
  //this widget.
  //Let's make this simple example widget always need minimum 60 by 50 and
  //natural 100 by 70.
  /*************************************************************************
   * Method: PlotArea::get_preferred_width_vfunc
   ************************************************************************/
  void PlotArea::get_preferred_width_vfunc (int& minimum_width, int& natural_width) const	{
    minimum_width = 60 ;
    natural_width = 100 ;
  }

  /*************************************************************************
   * Method: PlotArea::get_preferred_height_for_width_vfunc
   ************************************************************************/
  void PlotArea::get_preferred_height_for_width_vfunc (int /* width */,
						       int& minimum_height, int& natural_height) const {
    minimum_height = 500 ;
    natural_height = 70 ;
  }

  /*************************************************************************
   * Method: PlotArea::get_preferred_height_vfunc
   ************************************************************************/
  void PlotArea::get_preferred_height_vfunc (int& minimum_height, int& natural_height) const {
    minimum_height = 50 ;
    natural_height = 70 ;
  }

  /*************************************************************************
   * Method: PlotArea::get_preferred_width_for_height_vfunc
   ************************************************************************/
  void PlotArea::get_preferred_width_for_height_vfunc (int /* height */,
						       int& minimum_width, int& natural_width) const {
    minimum_width = 60 ;
    natural_width = 100 ;
  }

  /*************************************************************************
   * Method: PlotArea::on_size_allocate
   ************************************************************************/
  void PlotArea::on_size_allocate(Gtk::Allocation& allocation) {
    //Do something with the space that we have actually been given:
    //(We will not be given heights or widths less than we have requested, though
    //we might get more)

    //Use the offered allocation for this container:
    set_allocation (allocation) ;

    if(_refGdkWindow) {
      _refGdkWindow->move_resize (allocation.get_x (), allocation.get_y (),
				  allocation.get_width (), allocation.get_height ()) ;
    }
  }

  /*************************************************************************
   * Method: PlotArea::on_map
   ************************************************************************/
  void PlotArea::on_map () {
    //Call base class:
    Gtk::Widget::on_map () ;
  }

  /*************************************************************************
   * Method: PlotArea::on_unmap
   ************************************************************************/
  void PlotArea::on_unmap () {
    //Call base class:
    Gtk::Widget::on_unmap () ;
  }

  /*************************************************************************
   * Method: PlotArea::on_realize
   ************************************************************************/
  void PlotArea::on_realize () {
    //Do not call base class Gtk::Widget::on_realize().
    //It's intended only for widgets that set_has_window(false).
    set_realized () ;

    if (!_refGdkWindow) {
      //Create the GdkWindow:

      GdkWindowAttr attributes ;
      memset (&attributes, 0, sizeof (attributes)) ;

      Gtk::Allocation allocation = get_allocation () ;

      //Set initial position and size of the Gdk::Window:
      attributes.x = allocation.get_x () ;
      attributes.y = allocation.get_y () ;
		
      attributes.width = allocation.get_width () ;
      attributes.height = allocation.get_height () ;
      attributes.event_mask = get_events () | Gdk::EXPOSURE_MASK ;
      attributes.window_type = GDK_WINDOW_CHILD;
      attributes.wclass = GDK_INPUT_OUTPUT;

      _refGdkWindow = Gdk::Window::create (get_parent_window (),
					   &attributes,
					   GDK_WA_X | GDK_WA_Y) ;
      set_window (_refGdkWindow) ;

      //make the widget receive expose events
      _refGdkWindow->set_user_data (gobj ()) ;
    }
  }
	
  /*************************************************************************
   * Method: PlotArea::on_unrealize
   ************************************************************************/
  void PlotArea::on_unrealize () {
    _refGdkWindow.reset ( );

    //Call base class:
    Gtk::Widget::on_unrealize () ;
  }


  /*************************************************************************
   * Method: PlotArea::on_draw
   ************************************************************************/
  bool PlotArea::on_draw (const Cairo::RefPtr<Cairo::Context>& cr) {
    const Gtk::Allocation      allocation  = get_allocation ();
    const double               allocated_x = (double) allocation.get_x() ;
    const double               allocated_y = (double) allocation.get_y () ;
		
    const double               allocated_width = (double) allocation.get_width () ;
    const double               allocated_height = (double) allocation.get_height ()
      ;
    const double               scale_x = (double) allocated_width / 1000.0D ;
    const double               scale_y = (double) allocated_height / 1000.0D ;
    Plotmm::CairoAdapter       ad (cr, allocated_x, allocated_y, allocated_width, allocated_height) ;

    if (_plot) {
      // draw the background
      _plot->render (ad, allocated_width, allocated_height) ;
    }	

    return true;
  }


	
} // namespace Plotmm
