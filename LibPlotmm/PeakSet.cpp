/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <PeakSet.hpp>
#include <iostream>

namespace Plotmm
{
	/*************************************************************************
	 * Contructor: PeakSet::PeakSet
	 ************************************************************************/
	PeakSet::PeakSet (double* x, double* y, int n_points) :
		_x (x), _y (y), _n_points (n_points),
		_color_red (0.0D), _color_green (0.0D), _color_blue (0.0D) {
	}

	/*************************************************************************
	 * Method: PeakSet::get_data
	 ************************************************************************/
	void PeakSet::get_data (double*& x,
							double*& y,
							int&     n_points) {
		x = _x ;
		y = _y ;
		n_points = _n_points ;
	}

	/*************************************************************************
	 * Method: PeakSet::get_color
	 ************************************************************************/
	void PeakSet::get_color (double& color_red,
							 double& color_green,
							 double& color_blue) {
		color_red   = _color_red ;
		color_green = _color_green ;
		color_blue  = _color_blue ;
	}
	
	/*************************************************************************
	 * Method: PeakSet::set_color
	 ************************************************************************/
	void PeakSet::set_color (double color_red,
							 double color_green,
							 double color_blue) {
		_color_red   = color_red ;
		_color_green = color_green ;
		_color_blue  = color_blue ;
	}


	/*************************************************************************
	 * Method: PeakSet::render_markers
	 ************************************************************************/
	void PeakSet::render_markers (RenderAdapter& ad,
								  double         origin_x,
								  double         origin_y,
								  double         x_scale,
								  double         y_scale,
								  double         x_axis_major_tic_start,
								  double         y_axis_major_tic_start) {
		int _i ;
		for (_i = 0; _i < _n_points; _i++) {
			_render_marker (ad,
							_x[_i],
							_y[_i],
							origin_x,
							origin_y,
							x_scale,
							y_scale,
							x_axis_major_tic_start,
							y_axis_major_tic_start) ;
		}
	}
	

	/*************************************************************************
	 * Method: PeakSet::_render_marker
	 ************************************************************************/
	void PeakSet::_render_marker (RenderAdapter& ad,
								  double         x,
								  double         y,
								  double         origin_x,
								  double         origin_y,
								  double         x_scale,
								  double         y_scale,
								  double         x_axis_major_tic_start,
								  double         y_axis_major_tic_start) {
		
		const double _marker_size = 10.0D ;
		const double _marker_half_size = 10.0D / 2.0D ;
		
		// set peak marker transformation
		ad.d2_set_matrix (1.0D, 0.0D, 0.0D, 1.0D, origin_x, origin_y) ;
		ad.d2_transform (x_scale, 0.0D, 0.0D, y_scale, 0.0D, 0.0D) ;
		ad.d2_transform (1.0D , 0.0D, 0.0D, 1.0D, -x_axis_major_tic_start, -y_axis_major_tic_start) ;

		// draw first half marker
		ad.d2_move_to (x - _marker_half_size / x_scale, y) ;
		ad.d2_line_to (x + _marker_half_size / x_scale, y) ;

		// do stroke peak marker
		ad.d2_set_matrix (1.0D, 0.0D, 0.0D, 1.0D, 0.0D, 0.0D) ; // Don't transform stroke
		ad.set_line_width (2.0) ;
		ad.set_source_rgba (_color_red, _color_green, _color_blue, 1.0D) ;
		ad.stroke () ;

		// set peak marker transformation
		ad.d2_set_matrix (1.0D, 0.0D, 0.0D, 1.0D, origin_x, origin_y) ;
		ad.d2_transform (x_scale, 0.0D, 0.0D, y_scale, 0.0D, 0.0D) ;
		ad.d2_transform (1.0D , 0.0D, 0.0D, 1.0D, -x_axis_major_tic_start, -y_axis_major_tic_start) ;

		// draw second half marker
		ad.d2_move_to (x, y - _marker_half_size / y_scale ) ;
		ad.d2_line_to (x, y + _marker_half_size / y_scale) ;

		// do stroke peak marker
		ad.d2_set_matrix (1.0D, 0.0D, 0.0D, 1.0D, 0.0D, 0.0D) ; // Don't transform stroke
		ad.set_line_width (2.0) ;
		ad.set_source_rgba (_color_red, _color_green, _color_blue, 1.0D) ;
		ad.stroke () ;		
	}
	

} // namespace LibPlotMM
