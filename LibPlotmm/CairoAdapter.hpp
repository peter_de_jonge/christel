/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#ifndef LIBPLOTMM_CAIROADAPTER_HPP
#define LIBPLOTMM_CAIROADAPTER_HPP

#include <cairomm/context.h>
#include <RenderAdapter.hpp>
#include <gdkmm/general.h>  // for cairo helper functions


namespace Plotmm
{
	class CairoAdapter : public RenderAdapter {
	public:
		CairoAdapter (const Cairo::RefPtr<Cairo::Context>& cr,
					  double                               allocated_x,
					  double                               allocated_y,
					  double                               allocated_width,
					  double                               allocated_height) ;
		~CairoAdapter () ;
		virtual void clear () ;
		virtual void close_path () ;
		virtual void d2_line_to (double x, double y) ;
		virtual void d2_move_to (double x, double y) ;
		virtual void d2_set_matrix (double  	xx,
									double  	yx,
									double  	xy,
									double  	yy,
									double  	x0,
									double  	y0) ;
		virtual void d2_text (std::string& text, double x, double y) ;
		virtual void d2_transform (double  	xx,
								   double  	yx,
								   double  	xy,
								   double  	yy,
								   double  	x0,
								   double  	y0) ;
		virtual void fill () ;
		virtual void fill_preserve () ;
		virtual void set_line_cap (LineCap line_cap) ;
		virtual void set_line_width (double line_width) ;
		virtual	void set_source_rgba (double red, double green, double blue, double alpha) ;
		virtual void stroke () ;
	private:
		Cairo::RefPtr<Cairo::Context> _cr ;
		Cairo::Matrix                 _y_flip_matrix ;
		double                        _allocated_x ;
		double                        _allocated_y ;
		double                        _allocated_width ;
		double                        _allocated_height ;

		void                          _resetMatrix () ;
	} ;

} // namespace Plotmm

#endif // LIBPLOTMM_CAIROADAPTER_HPP
