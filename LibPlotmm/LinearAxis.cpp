/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <LinearAxis.hpp>
#include <cmath>
#include <iostream>

namespace Plotmm
{
  /*************************************************************************
   * Contructor: LinearAxis::LinearAxis
   ************************************************************************/
  LinearAxis::LinearAxis (AxisDir direction,
			  double  major_tic_start,
			  double  major_tic_increase,
			  double  major_tic_end,
			  double  major_tic_label_base,
			  int     major_tic_label_n_decimals,
			  int     n_minor_tics_per_major_interval) :
    Axis::Axis (direction,
		major_tic_start,
		major_tic_increase,
		major_tic_end,
		major_tic_label_base,
		major_tic_label_n_decimals,
		n_minor_tics_per_major_interval) {
  }
	
  /*************************************************************************
   * Destructor: LinearAxis::LinearAxis
   ************************************************************************/
  LinearAxis::~LinearAxis () {} ;

  /*************************************************************************
   * Method: LinearAxis::render
   ************************************************************************/
  void LinearAxis::render (RenderAdapter& ad,
			   double x,
			   double y,
			   double l) {
  
    double d_major_tics = l / (_n_major_tics - 1) ;
    double d_minor_tics = d_major_tics / (_n_minor_tics_per_major_interval + 1) ;
		
    ad.set_source_rgba (0.0D, 0.0D, 0.0D, 1.0D) ;
    ad.set_line_width (_line_width) ;
    ad.set_line_cap (LINE_CAP_ROUND) ;
    if (_direction == AXIS_DIR_X) {
      _render_x_axis (ad, x, y, l) ;
      _render_x_tics (ad, x ,y, d_major_tics, d_minor_tics) ;
    } else {
      _render_y_axis (ad, x, y, l) ;
      _render_y_tics (ad, x ,y, d_major_tics, d_minor_tics) ;
    }
  }

	
  /*************************************************************************
   * Method: LinearAxis::render
   ************************************************************************/
  void LinearAxis::_render_x_axis (RenderAdapter& ad,
				   double x,
				   double y,
				   double l) {
    ad.d2_move_to (x, y) ;
    ad.d2_line_to (x + l, y) ;
    ad.stroke () ;
  }

  /*************************************************************************
   * Method: LinearAxis::_render_x_tics
   ************************************************************************/
  void LinearAxis::_render_x_tics (RenderAdapter& ad,
				   double x,
				   double y,
				   double da,
				   double dm) {
    double _xa = x ;
    double _xm = _xa ;
    double _label = _major_tic_start ;
    int    _i ;
    int    _p ;

    ad.set_line_width (_major_tic_width) ;
    ad.d2_move_to (_xa, y) ;
    ad.d2_line_to (_xa, y + _major_tic_length) ;
    ad.stroke () ;
    _render_x_tic_label (ad, _xa, y, _label) ;
    for (_i = 1; _i < _n_major_tics ; _i++) {
      _xm = _xa ;
      for (_p = 0; _p < _n_minor_tics_per_major_interval ; _p++) {
	_xm += dm ;
	ad.set_line_width (_minor_tic_width) ;
	ad.d2_move_to (_xm, y) ;
	ad.d2_line_to (_xm, y + _minor_tic_length) ;
	ad.stroke () ;
      }
			
      _xa += da ;
      _label += _major_tic_increase ;
      ad.set_line_width (_major_tic_width) ;
      ad.d2_move_to (_xa, y) ;
      ad.d2_line_to (_xa, y + _major_tic_length) ;
      ad.stroke () ;
      _render_x_tic_label (ad, _xa, y, _label) ;
    }
  }


	
  /*************************************************************************
   * Method: LinearAxis::_render_x_tic_label
   ************************************************************************/
  void LinearAxis::_render_x_tic_label (RenderAdapter& ad,
					double x,
					double y,
					double label) {
    std::string text = std::to_string (label / _major_tic_label_base) ;
    _truncate_numeric_tic_label (text) ;
    ad.d2_text (text, x, y - 20.0D) ;
  }
	
  /*************************************************************************
   * Method: LinearAxis::render
   ************************************************************************/
  void LinearAxis::_render_y_axis (RenderAdapter& ad,
				   double x,
				   double y,
				   double l) {

    ad.d2_move_to (x, y) ;
    ad.d2_line_to (x, y+l) ;
    ad.stroke () ;
  }

  /*************************************************************************
   * Method: LinearAxis::_render_y_tics
   ************************************************************************/
  void LinearAxis::_render_y_tics (RenderAdapter& ad,
				   double x,
				   double y,
				   double da,
				   double dm) {
    double _ya = y ;
    double _ym = _ya ;
    double _label = _major_tic_start ;
    int    _i ;
    int    _p ;

    ad.set_line_width (_major_tic_width) ;
    ad.d2_move_to (x, _ya) ;
    ad.d2_line_to (x + _major_tic_length, _ya) ;
    ad.stroke () ;
    _render_y_tic_label (ad, x, _ya, _label) ;
    for (_i = 1; _i < _n_major_tics ; _i++) {
      _ym = _ya ;
      for (_p = 0; _p < _n_minor_tics_per_major_interval ; _p++) {
	_ym += dm ;
	ad.set_line_width (_minor_tic_width) ;
	ad.d2_move_to (x, _ym) ;
	ad.d2_line_to (x + _minor_tic_length, _ym) ;
	ad.stroke () ;
      }
      _ya += da ;
      _label += _major_tic_increase ;
      ad.set_line_width (_major_tic_width) ;
      ad.d2_move_to (x, _ya) ;
      ad.d2_line_to (x + _major_tic_length, _ya) ;
      ad.stroke () ;
      _render_y_tic_label (ad, x, _ya, _label) ;
    }
  }

  /*************************************************************************
   * Method: LinearAxis::_render_y_tic_label
   ************************************************************************/
  void LinearAxis::_render_y_tic_label (RenderAdapter& ad,
					double x,
					double y,
					double label) {
    std::string text = std::to_string (label / _major_tic_label_base) ;
    _truncate_numeric_tic_label (text) ;
    ad.d2_text (text, x - 30.0D, y) ;
  }
	
  /*************************************************************************
   * Method: LinearAxis::_resolve_n_major_tic_labels
   ************************************************************************/
  void LinearAxis::_resolve_n_major_tic_labels (double* labels) {
    int _i ;
    double label = _major_tic_start ;
		
    for (_i = 0; _i < _n_major_tics ; _i++) {
      labels[_i] = label ;
      label += _major_tic_increase ;
    }
  }
} // namespace LibPlotMM
