/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#include <Axis.hpp>
#include <cmath>

namespace Plotmm
{
	/*************************************************************************
	 * Contructor: Axis::Axis
	 ************************************************************************/
	Axis::Axis (AxisDir direction,
				double  major_tic_start,
				double  major_tic_increase,
				double  major_tic_end,
				double  major_tic_label_base,
				int     major_tic_label_n_decimals,
				int     n_minor_tics_per_major_interval) {
		_direction = direction ;
		_line_width = 2.0D ;
		_major_tic_length = 8.0D ;
		_major_tic_width = 2.0D ;
		_major_tic_start = major_tic_start ;
		_major_tic_increase = major_tic_increase ;
		_major_tic_end = major_tic_end ;
		_minor_tic_length = 6.0D ;
		_minor_tic_width = 1.0D ;
		_major_tic_label_base = major_tic_label_base ;
		_major_tic_label_n_decimals = major_tic_label_n_decimals ;
		_n_minor_tics_per_major_interval = n_minor_tics_per_major_interval ;
		_resolve_n_major_tics () ;
	}
	
	/*************************************************************************
	 * Destructor: Axis::Axis
	 ************************************************************************/
	Axis::~Axis () {} ;


	/*************************************************************************
	 * Method: Axis::get_major_tic_start
	 ************************************************************************/
	double Axis::get_major_tic_start () {
		return _major_tic_start ;
	}

	/*************************************************************************
	 * Method: Axis::get_major_tic_end
	 ************************************************************************/
	double	Axis::get_major_tic_end () {
		return _major_tic_end ;
	}
	
	/*************************************************************************
	 * Method: Axis::set_line_width
	 ************************************************************************/
	void  Axis::set_line_width (double line_width) {
		_line_width = line_width ;
	}
	
	/*************************************************************************
	 * Method: Axis::set_major_tic_length
	 ************************************************************************/
	void Axis::set_major_tic_length (double major_tic_length) {
		_major_tic_length = major_tic_length ;
	}
	
	/*************************************************************************
	 * Method: Axis:: set_major_tic_width
	 ************************************************************************/
	void Axis::set_major_tic_width (double major_tic_width) {
		_major_tic_width = major_tic_width ;
	}

	/*************************************************************************
	 * Method: Axis::set_minor_tic_length
	 ************************************************************************/
	void Axis::set_minor_tic_length (double minor_tic_length) {
		_minor_tic_length = minor_tic_length ;
	}
	
	/*************************************************************************
	 * Method: Axis:: set_minor_tic_width
	 ************************************************************************/
	void Axis::set_minor_tic_width (double minor_tic_width) {
		_minor_tic_width = minor_tic_width ;
	}

	/*************************************************************************
	 * Method: Axis::_resolve_n_major_tics
	 ************************************************************************/
	void Axis::_resolve_n_major_tics () {
		_n_major_tics = (int) ceil ((_major_tic_end -_major_tic_start) / _major_tic_increase) + 1 ;
	}
	
	/*************************************************************************
	 * Method: Axis::_truncate_numeric_tic_label
	 ************************************************************************/
	void  Axis::_truncate_numeric_tic_label (std::string& tic_label) {
		char   _decimal_separator_character = ',' ; // TODO: this should be retrieved from the locale
		size_t _decimal_separator_position  = tic_label.find_first_of (_decimal_separator_character) ;
		if (_major_tic_label_n_decimals == 0) {
			tic_label.erase (_decimal_separator_position) ;
		} else {
			tic_label.erase (_decimal_separator_position + _major_tic_label_n_decimals + 1) ;
		}
	}
	
	
} // namespace LibPlotMM
