/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#include <D2LinePlot.hpp>

#include <iostream>

namespace Plotmm
{

  /*************************************************************************
   * Contructor: D2LinePlot::D2LinePlot
   ************************************************************************/
  D2LinePlot::D2LinePlot (Axis*    x_axis,
			  Axis*    y_axis) :
    _x_axis (x_axis), _y_axis (y_axis), _peak_set (NULL) {
  }
  
  /*************************************************************************
   * Contructor: D2LinePlot::D2LinePlot
   ************************************************************************/
  D2LinePlot::D2LinePlot (Axis*    x_axis,
			  Axis*    y_axis,
			  DataSet* data_set) :
    _x_axis (x_axis), _y_axis (y_axis), _peak_set (NULL) {
    addDataSet (data_set) ;
  }

  /*************************************************************************
   * Contructor: D2LinePlot::D2LinePlot
   ************************************************************************/
  D2LinePlot::D2LinePlot (Axis*                 x_axis,
			  Axis*                 y_axis,
			  std::vector<DataSet*> data_sets) :
    _x_axis (x_axis), _y_axis (y_axis), _data_sets (data_sets), _peak_set (NULL) {
  }

 /*************************************************************************
   * Contructor: D2LinePlot::D2LinePlot
   ************************************************************************/
  D2LinePlot::D2LinePlot (Axis*    x_axis,
			  Axis*    y_axis,
			  PeakSet* peak_set) :
    _x_axis (x_axis), _y_axis (y_axis), _peak_set (peak_set) {
  }
  
  /*************************************************************************
   * Contructor: D2LinePlot::D2LinePlot
   ************************************************************************/
  D2LinePlot::D2LinePlot (Axis*    x_axis,
			  Axis*    y_axis,
			  DataSet* data_set,
			  PeakSet* peak_set) :
    _x_axis (x_axis), _y_axis (y_axis), _peak_set (peak_set) {
    addDataSet (data_set) ;
  }

  /*************************************************************************
   * Contructor: D2LinePlot::D2LinePlot
   ************************************************************************/
  D2LinePlot::D2LinePlot (Axis*                 x_axis,
			  Axis*                 y_axis,
			  std::vector<DataSet*> data_sets,
			  PeakSet*              peak_set) :
    _x_axis (x_axis), _y_axis (y_axis), _data_sets (data_sets), _peak_set (peak_set) {
  }

  /*************************************************************************
   * Method: D2LinePlot::addDataSet
   ************************************************************************/
  void D2LinePlot::addDataSet (DataSet* dataset) {
    _data_sets.push_back (dataset) ;
  }

  /*************************************************************************
   * Method: D2LinePlot::render
   ************************************************************************/
  void D2LinePlot::render (RenderAdapter& ad, double aw, double ah) {
  
    const double  _origin_x = 70.0D ;
    const double  _origin_y = 50.0D ;
    const double  _axis_x_l  = aw - 130.0D ;
    const double  _axis_y_l  = ah - 100.0D ;
    const double  _x_axis_major_tic_start = _x_axis->get_major_tic_start () ;
    const double  _x_axis_major_tic_end   = _x_axis->get_major_tic_end () ;
    const double  _y_axis_major_tic_start = _y_axis->get_major_tic_start () ;
    const double  _y_axis_major_tic_end   = _y_axis->get_major_tic_end () ;
    const double  _x_scale = _axis_x_l / (_x_axis_major_tic_end - _x_axis_major_tic_start) ;
    const double  _y_scale = _axis_y_l / (_y_axis_major_tic_end - _y_axis_major_tic_start) ;

    // draw the background
    ad.set_source_rgba (1.0D, 1.0D, 1.0D, 1.0D) ;
    ad.clear () ;
		
    // draw axises
    _x_axis->render (ad, _origin_x, _origin_y, _axis_x_l) ;
    _y_axis->render (ad, _origin_x, _origin_y, _axis_y_l) ;

    for (int n = 0; n < _data_sets.size(); n++) {
      _renderDataSet (ad,
		      _data_sets[n],
		      _origin_x,
		      _origin_y,
		      _x_scale,
		      _y_scale,
		      _x_axis_major_tic_start,
		      _x_axis_major_tic_end,
		      _y_axis_major_tic_start,
		      _y_axis_major_tic_end) ;
    }

    if (_peak_set) {
      _peak_set->render_markers (ad,
    				 _origin_x,
    				 _origin_y,
    				 _x_scale,
    				 _y_scale,
    				 _x_axis_major_tic_start,
    				 _y_axis_major_tic_start) ;
    }
  }

  /*************************************************************************
   * Method: D2LinePlot::_renderDataSet
   ************************************************************************/
  void D2LinePlot::_renderDataSet (RenderAdapter& ad,
				   DataSet*       data_set,
				   double         origin_x,
				   double         origin_y,
				   double         x_scale,
				   double         y_scale,
				   double         x_axis_major_tic_start,
				   double         x_axis_major_tic_end,
				   double         y_axis_major_tic_start,
				   double         y_axis_major_tic_end) {
    double        color_red ;
    double        color_green ;
    double        color_blue ;
    int           i = 0 ;
    double*       x ;
    double*       y ;
    int           n_points ;
    
     // draw dataset
    data_set->get_data (x, y , n_points) ;
    data_set->get_color (color_red, color_green, color_blue) ;

    // set dataset transformation
    ad.d2_set_matrix (1.0D, 0.0D, 0.0D, 1.0D, origin_x, origin_y) ;
    ad.d2_transform (x_scale, 0.0D, 0.0D, y_scale, 0.0D, 0.0D) ;
    ad.d2_transform (1.0D , 0.0D, 0.0D, 1.0D, -x_axis_major_tic_start, -y_axis_major_tic_start) ;
	
    while ((x[i] < x_axis_major_tic_start) && (i < n_points)) {
      i++ ;
    }
		
    ad.d2_move_to (x[i], y[i]) ;
		
    i++ ;
    while ((i < n_points) && (x[i] < x_axis_major_tic_end)) {
      ad.d2_line_to (x[i], y[i]) ;
      i++ ;
    }
	  
    // do stroke dataset
    ad.d2_set_matrix (1.0D, 0.0D, 0.0D, 1.0D, 0.0D, 0.0D) ; // Don't transform stroke
    ad.set_line_width (2.0) ;
    ad.set_source_rgba (color_red, color_green, color_blue, 1.0D) ;
    ad.stroke () ;

  }
} // Namespace Plotmm
