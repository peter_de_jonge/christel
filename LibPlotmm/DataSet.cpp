/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#include <DataSet.hpp>

namespace Plotmm
{
  /*************************************************************************
   * Contructor: DataSet::DataSet
   ************************************************************************/
  DataSet::DataSet (double* x, double* y, int n_points) :
    _x (x), _y (y), _n_points (n_points),
    _color_red (0.0D), _color_green (0.0D), _color_blue (0.0D) {
  }

  /*************************************************************************
   * Method: DataSet::get_data
   ************************************************************************/
  void DataSet::get_data (double*& x,
			  double*& y,
			  int&     n_points) {
    x = _x ;
    y = _y ;
    n_points = _n_points ;
  }

  /*************************************************************************
   * Method: DataSet::get_color
   ************************************************************************/
  void DataSet::get_color (double& color_red,
			   double& color_green,
			   double& color_blue) {
    color_red   = _color_red ;
    color_green = _color_green ;
    color_blue  = _color_blue ;
  }
	
  /*************************************************************************
   * Method: DataSet::set_color
   ************************************************************************/
  void DataSet::set_color (double color_red,
			   double color_green,
			   double color_blue) {
    _color_red   = color_red ;
    _color_green = color_green ;
    _color_blue  = color_blue ;
  }

} // namespace LibPlotMM
