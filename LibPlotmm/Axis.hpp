/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#ifndef LIBPLOTMM_AXIS_HPP
#define LIBPLOTMM_AXIS_HPP

#include <RenderAdapter.hpp>
#include <string>

namespace Plotmm
{

	typedef enum {
	    AXIS_DIR_X = 0,
		AXIS_DIR_Y = 1,
	} AxisDir ;
	
    class Axis {
	public:
		Axis (AxisDir direction,
			  double  major_tic_start,
			  double  major_tic_increase,
			  double  major_tic_end,
			  double  major_tic_label_base,
			  int     major_tic_label_n_decimals,
			  int     n_minor_tics_per_major_interval) ;
		~Axis () ;
		virtual void render (RenderAdapter& adapter,
							 double x,
							 double y,
							 double l) = 0 ;
		double get_major_tic_start () ;
		double get_major_tic_end () ;
		virtual void set_line_width (double line_width) ;
		virtual void set_major_tic_length (double major_tic_length) ;
		virtual void set_major_tic_width (double major_tic_width) ;
		virtual void set_minor_tic_length (double minor_tic_length) ;
		virtual void set_minor_tic_width (double minor_tic_width) ;
	protected:
		AxisDir _direction ;
		double  _line_width ;
		double  _major_tic_length ;
		double  _major_tic_width ;
		double  _major_tic_start ;
		double  _major_tic_increase ;
		double  _major_tic_end ;
		double  _major_tic_label_base ;
		int     _major_tic_label_n_decimals ;
		double  _minor_tic_length ;
		double  _minor_tic_width ;
		int     _n_minor_tics_per_major_interval ;	
		double  _n_major_tics ;

		virtual void _resolve_n_major_tics () ;
		virtual void _truncate_numeric_tic_label (std::string& tic_label) ;
		
	} ;

} // namespace LibPlotMM

#endif // LIBPLOTMM_AXIS_HPP
