/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <VNAAdapter.hpp>

namespace Christel::Lib {

  /***************************************************************************
   *  Method: VNAAdapter::setNAveragingCycles
   **************************************************************************/
  void VNAAdapter::setNAveragingCycles (unsigned int n_averaging_cycles) {
    _n_averaging_cycles = n_averaging_cycles ;
  }
  
  /***************************************************************************
   *  Method: VNAAdapter::getNAveragingCycles
   **************************************************************************/
  unsigned int VNAAdapter::getNAveragingCycles ()  {
    return _n_averaging_cycles ;
  }

  /***************************************************************************
   *  Method: VNAAdapter::setCalibrationFile
   **************************************************************************/
  void VNAAdapter::setCalibrationFile (std::string calibration_filename) {
    _calibration_filename = calibration_filename ;
  }
  
  /***************************************************************************
   *  Method: VNAAdapter::getCalibrationFile
   **************************************************************************/
  std::string VNAAdapter::getCalibrationFile () {
    return _calibration_filename ;
  }

  /***************************************************************************
   *  Method: VNAAdapter::setUSBPortPath
   **************************************************************************/
  void VNAAdapter::setUSBPortPath (std::string vna_usb_port_path) {
    _vna_usb_port_path = vna_usb_port_path ;
  }

  /***************************************************************************
   *  Method: VNAAdapter::getUSBPortPath
   **************************************************************************/
  std::string VNAAdapter::getUSBPortPath () {
    return _vna_usb_port_path ;
  }

  
  /***************************************************************************
   *  Method: VNAAdapter::incUse
   **************************************************************************/
  void VNAAdapter::incUses () {
    // TODO: make this thread safe
    _uses++ ;
  }

  /***************************************************************************
   *  Method: VNAAdapter ::decUse
   **************************************************************************/
  void VNAAdapter::decUses () {
    // TODO: make this thread safe
    _uses-- ;
  }
  
  /***************************************************************************
   *  Method: VNAAdapter::getUses
   **************************************************************************/
  unsigned int VNAAdapter::getUses () {
    // TODO: make this thread safe
    return _uses ;
  }

  /***************************************************************************
   *  Method: VNAAdapter::inUse
   **************************************************************************/
  bool VNAAdapter::inUse () {
    // TODO: make this thread safe
    return _uses > 0 ;
  }
 
} // namespace Christel::Lib
