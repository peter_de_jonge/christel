/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_COMPUTE_ADAPTER_MODULE_HPP
#define LIBCRISTEL_COMPUTE_ADAPTER_MODULE_HPP

#include <ComputeAdapter.hpp>
#include <Module.hpp>

namespace Christel::Lib {
  class ComputeAdapterManager ;
  
  class ComputeAdapterModule : public Module {
    friend class ComputeAdapterManager ;
  public:
     ComputeAdapterMeta*  getComputeAdapterMeta () ;
  private:

    // A function pointer
    typedef Christel::Lib::ComputeAdapter* (CreateComputeAdapterFunction) (void) ;
    
    // A function pointer
    typedef Christel::Lib::ComputeAdapterMeta* (GetComputeAdapterMetaFunction) (void) ;

    
    ComputeAdapter* _adapter ;
    
    ComputeAdapterModule (boost::filesystem::path file_path) ;
    ComputeAdapter*      createComputeAdapter () ;
    bool                 unload () ;
    bool                 load () ;
  } ;

} // namespace Christel::Lib

#endif // LIBCRISTEL_COMPUTE_MODULE_ADAPTER_HPP
