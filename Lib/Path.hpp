/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#ifndef LIBCRISTEL_PATH_HPP
#define LIBCRISTEL_PATH_HPP

#include <boost/filesystem.hpp>

#ifdef UNIX

#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>

#endif

namespace Christel::Lib {

  class Path {
  public:
    /*********************************************************************
     *  Method: Path::getDataRootPath
     ********************************************************************/
    static boost::filesystem::path getDataDirectoryPath () {
#ifdef UNIX
      struct passwd *pw      = getpwuid (getuid ()) ;
      const char    *homedir = pw->pw_dir ;
      const boost::filesystem::path path = boost::filesystem::path (homedir) /= RELATIVE_USER_CONFIG_DIR ;
      return path ;
#endif
    }
      
    /*********************************************************************
     *  Method: Path::getComputeAdaptersDir ()
     ********************************************************************/
    static boost::filesystem::path getComputeAdaptersDirectoryPath () {
      const boost::filesystem::path path = boost::filesystem::path (COMPUTE_ADAPTERS_DIR) ;
      return path ;
    }

    /*********************************************************************
     *  Method: Path::getComputeAdaptersDir ()
     ********************************************************************/
    static boost::filesystem::path getVNAAdaptersDirectoryPath () {
      const boost::filesystem::path path = boost::filesystem::path (VNA_ADAPTERS_DIR) ;
      //std::cout << path.string () << std::endl ;
      return path ;
    }

    
  } ;
} // namespace Christel::Lib


#endif // LIBCRISTEL_PARSER_HPP


