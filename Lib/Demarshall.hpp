/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_DEMARSHALL_HPP
#define LIBCRISTEL_DEMARSHALL_HPP

#include <CrystalMeasurementResult.hpp>
#include <glibmm/ustring.h>
#include <CrystalStock.hpp>
#include <Deserializer.hpp>
#include <Crystal.hpp>
#include <Common.hpp>
#include <lmdb.h>

namespace Christel {
  namespace Lib {

    /*************************************************************************
     *  Function: resolveUUID
     ************************************************************************/
    unsigned char* resolveUUID (MDB_val key) {
      unsigned char* uuid = (unsigned char*)  malloc (CHRISTEL_LIB_UUID_SIZE) ;
      // TODO: preform check if key has 16 bytes size
      memcpy (uuid, key.mv_data, CHRISTEL_LIB_UUID_SIZE) ;
      return uuid ;
    }
    
    /*************************************************************************
     *  Function: demarshall
     ************************************************************************/
    CrystalStock* demarshall (MDB_val key, MDB_val value, std::function<std::vector<Crystal*>*(unsigned char* uuids, size_t n_uuids)> getCrystals) {
      Deserializer           deserializer (value) ;
      std::string            stock_name = deserializer.deserialize<std::string> () ;
      double                 f_sr       = deserializer.deserialize<double> () ;
      unsigned int           n_crystals = deserializer.deserialize<unsigned int> () ;
      unsigned char*         uuids      = (unsigned char*) malloc (n_crystals * CHRISTEL_LIB_UUID_SIZE) ;
      unsigned char*         uuid_pointer = uuids ;
      unsigned char*         uuid ;
      std::vector<Crystal*>* crystals ;

      for (unsigned int i = 0; i < n_crystals; i++) {
	uuid = deserializer.deserialize<unsigned char> (CHRISTEL_LIB_UUID_SIZE) ;
	memcpy (uuid_pointer, uuid, CHRISTEL_LIB_UUID_SIZE) ;
	uuid_pointer += CHRISTEL_LIB_UUID_SIZE ;
      }
      crystals = getCrystals (uuids, n_crystals) ;
      free (uuids) ;
      
      return new CrystalStock (resolveUUID (key), stock_name, f_sr, crystals) ;
    }

    /*************************************************************************
     *  Function: demarshall
     ************************************************************************/
    Crystal* demarshall (MDB_val key, MDB_val value,
			 std::function<CrystalMeasurementResult*(unsigned char* uuid)> getCrystalMeasurementResult) {
      Deserializer               deserializer (value) ;
      unsigned char*             crystal_measurement_result_uuid   = deserializer.deserialize<unsigned char> (CHRISTEL_LIB_UUID_SIZE) ;
      CrystalMeasurementResult*  crystal_measurement_result = getCrystalMeasurementResult (crystal_measurement_result_uuid) ;
      unsigned int               number = deserializer.deserialize<unsigned int> () ;
      if (crystal_measurement_result) {
	return new Crystal (resolveUUID (key), number, crystal_measurement_result) ;
      } else {
	return NULL ;
      }
    }
    
    /*************************************************************************
     *  Function: demarshall
     ************************************************************************/
    CrystalMeasurementResult* demarshall (MDB_val key, MDB_val value, std::function<TransmissionLossMeasurementResult*(unsigned char* uuid)> getTransmissionLossMeasurementResult) {
      Deserializer                       deserializer (value) ;
      unsigned char*                     transmission_loss_measurement_result_uuid = deserializer.deserialize<unsigned char> (CHRISTEL_LIB_UUID_SIZE) ;
      TransmissionLossMeasurementResult* transmission_loss_measurement_result = getTransmissionLossMeasurementResult (transmission_loss_measurement_result_uuid) ;
      size_t                             n_resonance_points           = deserializer.deserialize<size_t> () ;
      unsigned int*                      resonance_indexes            = deserializer.deserialize<unsigned int> (n_resonance_points) ;
      double*                            resonance_frequencies        = deserializer.deserialize<double> (n_resonance_points) ;
      double*                            resonance_losses             = deserializer.deserialize<double> (n_resonance_points) ;
      double*                            resonance_series_resistances = deserializer.deserialize<double> (n_resonance_points) ;
      double                             fixture_system_impedance     = deserializer.deserialize<double> () ;
      if (transmission_loss_measurement_result) {
	return new CrystalMeasurementResult (resolveUUID (key),
					     transmission_loss_measurement_result,
					     resonance_indexes,
					     resonance_frequencies,
					     resonance_losses,
					     resonance_series_resistances,
					     fixture_system_impedance,
					     n_resonance_points) ;
      } else {
	return NULL ;
      }
    }
    
    /*************************************************************************
     *  Function: demarshall
     ************************************************************************/
    void demarshall (MDB_val key, MDB_val value, TransmissionLossMeasurementResult*& crystal_measurement_result) {
      Deserializer  deserializer (value) ;
      unsigned int  f_start            = deserializer.deserialize<unsigned int> () ;
      unsigned int  f_stop             = deserializer.deserialize<unsigned int> () ;
      unsigned int  f_steps            = deserializer.deserialize<unsigned int> () ;
      size_t        n_samples          = deserializer.deserialize<size_t> () ;
      double*       frequencies        = deserializer.deserialize<double> (n_samples) ;
      double*       losses             = deserializer.deserialize<double> (n_samples) ;
      double*       phases             = deserializer.deserialize<double> (n_samples) ;
      double*       series_resistances = deserializer.deserialize<double> (n_samples) ;
      crystal_measurement_result = new TransmissionLossMeasurementResult (resolveUUID (key),
									  f_start,
									  f_stop,
									  f_steps,
									  frequencies,
									  losses,
									  phases,
									  series_resistances,
									  n_samples) ;
    }

    /*************************************************************************
     *  Function: demarshall
     ************************************************************************/
    void demarshall (MDB_val key, MDB_val value, CrystalImpedanceVsFrequencyComputationResult*& crystal_impedance_vs_frequency_computation_result) {
       Deserializer  deserializer (value) ;
       size_t        n_samples    = deserializer.deserialize<size_t> () ;
       double*       impedances   = deserializer.deserialize<double> (n_samples * 2) ;
       double*       frequencies  = deserializer.deserialize<double> (n_samples) ;

       crystal_impedance_vs_frequency_computation_result = new CrystalImpedanceVsFrequencyComputationResult (resolveUUID (key),
													     impedances,
													     frequencies,
													     n_samples) ;
    }
    
  } //namespace Lib
} // namespace Christel

#endif // LIBCRISTEL_DEMARSHALL_HPP
