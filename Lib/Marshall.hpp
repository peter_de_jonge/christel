/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_MARSHALL_HPP
#define LIBCRISTEL_MARSHALL_HPP

#include <CrystalStock.hpp>
#include <Serializer.hpp>
#include <Marshall.hpp>
#include <Common.hpp>
#include <iostream>
#include <string.h>
#include <string>
#include <lmdb.h>

namespace Christel {
  namespace Lib {

    /*************************************************************************
     *  Function: marshall
     ************************************************************************/
    MDB_val marshall (CrystalImpedanceVsFrequencyComputationResult* crystal_impedance_vs_frequency_computation_result) {
      Serializer   serializer ;
      double*      impedances ;
      double*      frequencies ;
      size_t       n_samples = crystal_impedance_vs_frequency_computation_result->getData (impedances, frequencies) ;
       
      serializer.serialize (n_samples) ;
      serializer.serialize (impedances, n_samples * 2) ;
      serializer.serialize (frequencies, n_samples) ;
      return serializer.mdbValue () ;
    }

		      
    /*************************************************************************
     *  Function: marshall
     ************************************************************************/
    MDB_val marshall (TransmissionLossMeasurementResult* transmission_loss_measurement_result) {
      Serializer   serializer ;
      unsigned int f_start ;
      unsigned int f_stop ;
      unsigned int f_steps ;
      double*      frequencies ;
      double*      losses ;
      double*      phases ;
      double*      series_resistances ;
      size_t       n_samples = transmission_loss_measurement_result->getData(frequencies, losses, phases, series_resistances) ;

      transmission_loss_measurement_result->getFrequencyRange (f_start, f_stop, f_steps) ;
      serializer.serialize (f_start) ;
      serializer.serialize (f_stop) ;
      serializer.serialize (f_steps) ;
      serializer.serialize (n_samples) ;
      serializer.serialize (frequencies,        n_samples) ;
      serializer.serialize (losses,             n_samples) ;
      serializer.serialize (phases,             n_samples) ;
      serializer.serialize (series_resistances, n_samples) ;
      return serializer.mdbValue () ;
    }

    /*************************************************************************
     *  Function: marshall
     ************************************************************************/
    MDB_val marshall (CrystalMeasurementResult* crystal_measurement_result) {
      Serializer     serializer ;
      unsigned char* transmission_loss_measurement_result_uuid = crystal_measurement_result->getTransmissionLossMeasurementResult ()->getUUID () ;
      double         fixture_system_impedance = crystal_measurement_result->getFixtureSystemImpedance () ;
      unsigned int*  resonance_indexes ;
      double*        resonance_frequencies ;
      double*        resonance_losses ;
      double*        resonance_series_resistances ;
      size_t         n_resonance_points = crystal_measurement_result->getSeriesResonancePoints (resonance_indexes,
												resonance_frequencies,
												resonance_losses,
												resonance_series_resistances) ;
      serializer.serialize (transmission_loss_measurement_result_uuid, CHRISTEL_LIB_UUID_SIZE) ;
      serializer.serialize (n_resonance_points) ;
      serializer.serialize (resonance_indexes,            n_resonance_points ) ;
      serializer.serialize (resonance_frequencies,        n_resonance_points) ;
      serializer.serialize (resonance_losses,             n_resonance_points ) ;
      serializer.serialize (resonance_series_resistances, n_resonance_points) ;
      serializer.serialize (fixture_system_impedance) ;
      return serializer.mdbValue () ;
    }

    /*************************************************************************
     *  Function: marshall
     ************************************************************************/
    MDB_val marshall (Crystal* crystal) {
      Serializer                                     serializer ;
      CrystalMeasurementResult*                      crystal_measurement_result ;
      unsigned int                                   number ;
      
      crystal->getData (number, crystal_measurement_result) ;
      serializer.serialize (crystal_measurement_result->getUUID (), CHRISTEL_LIB_UUID_SIZE) ;
      serializer.serialize (number) ;
      return serializer.mdbValue () ;    
    }


    /*************************************************************************
     *  Function: marshall
     ************************************************************************/
    MDB_val marshall (CrystalStock* object) {
      double                f_sr       = object->getSeriesResonanceFrequency () ;
      std::string           name       = object->getName () ;
      std::vector<Crystal*> crystals   = object->getCrystals () ;
      unsigned int          n_crystals = crystals.size () ;
      Crystal*              crystal ;
      unsigned char*        uuid ;
      Serializer            serializer ;
      serializer.serialize (name) ;
      serializer.serialize (f_sr) ;
      serializer.serialize (n_crystals) ;
      
      // Serialize crystal uuids
      for (auto crystal_iter = crystals.begin () ;
	   crystal_iter != crystals.end () ;
	   ++crystal_iter) {
	crystal = *crystal_iter ;
	 uuid = crystal->getUUID () ;
	 serializer.serialize (uuid, CHRISTEL_LIB_UUID_SIZE) ;
      }
      return serializer.mdbValue () ;
    }

  } //namespace Lib
} // namespace Christel

#endif // LIBCRISTEL_MARSHALL_HPP
