/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_COMPUTE_ADAPTER_HPP
#define LIBCRISTEL_COMPUTE_ADAPTER_HPP

#include <string>

namespace Christel::Lib {

  
  /**************************************************************************
   * Class: ComputeAdapter
   *************************************************************************/
  class ComputeAdapter {
  public:
    virtual unsigned int computeChrystalImpedances (double       L_m,
						    double       C_m,
						    double       R_m,
						    double       C_p,
						    double*      frequencies,
						    unsigned int n_frequencies,
						    double**     impedances) = 0 ;
      
    virtual unsigned int computeCrystalTransmissionLosses (double**     computed_losses,
							   unsigned int n_samples,
							   double*      frequencies,
							   double       system_impedance,
							   double       L_m,
							   double       C_m,
							   double       R_m,
							   double       C_p) = 0 ;
    virtual unsigned int filter (double**     output_samples,
				 double*      num_coefs,
				 unsigned int n_num_coefs,
				 double*      den_coefs,
				 unsigned int n_den_coefs,
				 double*      input_samples,
				 unsigned int n_samples) = 0 ;
      
    void         incUses () ;
    void         decUses () ;
    unsigned int getUses () ;
    bool         inUse () ;

  private:
    unsigned int            _uses ;

  } ;

  /**************************************************************************
   * Structure: ComputeAdapterMeta
   *************************************************************************/
  struct ComputeAdapterMeta {
    std::string name ;
    std::string version ;
  } ;
}
  
#define DEFINE_CHRISTAL_COMPUTE_ADAPTER_ENTRY_FUNCTIONS(ModuleType,ComputeAdapterMetaVar) \
									\
  extern "C" const ComputeAdapterMeta* getComputeAdapterMeta () {	\
    return &ComputeAdapterMetaVar ;					\
  }									\
    									\
  extern "C" Christel::Lib::ComputeAdapter* createComputeAdapter () {	\
    return new ModuleType () ;						\
  }									\
  									\
  extern "C" void destroyComputeAdapter (Christel::Lib::ComputeAdapter* compute_adapter) { \
    delete compute_adapter ;						\
  }									\
									\



#endif // LIBCRISTEL_COMPUTE_ADAPTER_HPP
