/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <CrystalStockManager.hpp>

namespace Christel {
  namespace Lib {

    /************************************************************************
     *  Constructor: CrystalStockManager::CrystalStockManager
     ***********************************************************************/
    CrystalStockManager::CrystalStockManager () {
    }

    /************************************************************************
     *  Method:CrystalStockManager::getStocks
     ***********************************************************************/
    std::vector<CrystalStock*> CrystalStockManager::getStocks () {
      return _crystal_stocks ;
    }

    /************************************************************************
     *  Method: CrystalStockManager::addStock
     ***********************************************************************/
    void CrystalStockManager::addStock (CrystalStock* stock) {
      // TODO: check if stock is already in vector and there are no duplicate stocknames
      _crystal_stocks.push_back (stock) ;
      _writeNewStock (stock) ;
      stock->crystal_added.connect (sigc::mem_fun (*this, &CrystalStockManager::_onCrystalAdded)) ;
      stock_added.emit (stock) ;
    }

    /************************************************************************
     *  Method: CrystalStockManager::getStockByName
     ***********************************************************************/
    CrystalStock *CrystalStockManager::getStockByName (Glib::ustring stock_name) {
      for (auto iter = _crystal_stocks.begin () ;
           iter != _crystal_stocks.end () ;
           ++iter) {
        if ((*iter)->getName () == stock_name) {
          return *iter ;
        }
      }
      return nullptr ;
    }

    /************************************************************************
     * Method: CrystalStockManager::removeStock
     ***********************************************************************/
    void CrystalStockManager::removeStock (CrystalStock* stock) {

    }
    
    /************************************************************************
     * Method: CrystalStockManager::readStocks
     ***********************************************************************/
    void CrystalStockManager::readStocks () {
      _repository.getStocks (_crystal_stocks) ;
      _connectStockHandlers () ;
    }

    /************************************************************************
     * Method: CrystalStockManager::nStocks
     ***********************************************************************/
    unsigned int CrystalStockManager::nStocks () {
      return _crystal_stocks.size () ;
    }

    /************************************************************************
     * Method: CrystalStockManager::haveStocks
     ***********************************************************************/
    bool CrystalStockManager::haveStocks () {
      return nStocks () != 0;
    }


    /************************************************************************
     * Method: CrystalStockManager::readStock
     ***********************************************************************/
    void CrystalStockManager::_writeNewStock (CrystalStock* stock) {
      _repository.writeCrystalStockRecord (stock) ;
    }
    
    /************************************************************************
     * Method: CrystalStockManager::_connectStockHandlers ()
     ***********************************************************************/
    void CrystalStockManager::_connectStockHandlers () {
      std::vector<Christel::Lib::CrystalStock *>  stocks = getStocks () ;
      Christel::Lib::CrystalStock*                stock ;
      for (auto stock_iter = stocks.begin () ;
	   stock_iter != stocks.end () ;
	   ++stock_iter) {
	stock = *stock_iter ;
	stock->crystal_added.connect (sigc::mem_fun (*this, &CrystalStockManager::_onCrystalAdded)) ;
      }
    }
      
    /************************************************************************
     * Method: CrystalStockManager::_onCrystalAdded
     ***********************************************************************/
    void CrystalStockManager::_onCrystalAdded (CrystalStock* crystal_stock, Crystal* crystal) {
      _repository.writeCrystalRecord (crystal) ;
      _repository.writeCrystalStockRecord (crystal_stock) ;
    }
    
  } // Lib
} // namespace Christel


