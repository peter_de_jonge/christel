/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_COMPUTE_HPP
#define LIBCRISTEL_COMPUTE_HPP

#include <CrystalImpedanceVsFrequencyComputationResult.hpp>
#include <CrystalMeasurementResult.hpp>
#include <ComputeAdapter.hpp>

namespace Christel {
  namespace Lib {

    class ComputeAdapterManager ;
    
    class Compute {
      friend class ComputeAdapterManager ;
    public:
      static Compute* getCompute () ;
      unsigned int computeChrystalImpedances (double       L_m,
					      double       C_m,
					      double       R_m,
					      double       C_p,
					      double*      frequencies,
					      unsigned int n_frequencies,
					      double*&     impedances) ;

      CrystalImpedanceVsFrequencyComputationResult* computeChrystalImpedances (double       L_m,
									       double       C_m,
									       double       R_m,
									       double       C_p,
									       double*      frequencies,
									       unsigned int n_frequencies) ;
      /**
       *  Compute the crystals' parameters by 3D method
       *
       *  @param The crystal measurement result
       *  @param The motional inductance L_m of the chrystal
       *  @param The motional capacitance C_m of the chrystal
       *  @param The motional resistence R_m of the chrystal
       *  @param The parallel capacitance C_p of the chrystal
       */
      void computeCrystalParametersBy3DBMethod(CrystalMeasurementResult* crystal_measurement_result,
					       double& L_m,
					       double& C_m,
					       double& R_m,
					       double& C_p) ;
      /**
       *  Compute the crystals' parameters by 3D method
       *
       *  @param The crystal measurement result
       *  @param The motional inductance L_m of the chrystal
       *  @param The motional capacitance C_m of the chrystal
       *  @param The motional resistence R_m of the chrystal
       *  @param The parallel capacitance C_p of the chrystal
       */
      // TODO: Implement Compute::_extractCrystalParametersByPhaseShiftMethod
      // void _extractCrystalParametersByPhaseShiftMethod (CrystalMeasurementResult* crystal_measurement_result,
      // 					                double&                   L_m,
      // 							double&                   C_m,
      // 							double&                   R_m,
      // 							double&                   C_p) ;

      unsigned int computeCrystalTransmissionLoss (double*&     computed_losses,
						   unsigned int n_samples,
						   double*      frequencies,
						   double       system_impedance,
						   double       L_m,
						   double       C_m,
						   double       R_m,
						   double       C_p) ;      
    private:
      ComputeAdapter* _compute_adapter ;
      
      Compute () ;
      ComputeAdapter* getComputeAdapter () ;
      void            setComputeAdapter (ComputeAdapter* compute_adapter) ;
      
    } ;
    
  } // namespace Lib
} // namespace Christel

#endif // LIBCRISTEL_COMPUTER_HPP
