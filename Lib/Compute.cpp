/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <Compute.hpp>
#include <functional>
#include <float.h>
#include <cmath>

namespace Christel::Lib {

  Compute* _compute = NULL ;
    
  /*************************************************************************
   *  Method: Compute::getCompute () ;
   ************************************************************************/
  Compute* Compute::getCompute () {
    if (!_compute) {
      _compute = new Compute () ;
    } 
    return _compute ;
  }

    
  /*************************************************************************
   *  Constructor: Compute::Compute
   ************************************************************************/
  Compute::Compute () {
    _compute_adapter = nullptr ;
  }


  /*************************************************************************
   *  Method: Compute::computeChrystalImpedances
   ************************************************************************/   
  unsigned int Compute::computeChrystalImpedances (double       L_m,
						   double       C_m,
						   double       R_m,
						   double       C_p,
						   double*      frequencies,
						   unsigned int n_frequencies,
						   double*&     impedances) {

    unsigned int n_samples = 0 ;
    if (_compute_adapter != nullptr) {
      _compute_adapter->incUses () ;
	n_samples = _compute_adapter->computeChrystalImpedances (L_m,
								 C_m,
								 R_m,
								 C_p,
								 frequencies,
								 n_frequencies,
								 &impedances) ;
      _compute_adapter->decUses () ;
    } else {
      impedances = nullptr ;
    }

    return n_samples ;
  }

  /*************************************************************************
   *  Method: Compute::computeChrystalImpedances
   ************************************************************************/   
  CrystalImpedanceVsFrequencyComputationResult* Compute::computeChrystalImpedances (double       L_m,
										    double       C_m,
										    double       R_m,
										    double       C_p,
										    double*      frequencies,
										    unsigned int n_frequencies) {
    CrystalImpedanceVsFrequencyComputationResult* computation_result = nullptr ;
    double* impedances ;
    
    if (_compute_adapter != nullptr) {
      _compute_adapter->incUses () ;
	_compute_adapter->computeChrystalImpedances (L_m,
						     C_m,
						     R_m,
						     C_p,
						     frequencies,
						     n_frequencies,
						     &impedances) ;
      

      computation_result = new CrystalImpedanceVsFrequencyComputationResult (impedances, frequencies, n_frequencies) ;
      _compute_adapter->decUses () ;
    }

    return computation_result ;
  }

    
  /*************************************************************************
   *  Method: Compute::computeCrystalParametersBy3DBMethod
   ************************************************************************/
  void Compute::computeCrystalParametersBy3DBMethod(CrystalMeasurementResult* crystal_measurement_result,
						    double& L_m,
						    double& C_m,
						    double& R_m,
						    double& C_p) {
    double                             fixture_system_impedance             = crystal_measurement_result->getFixtureSystemImpedance () ;
    TransmissionLossMeasurementResult* _transmission_loss_measurement_result = crystal_measurement_result->getTransmissionLossMeasurementResult () ;
    unsigned int*                      _resonance_indexes ;
    double*                            _resonance_frequencies ;
    double*                            _resonance_losses ;
    double*                            _resonance_series_resistances ;
    double*                            _series_resistances ;
    double*                            _frequencies ;
    double*                            _losses ;
    double*                            _phases ;

    crystal_measurement_result->getSeriesResonancePoints (_resonance_indexes,
							  _resonance_frequencies,
							  _resonance_losses,
							  _resonance_series_resistances) ;
           
    unsigned int _n_samples                          = _transmission_loss_measurement_result->getData(_frequencies, _losses, _phases, _series_resistances) ;
    double       _min_loss                           = _resonance_losses [0] ;
    double       _max_loss                           = _resonance_losses [1] ;
    double       _f_series_resonance                 = _resonance_frequencies [0] ;
    double       _f_parallel_resonance               = _resonance_frequencies[1] ;
    int          _i_min_loss                         = _resonance_indexes[0] ;
    int          _i_max_loss                         = _resonance_indexes[1] ;
    double       _R_g                                = fixture_system_impedance ;
    double       _cumulative_loss_curve_diff_current = DBL_MAX ;
    double       _cumulative_loss_curve_diff_new     = DBL_MAX ;
    double       C_p_estimate                        = 0.0D ;
    double       _R_ref                              = 0.0D ;
    int          _i_left_3dB                         = 0 ;
    int          _i_right_3dB                        = 0 ;
    double       _3dB_level ;
    double       _bandwidth ;
    double       _C_h ;
    int          _i ;
    double       _q ;

      
    // Determine -3dB Level
    _3dB_level  = _min_loss - 3.0 ;
    std::cout << "_3dB_level: " << _3dB_level << std::endl ;
    std::cout << "_min_loss: " << _min_loss << std::endl ;

    // Compute left 3dB point
    _i = _i_min_loss ; 
    while ((_i_left_3dB == 0) && (_i > 0)) {
      if (_losses[_i] <= _3dB_level) {
	_i_left_3dB = _i ;
      }
      _i-- ;
    }
      
    // Computer right 3dB point
    _i = _i_min_loss ;
    while ((_i_right_3dB == 0) && (_i < _n_samples)) {
      if (_losses[_i] <= _3dB_level) {
	_i_right_3dB = _i ;
      }
      _i++ ;
    }
  
    R_m = 2.0D * _R_g * (pow(10, (abs(_min_loss) / 20.0D) - 1.0D)) ;
    //      R_m = _resonance_series_resistances[0] ;
    _R_ref = 2.0D * _R_g + R_m ;
    _bandwidth = _frequencies[_i_right_3dB] - _frequencies[_i_left_3dB] ;
    _q = _f_series_resonance/_bandwidth ;
    L_m = _q * _R_ref / (2 * M_PI * _f_series_resonance) ;
    C_m=  1 / (4 * pow (M_PI, 2) * pow (_f_series_resonance,2) * L_m) ;
    C_p_estimate = (C_m * pow(_f_series_resonance,2)) / (pow (_f_parallel_resonance, 2) - pow (_f_series_resonance,2)) ;      
    C_p = C_p_estimate ;
    
    // std::cout << "L_M=" << L_m << std::endl ;
    // std::cout << "C_M=" << C_m << std::endl ;
    // std::cout << "C_P=" << C_p << std::endl ;
    // std::cout << "R_M=" << R_m << std::endl ;
  }


  /*************************************************************************
   *  Method: Compute::computeCrystalTransmissionLoss
   ************************************************************************/
  unsigned int Compute::computeCrystalTransmissionLoss (double*&     computed_losses,
							unsigned int n_samples,
							double*      frequencies,
							double       system_impedance,
							double       L_m,
							double       C_m,
							double       R_m,
							double       C_p) {
    
    unsigned int _n_samples = 0 ;
    if (_compute_adapter != nullptr) {
      _compute_adapter->incUses () ;
	_n_samples = _compute_adapter->computeCrystalTransmissionLosses (&computed_losses,
									 n_samples,
									 frequencies,
									 system_impedance,
									 L_m,
									 C_m,
									 R_m,
									 C_p) ;
	
      _compute_adapter->decUses () ;
    } else {
      computed_losses = nullptr ;
    }
    
    return _n_samples ;
  }

  
  /*************************************************************************
   *  Method: Compute::getComputeAdapter
   ************************************************************************/
  ComputeAdapter* Compute::getComputeAdapter () {
    return _compute_adapter ;
  }

  /*************************************************************************
   *  Method: Compute::setComputeAdapter
   ************************************************************************/
  void Compute::setComputeAdapter (ComputeAdapter* compute_adapter) {
    _compute_adapter = compute_adapter ;
  }
    
} // namespace Christel::Lib

