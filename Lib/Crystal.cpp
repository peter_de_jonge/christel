/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <Compute.hpp>
#include "Crystal.hpp"

namespace Christel {
  namespace Lib {

    /*****************************************************************************
     *  Constructor: Crystal::Crystal
     *****************************************************************************/
    Crystal::Crystal (unsigned int                                   number,
                      CrystalMeasurementResult*                      crystal_measurement_result) :
      Model (),
      _number (number),
      _crystal_measurement_result (crystal_measurement_result) {
    }

    /*****************************************************************************
     *  Constructor: Crystal::Crystal
     *****************************************************************************/
    Crystal::Crystal (unsigned char*                                 uuid,
                      unsigned int                                   number,
                      CrystalMeasurementResult*                      crystal_measurement_result) :

      Model (uuid),
      _number (number),
      _crystal_measurement_result (crystal_measurement_result) {
    }
    
    /*****************************************************************************
     *  Method: Crystal::getNumber
     *****************************************************************************/
    unsigned int Crystal::getNumber () {
      return _number ;
    }

    /*****************************************************************************
     *  Method: Crystal::getCrystalMeasurementResult
     *****************************************************************************/
    CrystalMeasurementResult* Crystal::getCrystalMeasurementResult () {
      return _crystal_measurement_result ;
    }

    /*****************************************************************************
     *  Method: Crystal::getFSerRes
     *****************************************************************************/
    double Crystal::getFSerRes () {
      unsigned int* resonance_indexes ;
      double*       frequencies ;
      double*       losses ;
      double*       series_resistances ;
      _crystal_measurement_result->getSeriesResonancePoints (resonance_indexes, frequencies, losses, series_resistances) ;
      return frequencies[0] ;			
    }

    /*****************************************************************************
     *  Method: Crystal::getFParRes
     *****************************************************************************/
    double Crystal::getFParRes () {
      unsigned int* resonance_indexes ;
      double*       frequencies ;
      double*       losses ;
      double*       series_resistances ;
      _crystal_measurement_result->getSeriesResonancePoints (resonance_indexes, frequencies, losses, series_resistances) ;
      return frequencies[1] ;
    }

    /*****************************************************************************
     *  Method: Crystal::getData
     *****************************************************************************/
    void Crystal::getData (unsigned int&                                   number,
                           CrystalMeasurementResult*&                      crystal_measurement_result) {
      number                      = _number ;
      crystal_measurement_result  = _crystal_measurement_result ;
    }


    /*****************************************************************************
     *  Method: Crystal::computeParameters
     *****************************************************************************/
    void Crystal::computeParameters (ParameterComputationMethod computation_method,
				     double&                    L_m,
				     double&                    C_m,
				     double&                    R_m,
				     double&                    C_p) {

      Compute* _compute = Compute::getCompute () ;
      
      _compute->computeCrystalParametersBy3DBMethod (_crystal_measurement_result, L_m, C_m, R_m, C_p) ;
    }

    /*****************************************************************************
     *  Method: Crystal::computeTransmissionLoss
     *****************************************************************************/
    unsigned Crystal::computeTransmissionLoss (ParameterComputationMethod parameter_computation_method,
					       double*&                   computed_losses,
					       double*&                   frequencies) {

      TransmissionLossMeasurementResult*  transmission_loss_measurement_result = _crystal_measurement_result->getTransmissionLossMeasurementResult () ;
      double                              system_impedance                     = _crystal_measurement_result->getFixtureSystemImpedance () ;
      Compute*                            compute                              = Compute::getCompute () ;
      double*                             series_resistances ;
      double*                             losses ;
      double*                             phases ;
      double                              L_m ;
      double                              C_m ;
      double                              R_m ;
      double                              C_p ;
      unsigned int                        n_samples = transmission_loss_measurement_result->getData (frequencies,
												     losses,
												     phases,
												     series_resistances) ;



      
      computeParameters (parameter_computation_method, L_m, C_m, R_m, C_p) ;
    
      n_samples = compute->computeCrystalTransmissionLoss (computed_losses,
      							   n_samples,
      							   frequencies,
      							   system_impedance,
      							   L_m,
      							   C_m,
      							   R_m,
      							   C_p) ;
      return n_samples ;
    }

  } // namespace LibChristel
}
