/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#include <TransmissionLossMeasurementController.hpp>
#include <Path.hpp>
#include <math.h>


namespace Christel::Lib {

  TransmissionLossMeasurementController* _transmission_loss_measurement_controller = NULL ;
    
  /*************************************************************************
   *  Method: TransmissionLossMeasurementController::getTransmissionLossMeasurementController
   ************************************************************************/
  TransmissionLossMeasurementController* TransmissionLossMeasurementController::getTransmissionLossMeasurementController () {
    if (!_transmission_loss_measurement_controller) {
      _transmission_loss_measurement_controller = new TransmissionLossMeasurementController () ;
    } 
    return _transmission_loss_measurement_controller ;
  }
    
  /*************************************************************************
   * Contructor: TransmissionLossMeasurementController::TransmissionLossMeasurementController
   ************************************************************************/
  TransmissionLossMeasurementController::TransmissionLossMeasurementController () {
  }

  /*************************************************************************
   * Method: TransmissionLossMeasurementController::do_measure
   ************************************************************************/
  TransmissionLossMeasurementResult* TransmissionLossMeasurementController::doMeasure (unsigned int f_start,
										       unsigned int f_steps,
										       unsigned int f_stop) {
    double*                             series_resistances   = nullptr ;
    TransmissionLossMeasurementResult*  measurement_result   = nullptr ;
    double*                             frequencies          = nullptr ;
    double*                             losses               = nullptr ;
    double*                             phases               = nullptr ;
    unsigned int                        n_res_points         = 0 ;
    int                                 n_samples            = 0 ;


    if (_vna_adapter != nullptr) {
      n_samples = _vna_adapter->doTransmissionLossMeasurement (f_start,
							       f_steps,
							       f_stop,
							       frequencies,
							       losses,
							       phases,
							       series_resistances) ;
	measurement_result = new TransmissionLossMeasurementResult (f_start,
								    f_stop,
								    f_steps,
								    frequencies,
								    losses,
								    phases,
								    series_resistances,
								    n_samples) ;
    }			
    return measurement_result ;
  }

  /*************************************************************************
   * Method: TransmissionLossMeasurementController::getVNAAdapter
   ************************************************************************/
  VNAAdapter* TransmissionLossMeasurementController::getVNAAdapter () {
    return _vna_adapter ;
  }

  /*************************************************************************
   * Method: TransmissionLossMeasurementController::setVNAAdapter
   ************************************************************************/
  void TransmissionLossMeasurementController::setVNAAdapter (VNAAdapter* vna_adapter) {
    _vna_adapter = vna_adapter ;
  }

} // namespace Lib::Christel

