/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef CHRISTEL_LIB_VNA_ADAPTER_MANAGER_HPP
#define CHRISTEL_LIB_VNA_ADAPTER_MANAGER_HPP

#include <VNAAdapterModule.hpp>
#include <VNAAdapter.hpp>
#include <sigc++/sigc++.h>

namespace Christel::Lib {

  class VNAAdapterManager {
  public:
    void                           getActiveVNAAdapter (VNAAdapter*& vna_adapter, VNAAdapterMeta*& vna_adapter_meta) ;
    void                           setActiveVNAAdapterModule (VNAAdapterModule* vna_adapter_module) ;    
    static VNAAdapterManager*      getVNAAdapterManager () ;
    std::vector<VNAAdapterModule*> getVNAAdapterModules () ;


    /******************************************************************************
     * Signal: active_adapter_changed
     *****************************************************************************/
    sigc::signal<void, VNAAdapter*, VNAAdapterMeta*> active_adapter_changed ;
    
  private:
    VNAAdapter*                    _current_adapter ; 
    VNAAdapterModule*              _current_module ;
    std::vector<VNAAdapterModule*> _modules ;
    VNAAdapterManager () ;

    void                    _resolveModules (boost::filesystem::path module_dir_path) ;
    boost::filesystem::path _getVNAAdaptersDirectoryPath () ;
  } ;
  
} // namespace Christel::Lib

#endif // CHRISTEL_LIB_VNA_ADAPTER_MANAGER_HPP
