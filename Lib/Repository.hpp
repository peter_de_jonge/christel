/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#ifndef LIBCRISTEL_REPOSITORY_HPP
#define LIBCRISTEL_REPOSITORY_HPP

#include <CrystalStock.hpp>
#include <Repository.hpp>
#include <uuid/uuid.h>
#include <Crystal.hpp>
#include <Common.hpp>
#include <lmdb.h>
#include <vector>

namespace Christel {
  namespace Lib {

    class Repository {
    public:
      Repository () ;
      ~Repository () ;
      void                   getStocks (std::vector<CrystalStock*>&) ;
      void                   writeCrystalStockRecord (CrystalStock* stock) ;
      void                   writeCrystalRecord (Crystal* crystal) ;
      
    private:  
      MDB_env*                                       _env_handle ;
      MDB_txn*                                       _txn ;
      MDB_val                                        _resolveKey (unsigned char* uuid) ;
      CrystalStock*                                  _resolveStock (MDB_val key, MDB_val value) ;
      std::vector<Crystal*>*                         _resolveCrystals (char unsigned* uuids, size_t n_uuids) ;
      Crystal*                                       _resolveCrystal (char unsigned* uuid) ;
      CrystalMeasurementResult*                      _resolveCrystalMeasurementResult (char unsigned* uuid) ;
      TransmissionLossMeasurementResult*             _resolveTransmissionLossMeasurementResult (char unsigned* uuid) ;
      CrystalImpedanceVsFrequencyComputationResult*  _resolveCrystalImpedanceVsFrequencyComputationResult (char unsigned* uuid) ;
    } ;
      
      
  } //namespace Lib
} // namespace Christel

#endif // LIBCRISTEL_REPOSITORY_HPP
