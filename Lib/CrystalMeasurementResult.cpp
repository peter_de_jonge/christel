/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <CrystalMeasurementResult.hpp>

namespace Christel {
  namespace Lib {
		
    /*************************************************************************
     * Method: CrystalMeasurementResult::CrystalMeasurementResult
     ************************************************************************/
    CrystalMeasurementResult::CrystalMeasurementResult (TransmissionLossMeasurementResult* transmission_loss_measurement_result,
                                                        unsigned int*                      resonance_indexes,
                                                        double*                            resonance_frequencies,
                                                        double*                            resonance_losses,
                                                        double*                            resonance_series_resistances,
							double                             fixture_system_impedance,
                                                        unsigned int                       n_resonance_points) :
      Model (),
      _transmission_loss_measurement_result (transmission_loss_measurement_result),
      _resonance_indexes (resonance_indexes),
      _resonance_frequencies (resonance_frequencies),
      _resonance_losses (resonance_losses),
      _resonance_series_resistances (resonance_series_resistances),
      _fixture_system_impedance (fixture_system_impedance),
      _n_resonance_points (n_resonance_points) {
    }

    /*************************************************************************
     * Method: CrystalMeasurementResult::CrystalMeasurementResult
     ************************************************************************/
    CrystalMeasurementResult::CrystalMeasurementResult (unsigned char*                     uuid,
							TransmissionLossMeasurementResult* transmission_loss_measurement_result,
							unsigned int*                      resonance_indexes,
							double*                            resonance_frequencies,
							double*                            resonance_losses,
							double*                            resonance_series_resistances,
							double                             fixture_system_impedance,
							unsigned int                       n_resonance_points) :
      Model (uuid),
      _transmission_loss_measurement_result (transmission_loss_measurement_result),
      _resonance_indexes (resonance_indexes),
      _resonance_frequencies (resonance_frequencies),
      _resonance_losses (resonance_losses),
      _resonance_series_resistances (resonance_series_resistances),
      _fixture_system_impedance (fixture_system_impedance),
      _n_resonance_points (n_resonance_points) {
    }

    /*************************************************************************
     * Method: CrystalMeasurementResult::getPrescanResult
     ************************************************************************/
    TransmissionLossMeasurementResult* CrystalMeasurementResult::getTransmissionLossMeasurementResult () {
      return _transmission_loss_measurement_result ;
    }	

    /***************************************************************************
     *  Method:  CrystalMeasurementResult::getSeriesResonancePoints
     **************************************************************************/
    unsigned int CrystalMeasurementResult::getSeriesResonancePoints (unsigned int*& resonance_indexes,
                                                                     double*&       resonance_frequencies,
                                                                     double*&       resonance_losses,
                                                                     double*&       resonance_series_resistances) {
      resonance_indexes              = _resonance_indexes ;
      resonance_frequencies          = _resonance_frequencies ;
      resonance_losses               = _resonance_losses ;
      resonance_series_resistances   = _resonance_series_resistances ;
      return _n_resonance_points ;
    }

    /***************************************************************************
     *  Method: CrystalMeasurementResult::getFixtureSystemImpedance
     **************************************************************************/
    double CrystalMeasurementResult::getFixtureSystemImpedance () {
      return _fixture_system_impedance ;
    }
		
  } // namespace Lib
} // namespace Christel
