/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_DESERIALIZER_HPP
#define LIBCRISTEL_DESERIALIZER_HPP

#include <string.h>
#include <cstring>
#include <string>
#include <lmdb.h>

namespace Christel {
  namespace Lib {

    class Deserializer {
    public:
      Deserializer (MDB_val value) ;
      
      template <typename type>
      type deserialize () ;

      char* deserialize () ;
     
      template <typename type>
      type* deserialize (size_t array_length) ;
      
    private:
      void* _current_point ;
      
      template <typename type>
      type* _deserializeArray (size_t array_length) ;
    } ; 


    /*************************************************************************
     *  Constructor: Deserializer::Deserializer
     ************************************************************************/
    Deserializer::Deserializer  (MDB_val value) {
      _current_point = value.mv_data ;
    } ;


  
    /*************************************************************************
     *  Method: Deserializer::deserialize
     ************************************************************************/
    template <typename type>
    type Deserializer::deserialize () {
      type value = *((type*) _current_point) ;
      _current_point = (void*) (((size_t) _current_point) + sizeof (type)) ;
      return value ;
    }

    /*************************************************************************
     *  Method: Deserializer::deserialize
     ************************************************************************/
    template <typename type>
    type* Deserializer::deserialize (size_t array_length) {
      type* value = _deserializeArray<type> (array_length) ;
      return value ;
    }
    
    /*************************************************************************
     *  Method: Deserializer::deserialize
     ************************************************************************/
    template <>
    std::string Deserializer::deserialize <std::string>() {
      char*       cstring = deserialize () ;
      std::string value (cstring) ;
      free (cstring) ;
      return value ;
    }

    /*************************************************************************
     *  Method: Deserializer::deserialize
     ************************************************************************/
    char* Deserializer::deserialize () {      
      char* value = _deserializeArray<char> ((size_t) (strlen ((char*) _current_point) + 1)) ;
      return value ;
    }

    // /*************************************************************************
    //  *  Method: Deserializer::deserialize
    //  ************************************************************************/
    //  const char* Deserializer::deserialize () {
    //    size_t array_length = strlen (value) + 1 ; // include c string delimiter
    //    serialize (value, array_length) ;
    // }
    
    
    /*************************************************************************
     *  Method: Deserializer::_deserializeArray
     ************************************************************************/
    template <typename type>
    type* Deserializer::_deserializeArray (size_t array_length) {
      size_t array_size = array_length * sizeof (type) ;
      type* value = (type*) malloc (array_size) ;
      memcpy (value, _current_point, array_size) ;
      _current_point = (void*) (((size_t) _current_point) + array_size) ;
      return value ;
    }
    
  } // namespace Lib
} // namespace Christel

#endif // LIBCRISTEL_DESERIALIZER_HPP
