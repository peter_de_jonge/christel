/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_TRANSMISSION_LOSS_MEASUREMENT_RESULT_HPP
#define LIBCRISTEL_TRANSMISSION_LOSS_MEASUREMENT_RESULT_HPP

#include <Model.hpp>

namespace Christel {
  namespace Lib {
		
    // Forward declaration TransmissonLossMeasurement
    class TransmissionLossMeasurementController ;

    class TransmissionLossMeasurementResult : public Model {
      friend TransmissionLossMeasurementController ;
    public:
      TransmissionLossMeasurementResult (unsigned int  f_start,
                                         unsigned int  f_stop,
                                         unsigned int  f_steps,
                                         double*       frequencies,
                                         double*       losses,
					 double*       phases,
                                         double*       series_resistances,
                                         unsigned int  n_samples) ;

      TransmissionLossMeasurementResult (unsigned char* uuid,
                                         unsigned int  f_start,
                                         unsigned int  f_stop,
                                         unsigned int  f_steps,
                                         double*       frequencies,
                                         double*       losses,
                                         double*       phases,
                                         double*       series_resistances,
                                         unsigned int  n_samples) ;
      unsigned int getData(double*& frequencies,
                           double*& losses,
			   double*& phases,
                           double*& resistances) ;

      unsigned int getFrequencies (double*& frequencies) ;
      
      void getFrequencyRange(unsigned int& f_start,
                             unsigned int& f_stop,
                             unsigned int& f_steps) ;
			
    private:
      unsigned int _f_start ;
      unsigned int _f_stop ;
      unsigned int _f_steps ;
      double*      _frequencies ;
      double*      _losses ;
      double*      _phases ;
      double*      _series_resistances ;
      unsigned int _n_samples ;			
    } ;
  } // namespace Lib
} // namespace Christel

#endif // TRANSMISSION_LOSS_MEASUREMENT_RESULT
