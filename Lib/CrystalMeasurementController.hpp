/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_CRYSTAL_MEASUREMENT_HPP
#define LIBCRISTEL_CRYSTAL_MEASUREMENT_HPP

#include <TransmissionLossMeasurementController.hpp>
#include <CrystalMeasurementResult.hpp>
#include <CrystalStock.hpp>
#include <sigc++/sigc++.h>
#include <Compute.hpp>
#include <Crystal.hpp>
#include <glibmm.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <string>
#include <thread>
#include <mutex>



namespace Christel {
  namespace Lib {
    
    class CrystalMeasurementController {
    public:
      CrystalMeasurementController (CrystalStock* crystal_stock,
				    double        fixture_system_impedance) ;
      Crystal* doMeasure (unsigned int crystal_number) ;
      Crystal* doMeasure (bool fill_gap_in_ids) ;

      		
    private:
      TransmissionLossMeasurementController* _transmission_loss_measurement_controller ;
      double                                  _fixture_system_impedance ;
      CrystalStock*                          _crystal_stock ;
      
      void                                   _resolveFrequencyRange (unsigned int approximate_series_resonance_frequency,
                                                                     unsigned int& _f_start,
                                                                     unsigned int& _f_stop,
                                                                     unsigned int& _f_steps) ;
      unsigned int                           _resolveResonancePoints (TransmissionLossMeasurementResult* transmission_loss_measurement_result,
                                                                      unsigned int*& resonance_indexes,
                                                                      double*&       resonance_frequencies,
                                                                      double*&       resonance_losses,
                                                                      double*&       resonance_resistances) ;
      void 	                             _extractCrystalParametersBy3DBMethod (CrystalMeasurementResult* crystal_measurement_result,
										   double& L_m,
										   double& C_m,
										   double& R_m,
										   double& C_p) ;
      void 	                             _extractCrystalParametersByPhaseShiftMethod (CrystalMeasurementResult* crystal_measurement_result,
											  double& L_m,
											  double& C_m,
											  double& R_m,
											  double& C_p) ;
      double                                 _gauss (double sigma, double x) ;
      std::vector<double>                    _gaussKernel (int samples, double sigma) ;
      std::vector<double>                    _gaussSmoothen (std::vector<double> values,
                                                             double              sigma,
                                                             int                 samples) ;
      std::vector<double>                    _cArrayToVector (double* c_array, int     size) ;
      int                                    _vectorToCArray (std::vector<double> vector,
                                                              double**            c_array) ;
      int                                    _cGaussSmoothen (double*  in_values,
                                                              double** out_values,
                                                              int      n_samples,
                                                              double   sigma,
                                                              int      c) ;
					
    } ;

  } //namepace Lib
} // namespace LibChristel

#endif // LIBCRISTEL_CRYSTAL_MEASUREMENT_HPP
