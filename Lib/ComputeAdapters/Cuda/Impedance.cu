/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include "Cuda.h"
#include <math.h>
#include <stdio.h>
#include <iostream>

/*****************************************************************************
 *  Function: _christelCudaComputeCrystalImpedance
 ****************************************************************************/
__global__
void _christelCudaComputeCrystalImpedance (double        L_m,
					   double        C_m,
					   double        R_m,
					   double        C_p,
					   double*       frequencies,
					   unsigned int  n_frequencies,
					   double*       impedances) {
  int frequency_index = blockIdx.x * blockDim.x + threadIdx.x ;
  int impedance_index = frequency_index * 2 ;
  double numerator_real ; // teller in Dutch
  double numerator_imag ; // teller in Dutch
  double denominator ;    // noemer in Dutch
  double X_L_m ;
  double X_C_m ;
  double X_C_p ;
  if (frequency_index < n_frequencies) {
    X_L_m = 2 * CHRISTEL_CUDA_PI * frequencies[frequency_index] * L_m ;
    X_C_m = -1.0 / (2 * CHRISTEL_CUDA_PI * frequencies[frequency_index] * C_m) ;
    X_C_p = -1.0 / (2 * CHRISTEL_CUDA_PI * frequencies[frequency_index] * C_p) ;
    numerator_real = R_m * pow (X_C_p, 2) ;
    numerator_imag = pow (R_m,2) * X_C_p + X_C_p * (X_L_m + X_C_m) * (X_L_m + X_C_m + X_C_p) ;
    denominator    = pow (R_m,2) + pow ((X_L_m + X_C_m + X_C_p), 2) ;
    impedances[impedance_index] = numerator_real / denominator ;
    impedances[impedance_index + 1] = numerator_imag / denominator ;
  }
}

/*****************************************************************************
 *  Function: christelCudaComputeCrystalImpedanceVsFrequency
 ****************************************************************************/
unsigned int christelCudaComputeCrystalImpedanceVsFrequency (double       L_m,
							     double       C_m,
							     double       R_m,
							     double       C_p,
							     double*      frequencies,
							     unsigned int n_frequencies,
							     double**     impedances) {

  double*       d_frequencies ;
  double*       d_impedances ;
  unsigned int  frequencies_size = n_frequencies * sizeof(double) ;
  
  // Impedance vector is interleaved with real and imaginary values
  unsigned int  impedances_size = frequencies_size * 2 ;
  
  // Allocate host memory
  *impedances = (double*) malloc (impedances_size) ;

  // Allocate device memory
  cudaMalloc (&d_frequencies, n_frequencies * sizeof(double)) ;
  cudaMalloc (&d_impedances, impedances_size) ;
  
  // Copy vectors from host memory to device memory
  cudaMemcpy (d_frequencies, frequencies, frequencies_size, cudaMemcpyHostToDevice) ;

   // Invoke kernel
  int threadsPerBlock = 256 ;
  int numberOfBlocks = (int) ceil ((double)n_frequencies / (double)threadsPerBlock) ;
  
  _christelCudaComputeCrystalImpedance<<<numberOfBlocks, threadsPerBlock>>> (L_m,
									     C_m,
									     R_m,
									     C_p,
									     d_frequencies,
									     n_frequencies,
									     d_impedances) ;
  // Copy result from device memory to host memory
  // h_C contains the result in host memory
  cudaMemcpy (*impedances, d_impedances, impedances_size, cudaMemcpyDeviceToHost) ;
  
  
  // Free device memory
  cudaFree (d_impedances) ;
  cudaFree (d_frequencies) ;
    
  return impedances_size ;
}


// const unsigned int  christel_cuda_oversampling_per_chrystal = 100 ;

// __global__   
// void christel_cuda_compute_crystal_resonance_frequency (unsigned int    n,
// 							double*      L_ms,
// 							double*      C_ms,
// 							double*    f_ress)
// {
//     int index = blockIdx.x * blockDim.x + threadIdx.x ;
//     int stride = blockDim.x * gridDim.x ;
//     for (int i = index; i < n; i += stride)
//     {
// 	f_ress[i] = 1 / (2 * L_ms[i] * C_ms[i]) ;
//     }
// }


// // Kernel function to add the elements of two arrays
// __global__
// void christel_cuda_compute_frequency_sample (int n,
// 					     double f_min,
// 					     double f_step,
// 					     double *frequencies)
// {
//     int index = blockIdx.x * blockDim.x + threadIdx.x ;
//     int stride = blockDim.x * gridDim.x ;
//     for (int i = index; i < n; i += stride)
//     {
// 	frequencies[i] = f_min + i * f_step ;
//     }
// }

       
// double christel_cuda_compute_average_crystal_resonance_frequency (double*       L_ms,
// 								  double*       C_ms,
// 								  int           n_crystals)
// {
//     double                          average_f_res = 0 ;
//     unsigned int                                i = 0 ;
//     double*                                 freqs ;
    
//     // Allocate Unified Memory – accessible from CPU or GPU
//     cudaMallocManaged (&freqs, n_crystals * sizeof (double)) ;

//     int blockSize = 1 ; //n_crystals ; // 128 ;
//     int numBlocks = 1 ; //(n_crystals + blockSize - 1) / blockSize ;
      
//     christel_cuda_compute_crystal_resonance_frequency<<<numBlocks, blockSize>>> (n_crystals, L_ms, C_ms, freqs) ;
    
//     // Wait for GPU to finish before accessing on host
//     cudaDeviceSynchronize() ;
   
//     for (i = 0; i < n_crystals; i++)
//     {
// 	printf ("%12.10e", freqs[i]) ;
// 	average_f_res = average_f_res + freqs[i] ;
//     }
    
//     average_f_res =  average_f_res / n_crystals ;
    
//     // Free memory
//     cudaFree (freqs) ;
    
//     return average_f_res ;
// }


// double* christel_cuda_compute_frequencies (double        average_crystal_resonance_frequency,
// 					   int           n_crystals_in_filter,
// 					   double        filter_bandwidth,
// 					   unsigned int* n_frequencies)

					   
// {
//     unsigned int n_f = (unsigned int) std::ceil (filter_bandwidth *
// 						 christel_cuda_oversampling_per_chrystal *
// 						 n_crystals_in_filter) ;
//     double     f_min = average_crystal_resonance_frequency - filter_bandwidth ;
//     double     f_max = average_crystal_resonance_frequency + filter_bandwidth ;
//     double    f_span = f_max - f_min ;
//     double    f_step = f_span / n_f ;
//     double* frequencies ;
    
//     // Allocate Unified Memory – accessible from CPU or GPU
//     cudaMallocManaged (&frequencies, n_f * sizeof (double)) ;
 
        
//     int blockSize = 128 ;
//     int numBlocks = (n_f + blockSize - 1) / blockSize;
      
//     christel_cuda_compute_frequency_sample<<<numBlocks, blockSize>>> (n_f, f_min, f_step, frequencies) ;
    
//     // Wait for GPU to finish before accessing on host
//     cudaDeviceSynchronize() ;
//     //}
    
//     *n_frequencies = n_f ;
//     return frequencies ;
// }


// void christel_cuda_compute_optimum_crystal_filter_sets (unsigned int* Ids,
// 							double*       L_ms,
// 							double*       C_ms,
// 							double*       R_ms,
// 							double*       C_ps,
// 							int           n_crystals,
// 							int           n_crystals_in_filter,
// 							double        filter_bandwidth)
//     {
// 	double average_crystal_resonance_frequency = christel_cuda_compute_average_crystal_resonance_frequency (L_ms,
// 														C_ms,
// 														n_crystals) ;
														 
// 	printf ("\n\nAverage crystal resonance frequency:  %12.10e\n", average_crystal_resonance_frequency) ;

// 	unsigned int n_frequencies ;
	
// 	double* frequencies = christel_cuda_compute_frequencies (average_crystal_resonance_frequency,
// 								 n_crystals_in_filter,
// 								 filter_bandwidth,
// 								 &n_frequencies) ;
// 	// unsigned int i ;
	
// 	// printf ("\n\nFrequencies:\n") ;
// 	// for (i = 0; i < n_frequencies; i++)
// 	// {
// 	//     printf ("%12.10e\n", frequencies[i]) ;
// 	// }

//     }
