/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include "Impedance.hpp"
#include "Constants.hpp"
#include <math.h>


namespace Christel::Lib::DefaultComputeAdapter {

  /*****************************************************************************
   *  Function: christelDefaultComputeChrystalImpedance
   ****************************************************************************/
  void christelDefaultComputeChrystalImpedance (double   L_m,
						double   C_m,
						double   R_m,
						double   C_p,
						double   frequency,
						double*  impedance) {
    double numerator_real ; // teller in Dutch
    double numerator_imag ; // teller in Dutch
    double denominator ;    // noemer in Dutch
    double X_L_m ;
    double X_C_m ;
    double X_C_p ;
    X_L_m          = 2 * CHRISTEL_DEFAULT_PI * frequency * L_m ;
    X_C_m          = -1.0 / (2 * CHRISTEL_DEFAULT_PI * frequency * C_m) ;
    X_C_p          = -1.0 / (2 * CHRISTEL_DEFAULT_PI * frequency * C_p) ;
    numerator_real = R_m * pow (X_C_p, 2) ;
    numerator_imag = pow (R_m,2) * X_C_p + X_C_p * (X_L_m + X_C_m) * (X_L_m + X_C_m + X_C_p) ;
    denominator    = pow (R_m,2) + pow ((X_L_m + X_C_m + X_C_p), 2) ;
    impedance[0]   = numerator_real / denominator ;
    impedance[1]   = numerator_imag / denominator ;
  }

  
  /*****************************************************************************
   *  Function: christelDefaultComputeChrystalImpedances
   ****************************************************************************/
  unsigned int christelDefaultComputeChrystalImpedances (double       L_m,
							 double       C_m,
							 double       R_m,
							 double       C_p,
							 double*      frequencies,
							 unsigned int n_frequencies,
							 double**     impedances) {
    // Impedance vector is interleaved with real and imaginary values
    size_t       impedances_size =  2 *n_frequencies * (sizeof (double)) ;
    double*      impedance_iter ;
    unsigned int i = 0 ;

    *impedances = (double*) malloc (impedances_size) ;
    impedance_iter = *impedances ;
      
    for (i = 0; i < n_frequencies; i++) {
      christelDefaultComputeChrystalImpedance (L_m,
					       C_m,
					       R_m,
					       C_p,
					       frequencies[i],
					       impedance_iter) ;
      impedance_iter += 2 ;           
    }
    return n_frequencies ;
  }
  
  
} // namespace Christel::Lib::DefaultComputeAdapter
