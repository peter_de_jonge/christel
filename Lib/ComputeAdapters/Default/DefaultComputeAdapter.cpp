/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include "DefaultComputeAdapter.hpp"
#include "TransmissionLoss.hpp"
#include "Impedance.hpp"
#include "Filter.hpp"

namespace Christel::Lib::DefaultComputeAdapter {

  const ComputeAdapterMeta AdapterMeta = {"Default", "0.0.0"} ;
			    
  DEFINE_CHRISTAL_COMPUTE_ADAPTER_ENTRY_FUNCTIONS(DefaultComputeAdapter,AdapterMeta) ;
  
  /*************************************************************************
   *  Constructor: DefaultComputeAdapter::DefaultComputeAdapter
   ************************************************************************/
  DefaultComputeAdapter::DefaultComputeAdapter () {
  }

    
  /*************************************************************************
   *  Method: DefaultComputeAdapter::computeChrystalImpedances
   ************************************************************************/
  unsigned int DefaultComputeAdapter::computeChrystalImpedances (double       L_m,
								 double       C_m,
								 double       R_m,
								 double       C_p,
								 double*      frequencies,
								 unsigned int n_frequencies,
								 double**     impedances) {
      
    return christelDefaultComputeChrystalImpedances (L_m,
						     C_m,
						     R_m,
						     C_p,
						     frequencies,
						     n_frequencies,
						     impedances) ;
  }
    
  /*************************************************************************
   *  Method: DefaultComputeAdapter::computeCrystalTransmissionLoss
   ************************************************************************/
  unsigned int DefaultComputeAdapter::computeCrystalTransmissionLosses (double**     computed_losses,
									unsigned int n_samples,
									double*      frequencies,
									double       system_impedance,
									double       L_m,
									double       C_m,
									double       R_m,
									double       C_p) {
    double* impedances ;
    unsigned int n_impedances = christelDefaultComputeChrystalImpedances (L_m,
									  C_m,
									  R_m,
									  C_p,
									  frequencies,
									  n_samples,
									  &impedances) ;
    

    unsigned int n_losses = christelDefaultComputeCrystalTransmissionLosses (computed_losses,
									     n_samples,
									     impedances,
									     system_impedance) ;

    free (impedances) ;

    return n_losses ;
  }
  
  /*************************************************************************
   *  Method: DefaultComputeAdapter::filter
   ************************************************************************/
  unsigned int DefaultComputeAdapter::filter (double**     output_samples,
					      double*      num_coefs,
					      unsigned int n_num_coefs,
					      double*      den_coefs,
					      unsigned int n_den_coefs,
					      double*      input_samples,
					      unsigned int n_samples) {
    return christelDefaultComputeFilter (output_samples,
					 num_coefs,
					 n_num_coefs,
					 den_coefs,
					 n_den_coefs,
					 input_samples,
					 n_samples) ;
  }
  
} // namespace Christel::Lib::DefaultComputeAdapter
