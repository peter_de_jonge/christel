/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include "TransmissionLoss.hpp"
#include <math.h>

namespace Christel::Lib::DefaultComputeAdapter {

  /*****************************************************************************
   *  Function: christelDefaultComputeCrystalTransmissionLoss
   ****************************************************************************/
  double christelDefaultComputeCrystalTransmissionLoss (double* impedance,
							double Z_S) {
    double X_X          = impedance[1] ;
    double R_X          = impedance[0] ;
    double numerator_real = Z_S * (Z_S + R_X) ;              // teller in Dutch
    double numerator_imag = - Z_S * X_X ;                    // teller in Dutch
    double denominator = pow((Z_S + R_X),2) + pow (X_X,2) ;  // noemer in Dutch
    return 20 * log10 (sqrt (pow((numerator_real / denominator),2) + pow((numerator_imag / denominator),2))) ;
  }
  
  /*****************************************************************************
   *  Function: christelDefaultComputeCrystalTransmissionLosses
   ****************************************************************************/
  unsigned int christelDefaultComputeCrystalTransmissionLosses (double**     computed_losses,
								unsigned int n_samples,
								double*      impedances,
								double       system_impedance) {
    unsigned int  computed_losses_size = n_samples * sizeof(double) ;
    double*       impedance_iter = impedances ;
    unsigned int  i = 0 ;
      
    *computed_losses = (double*) malloc (computed_losses_size) ;

    for (i = 0; i < n_samples; i++) {
      (*computed_losses)[i] = christelDefaultComputeCrystalTransmissionLoss (impedance_iter, system_impedance) ;
      impedance_iter += 2 ;           
    }
    return n_samples ;
  }
  
  
} // namespace Christel::Lib::DefaultComputeAdapter
