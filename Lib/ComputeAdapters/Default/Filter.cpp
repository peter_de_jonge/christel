/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include "Filter.hpp"

namespace Christel::Lib::DefaultComputeAdapter {
  
  unsigned int christelDefaultComputeFilter (double**     output_samples,
					     double*      num_coefs,
					     unsigned int n_num_coefs,
					     double*      den_coefs,
					     unsigned int n_den_coefs,
					     double*      input_samples,
					     unsigned int n_samples) {
				     
    unsigned Ximax = n_num_coefs - 1 ;
    unsigned Yimax = n_den_coefs - 1 ;
    double Xn[n_num_coefs] ;
    double Yn[n_den_coefs] ;
    unsigned int is = 0 ;
    unsigned int os = 0 ;

    (*output_samples) = (double*) malloc (n_samples * sizeof (double)) ;
    

    for (unsigned int i = 0; i < n_num_coefs; i++) {
      Xn[i] = 0.0D;
    }
    for (unsigned int i = 0; i < n_den_coefs; i++) {
      Yn[i] = 0.0D;
    }

    // Convolute to output
    for (; is < n_samples; is++) {
      double X = 0.0D;
      double Y = 0.0D;

      for (unsigned int i = Ximax; i > 0;) {
	unsigned int i_tmp = i - 1;
	X += num_coefs[i] * Xn[i];
	Xn[i] = Xn[i_tmp];
	i = i_tmp;
      }
      Xn[0] = input_samples[is];
      X += num_coefs[0] * Xn[0];

      for (unsigned int i = Yimax; i > 0;) {
	unsigned int i_tmp = i - 1;
	Y += den_coefs[i] * Yn[i];
	Yn[i] = Yn[i_tmp];
	i = i_tmp;
      }
      Yn[0] = X + den_coefs[0] * Yn[0] + Y;
      (*output_samples)[os] = Yn[0];

      os++;
    }
    return n_samples ;
  }
} // namespace Christel::Lib::DefaultComputeAdapter
