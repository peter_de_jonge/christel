/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCHRISTEL_CRYSTAL_STOCK_HPP
#define LIBCHRISTEL_CRYSTAL_STOCK_HPP

#include <sigc++/sigc++.h>
#include <Crystal.hpp>
#include <Model.hpp>
#include <string>
#include <vector>

namespace Christel {
  namespace Lib {

    class Repository ;
    
    class CrystalStock : public Model {
      friend Repository ;
    public:
      /**
       *  Constructor that sets the name of the crystal stock
       *
       *  @param std::string stock_name
       */
      CrystalStock (std::string   stock_name,
                    double        series_resonance_frequency) ;

      /**
       *  Constructor that sets the name of the crystal stock
       *
       *  @param std::string stock_name
       */
      CrystalStock (unsigned char*         uuid,
                    std::string            stock_name,
                    double                 series_resonance_frequency,
		    std::vector<Crystal*>* crystals) ;
      
      /**
       *  Method that gets the name of the chrystal stock
       *
       *  @return std::string
       */
      std::string getName () ;

      /**
       *  Method that sets the name of the chrystal stock
       *
       *  @param std::string name
       *  @return void
       */
      void setName (std::string name) ;

			
      /**
       *  Get the series_resonance_frequency
       *
       *  @return std::string
       */
      double getSeriesResonanceFrequency () ;
			
      /**
       * Method that gets the list of crystals in the stock
       *
       * @return vector<Crystal>
       */
      //TODO: delete this method and add an iterator to iterate over crystals
      std::vector<Crystal*> getCrystals () ;


      /**
       * Method that gets a crystal by number from the stock
       *
       * @param  unsigned int crystal_number
       * @return Crystal*
       */
      Crystal* getCrystalByNumber (unsigned int crystal_number) ;
				
      /**
       * Method that adds a crystal to the stock
       *
       * @param Crystal* crystal
       */
      void addCrystal (Crystal* crystal) ;

      /**
       * Method that removes a crystal from the stock
       *
       * @param Crystal* crystal
       */
      void removeCrystal (Crystal* crystal) ;

      /**
       * Signal that notifies when a crystal was added to the stock
       *
       * @param Crystal*
       * @return void
       */
      sigc::signal<void, CrystalStock*, Crystal*> crystal_added ;

      /**
       * Get next free crystal number
       *
       * @param bool fill_gap_in_ids
       * @return unsigned int free crystal number
       */
      unsigned int getNextFreeCrystalNumber (bool fill_gap_in_ids) ;
      
    private:
      std::string             _name ;
      double                  _series_resonance_frequency ;
      std::vector<Crystal*>*  _crystals ;
      // TODO: add mutation (add-/remove-/get-Crystal(s)) mutex

    };
  } // namespace Lib
} // namespace Christel

#endif // LIBCHRISTEL_CRYSTAL_STOCK_HPP


