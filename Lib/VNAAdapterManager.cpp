/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <TransmissionLossMeasurementController.hpp>
#include <VNAAdapterManager.hpp>
#include <iostream>
#include <Path.hpp>

namespace Christel::Lib {

  VNAAdapterManager* _vna_adapter_manager = nullptr ;
    
  /*************************************************************************
   *  Method: VNAAdapterManager::getVNAAdapterManager
   ************************************************************************/
  VNAAdapterManager* VNAAdapterManager::getVNAAdapterManager () {
    if (_vna_adapter_manager == nullptr) {
      _vna_adapter_manager = new VNAAdapterManager () ;
    } 
    return _vna_adapter_manager ;
  }

  /***************************************************************************
   *  Constructor: VNAAdapterManager::VNAAdapterManager
   **************************************************************************/
  VNAAdapterManager::VNAAdapterManager () {
    _current_adapter = nullptr ;
    _current_module  = nullptr ;
    std::cout << "VNAModulesPath" << Path::getVNAAdaptersDirectoryPath ().string () << std::endl ;
    _resolveModules (Path::getVNAAdaptersDirectoryPath ()) ;
    if (_modules.size () > 0) { // TEMPORARLY select portable adapter
      setActiveVNAAdapterModule (_modules[0]) ;
    }
  }

  /**************************************************************************
   *  Method:: VNAAdapterManager::resolveModules
   *************************************************************************/
  void VNAAdapterManager::_resolveModules (boost::filesystem::path module_dir_path) {
    std::cout << module_dir_path.string() << std::endl ;
    VNAAdapterModule* module = nullptr ;
    
    if (boost::filesystem::is_directory (module_dir_path)) {
      boost::filesystem::directory_iterator end_iter ;
      
      for (boost::filesystem::directory_iterator dir_itr (module_dir_path);
	   dir_itr != end_iter;
	   ++dir_itr) {
	try {
	  if (boost::filesystem::is_regular_file (dir_itr->status ())) {
	    // TODO: check if module is already loaded
	    std::cout << dir_itr->path ().string() << std::endl ;
	    module = new VNAAdapterModule (dir_itr->path ()) ;
	    module->load () ; // TODO: load only on use
	    _modules.push_back (module) ;
	  }
	}
	catch (const std::exception & ex) {
	  std::cout << dir_itr->path ().filename () << " " << ex.what () << std::endl ;
	}
      }
    }
  }

  /**************************************************************************
   *  Method:: VNAAdapterManager::getVNAAdapterModules
   *************************************************************************/
  std::vector<VNAAdapterModule*>  VNAAdapterManager::getVNAAdapterModules () {
    return _modules ;
  }

  /**************************************************************************
   *  Method:: VNAAdapterManager::setActiveVNAAdapterModule
   *************************************************************************/
  void VNAAdapterManager::setActiveVNAAdapterModule (VNAAdapterModule* vna_adapter_module) {
    TransmissionLossMeasurementController* transmission_loss_measurement_controller = TransmissionLossMeasurementController::getTransmissionLossMeasurementController () ;
    VNAAdapterMeta*                        vna_adapter_meta = nullptr ;
    
    // TODO: make this thread safe/atomic
    if (_current_adapter) {
      if (_current_adapter->inUse ()) {
	return ;
      } else {
	transmission_loss_measurement_controller->setVNAAdapter (nullptr) ;
	active_adapter_changed.emit (nullptr, nullptr) ;
	// TODO: destroy current adapter ;
	_current_adapter = nullptr ;
	_current_module = nullptr ;

      }
    }
    _current_module = vna_adapter_module ;
    vna_adapter_meta = _current_module->getVNAAdapterMeta () ;
    if (vna_adapter_meta != nullptr) {
      _current_adapter = _current_module->createVNAAdapter () ;
      active_adapter_changed.emit (_current_adapter, vna_adapter_meta) ;
      transmission_loss_measurement_controller->setVNAAdapter (_current_adapter) ;
      active_adapter_changed.emit (nullptr, nullptr) ;
    }   
  }

  /**************************************************************************
   *  Method:: VNAAdapterManager::getActiveVNAAdapter
   *************************************************************************/
  void VNAAdapterManager::getActiveVNAAdapter (VNAAdapter*&     vna_adapter,
					       VNAAdapterMeta*& vna_adapter_meta) {
    if (_current_module) {
      vna_adapter = _current_adapter ;
      vna_adapter_meta = vna_adapter_meta = _current_module->getVNAAdapterMeta () ;
    } else {
      vna_adapter = nullptr ;
      vna_adapter_meta = nullptr ;
    }
    
  }

  
} // namespace Christel::Lib
