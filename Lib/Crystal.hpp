/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCHRISTEL_CRYSTAL_HPP
#define LIBCHRISTEL_CRYSTAL_HPP

#include <CrystalImpedanceVsFrequencyComputationResult.hpp>
#include <CrystalMeasurementResult.hpp>
#include <Model.hpp>

namespace Christel {
  namespace Lib {

    class Crystal : public Model {
    public:

      typedef unsigned char ParameterComputationMethod ;
      static const ParameterComputationMethod ParameterComputationMethod3DB = 0 ;
	 

      
      /**
       *  Constructor that sets chrystal' number, the crystals' measurement
       *  result and its' parameters all at one
       *
       *  @param The number in the crystal stock
       *  @param The crystals' measurement result
       *  @param The crystals' computed impedance vs frequency result
       */
      Crystal (unsigned int                                   number,
               CrystalMeasurementResult*                      crystal_measurement_result) ;
                  
      /**
       *  Constructor that sets chrystal' number, the crystals' measurement
       *  result and its' parameters all at one
       *
       *  @param The number that indentifies the crystal
       *  @param The number in the crystal stock
       *  @param The crystals' measurement result
       *  @param The crystals' computed impedance vs frequency result
       *  @param The motional inductance L_m of the chrystal
       *  @param The motional capacitance C_m of the chrystal
       *  @param The motional resistence R_m of the chrystal
       *  @param The parallel (stray) capacitance C_p of the chrystal
       */
      Crystal (unsigned char*            uuid,
               unsigned int              number,
               CrystalMeasurementResult* crystal_measurement_result) ;
      
      /**
       *  Get the chrystals' number computed impedance vs frequency
       *
       *  @return The number of the chrystal
       */
      unsigned int getNumber () ;

      /**
       *  Get the chrystals' measurement result
       *
       *  @return The chrystals' measurement result
       */
      CrystalMeasurementResult* getCrystalMeasurementResult () ;

      /**
       *  Get the chrystals' series resonance frequency
       *
       *  @return The series resonance frequency of the chrystal
       */
      double getFSerRes () ;

      /**
       *  Get the chrystals' parallel resonance frequency
       *
       *  @return The parallel resonance frequency of the chrystal
       */
      double getFParRes () ;
			
      /**
       *  Get the chrystals' parameters all at one
       *
       *  @param The number that indentifies the crystal
       *  @param crystal_measurement_result
       *  @param crystal_impedance_vs_frequency_computation_result
       */
      void getData (unsigned int&                                   number,
                    CrystalMeasurementResult*&                      crystal_measurement_result) ;

      /**
       *  Compute the crystals' parameters
       *
       *  @param The computation method to use
       *  @param The motional inductance L_m of the chrystal
       *  @param The motional capacitance C_m of the chrystal
       *  @param The motional resistence R_m of the chrystal
       *  @param The parallel capacitance C_p of the chrystal
       */
      void computeParameters (ParameterComputationMethod computation_method,
			      double&                    L_m,
			      double&                    C_m,
			      double&                    R_m,
			      double&                    C_p) ;
      
      /**
       *  Compute the crystals' transmissionloss
       *
       *  @param frequencies
       *  @param losses
       */
      unsigned computeTransmissionLoss (ParameterComputationMethod computation_method,
					double*&                   computed_losses,
					double*&                   frequencies) ;

      /**
       *  Compare crystal by their number
       *
       *  @param Crystal *crystal_a
       *  @param Crystal *crystal_b
       *  @return bool number_a < number_b
       */
      static bool CompareByNumberALessThanB (Crystal *crystal_a, Crystal *crystal_b) {
        return crystal_a->getNumber () < crystal_b->getNumber () ;
      }

    private:
      CrystalMeasurementResult*                     _crystal_measurement_result ;
      unsigned int                                  _number ;
    } ;
  }
}
#endif // LIBCHRISTEL_CRYSTAL_HPP
