/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include  <ComputeAdapterModule.hpp>
#include  <iostream>

namespace Christel::Lib {

  /***************************************************************************
   *  Constructor: ComputeAdapterModule::ComputeAdapterModule
   **************************************************************************/
  ComputeAdapterModule::ComputeAdapterModule (boost::filesystem::path file_path) :
    Module (file_path) {
    _adapter = nullptr ;
  }

  /***************************************************************************
   *  Method: ComputeAdapterModule::load
   **************************************************************************/
  bool ComputeAdapterModule::load () {
    return Module::load () ;  
  }


  /***************************************************************************
   *  Method: ComputeAdapterModule::unload
   **************************************************************************/
  bool ComputeAdapterModule::unload () {
    return Module::unload () ;
  }
  /***************************************************************************
   *  Method: ComputeAdapterModule::getComputeAdapterMeta
   **************************************************************************/
  ComputeAdapterMeta* ComputeAdapterModule::getComputeAdapterMeta () {
    GetComputeAdapterMetaFunction* get_adapter_meta_function = getFunction<GetComputeAdapterMetaFunction> ("getComputeAdapterMeta") ;
    std::cout << "get_adapter_meta_function: " << (void *)get_adapter_meta_function  << std::endl ;
    if (get_adapter_meta_function) {
      return get_adapter_meta_function () ;
    } else {
      return nullptr ;
    }
  }
  
  /***************************************************************************
   *  Method: ComputeAdapterModule::createComputeAdapter
   **************************************************************************/
  ComputeAdapter* ComputeAdapterModule::createComputeAdapter () {
    CreateComputeAdapterFunction* create_adapter_function = getFunction<CreateComputeAdapterFunction> ("createComputeAdapter") ;
    std::cout << "create_adapter_function: " << (void *)create_adapter_function  << std::endl ;
    if (create_adapter_function != nullptr) {
      return create_adapter_function () ;
    } else {
      return nullptr ;
    }
  }
  
  
} // namespace Christel::Lib
