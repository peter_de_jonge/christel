/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#include <TransmissionLossMeasurementResult.hpp>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <vector>
#include <assert.h>

namespace Christel {
  namespace Lib {

    /***************************************************************************
     *  Constructor: TransmissionLossMeasurementResult::TransmissionLossMeasurementResult ()
     **************************************************************************/
    TransmissionLossMeasurementResult::TransmissionLossMeasurementResult (unsigned int  f_start,
                                                                          unsigned int  f_stop,
                                                                          unsigned int  f_steps,
                                                                          double*       frequencies,
                                                                          double*       losses,
									  double*       phases,
                                                                          double*       series_resistances,
                                                                          unsigned int  n_samples) :
      Model (),
      _f_start (f_start),
      _f_stop (f_stop),
      _f_steps (f_steps),
      _frequencies (frequencies),
      _losses (losses),
      _phases (phases),
      _series_resistances (series_resistances),
      _n_samples (n_samples) {
    }

    /***************************************************************************
     *  Constructor: TransmissionLossMeasurementResult::TransmissionLossMeasurementResult ()
     **************************************************************************/
    TransmissionLossMeasurementResult::TransmissionLossMeasurementResult (unsigned char* uuid,
                                                                          unsigned int   f_start,
                                                                          unsigned int   f_stop,
                                                                          unsigned int   f_steps,
                                                                          double*        frequencies,
                                                                          double*        losses,
									  double*        phases,
                                                                          double*        series_resistances,
                                                                          unsigned int   n_samples) :
      Model (uuid),
      _f_start (f_start),
      _f_stop (f_stop),
      _f_steps (f_steps),
      _frequencies (frequencies),
      _losses (losses),
      _phases (phases) ,
      _series_resistances (series_resistances),
      _n_samples (n_samples) {  
    }
    
    /***************************************************************************
     *  Method: TransmissionLossMeasurementResult::getData
     **************************************************************************/
    unsigned int TransmissionLossMeasurementResult::getData(double*& frequencies,
                                                            double*& losses,
							    double*& phases,
                                                            double*& series_resistances) {
      frequencies        = _frequencies ;
      losses             = _losses ;
      phases             = _phases ;
      series_resistances = _series_resistances ;
      return _n_samples;
    }

    /***************************************************************************
     *  Method: TransmissionLossMeasurementResult::getFrequencies
     **************************************************************************/
    unsigned int TransmissionLossMeasurementResult::getFrequencies (double*& frequencies) {
      frequencies        = _frequencies ;
      return _n_samples;
    }
    
    /***************************************************************************
     *  Method: TransmissionLossMeasurementResult::getFrequencyRange
     **************************************************************************/
    void TransmissionLossMeasurementResult::getFrequencyRange (unsigned int& f_start,
                                                               unsigned int& f_stop,
                                                               unsigned int& f_steps) {
      f_start = _f_start;
      f_stop  = _f_stop;
      f_steps = _f_steps;
    }
  }
}
