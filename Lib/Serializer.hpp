/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#ifndef LIBCRISTEL_SERIALIZER_HPP
#define LIBCRISTEL_SERIALIZER_HPP

#include <bits/stdc++.h>
#include <string.h>
#include <cstring>
#include <string>
#include <lmdb.h>

namespace Christel {
  namespace Lib {

    class Serializer {
    public:
      Serializer () ;
      
      template <typename type>
      void serialize (type value) ;

      void serialize (char* value) ;
      void serialize (const char* value) ;
     
      template <typename type>
      void serialize (type* value, size_t array_length) ;

      template <typename type>
      void serialize (const type* value, size_t array_length) ;
      
      MDB_val mdbValue () ;

      void formatData () ;
      
    private:
      MDB_val      _mdb_val ;
      
      template <typename type>
      void _serializeArray (const type* value, size_t array_length) ;
      void _hexFormat (unsigned int num1) ;
    } ; 


    /*************************************************************************
     *  Constructor: Serializer::Serializer
     ************************************************************************/
    Serializer::Serializer () {
      _mdb_val.mv_data = NULL ;
      _mdb_val.mv_size = 0 ;
    } ;
    
    /*************************************************************************
     *  Method: Serializer::serialize
     ************************************************************************/
    template <typename type>
    void Serializer::serialize (type value) {
      void*  pointer ;
      size_t new_size = _mdb_val.mv_size + sizeof (type) ;
 
      if (_mdb_val.mv_data == NULL) {
        _mdb_val.mv_data = malloc (new_size) ;
        pointer = _mdb_val.mv_data ;
      } else {
        _mdb_val.mv_data = realloc(_mdb_val.mv_data, new_size)  ;
        pointer = (void*) ((size_t) _mdb_val.mv_data + _mdb_val.mv_size) ;
      }
      *((type*) pointer) = value ;

      // std::cout << "Hex: " ;      
      // for (unsigned int i = 0; i < sizeof(type); i++) {
      // 	_hexFormat ((unsigned int)((unsigned char*)pointer)[i]) ;
      // 	//	std::cout << " " ;
      // }
      // std::cout << std::endl ;
      
      // std::cout << "Dec: " ;
      // for (unsigned int i = 0; i < sizeof(type); i++) {
      // 	std::cout << (unsigned int) (((unsigned char*)pointer)[i]) << " " ;
      // }
      // std::cout << std::endl ;
      // std::cout << "Size: " ;   
      // std::cout << sizeof (type) ;
      // std::cout << std::endl;
      // std::cout << std::endl;
      // std::cout << std::endl;
      
      
      _mdb_val.mv_size = new_size ;  
    }

    /*************************************************************************
     *  Method: Serializer::serialize
     ************************************************************************/
    template <typename type>
    void Serializer::serialize (type* value, size_t array_length) {
      _serializeArray (value, array_length) ;
    }
    
   /*************************************************************************
     *  Method: Serializer::serialize
     ************************************************************************/
    template <>
    void Serializer::serialize <std::string> (std::string value) {
      char*  cstr = new char [value.length()+1] ;
      std::strcpy (cstr, value.c_str()) ;
      serialize (cstr) ;
    }
    
    /*************************************************************************
     *  Method: Serializer::serialize
     ************************************************************************/
    template <typename type>
    void Serializer::serialize (const type* value, size_t array_length) {
      _serializeArray (value, array_length) ;
    }
    
    /*************************************************************************
     *  Method: Serializer::serialize
     ************************************************************************/
    void Serializer::serialize (char* value) {
      size_t array_length = strlen (value) + 1 ; // include c string delimiter
      serialize (value, array_length) ;
    }

    /*************************************************************************
     *  Method: Serializer::serialize
     ************************************************************************/
     void Serializer::serialize (const char* value) {
       size_t array_length = strlen (value) + 1 ; // include c string delimiter
       serialize (value, array_length) ;
    }

    /*************************************************************************
     *  Method: Serializer::mdbValue
     ************************************************************************/
    MDB_val Serializer::mdbValue () {
      return _mdb_val ;
    }

    /*************************************************************************
     *  Method: Serializer::_serializeArray
     ************************************************************************/
    template <typename type>
    void Serializer::_serializeArray (const type* value, size_t array_length) {
      void*  pointer ;
      size_t array_size = array_length * sizeof (type) ;
      size_t new_size = _mdb_val.mv_size + array_size ;
      
      if (_mdb_val.mv_data == NULL) {
        _mdb_val.mv_data = malloc (new_size) ;
        pointer = _mdb_val.mv_data ;
      } else {
        _mdb_val.mv_data = realloc(_mdb_val.mv_data, new_size)  ;
        pointer = (void*) ((size_t) _mdb_val.mv_data + _mdb_val.mv_size) ;
      }
      memcpy (pointer, value, array_size) ;
      _mdb_val.mv_size = new_size ;
      
    }
    
    /*************************************************************************
     *  Method: Serializer::formatData
     ************************************************************************/
    void Serializer::formatData () {
      std::cout << "Size: " << _mdb_val.mv_size << std::endl ;
      std::cout << "Data: " ;
      for (unsigned int i = 0; i < _mdb_val.mv_size; i++) {
	_hexFormat (((unsigned char*)_mdb_val.mv_data)[i]) ;
      }
      std::cout << std::endl ;
    }


    /*************************************************************************
     *  Method: Serializer::_hexFormat
     ************************************************************************/
    void Serializer::_hexFormat (unsigned int num1) {
      if (num1 == 0)
	std::cout << "0" ;
      u_int num = num1 ;
      std::string s = "" ;
      while (num) {
         int temp = num % 16 ;
         if (temp <= 9)
            s += (48 + temp) ;
         else
            s += (87 + temp) ;
         num = num / 16 ;
      }
      reverse (s.begin (), s.end ()) ;
      std::cout << s ;
   }

  } //namespace Lib
} // namespace Christel

#endif // LIBCRISTEL_SERIALIZER_HPP
