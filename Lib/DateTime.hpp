/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_DATE_TIME_HPP
#define LIBCRISTEL_DATE_TIME_HPP

#include <time.h>
#include <string>
#include <iostream>

namespace Christel {
  namespace Lib {

    class DateTime {
      friend std::ostream &operator<<(std::ostream &, const DateTime &);

    public:
      DateTime();

    private:
      time_t _time;
      std::string _format;
      std::string _formatted_date_time;

      std::string _formatDateTime();
    };
  }
}

#endif // LIBCRISTEL_DATE_TIME_HPP


