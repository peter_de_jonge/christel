/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCHRISTEL_CRYSTAL_STOCK_MANAGER_HPP
#define LIBCHRISTEL_CRYSTAL_STOCK_MANAGER_HPP

#include <glibmm/ustring.h>
#include <sigc++/sigc++.h>
#include <Repository.hpp>
#include <Path.hpp>
#include <string>
#include <vector>

#include <iostream>

namespace Christel {
  namespace Lib {

    class CrystalStockManager {

    public:
      /**
       *  Constructor that sets the name of the crystal stock
       *
       *  @param std::string stock_name
       */
      CrystalStockManager () ;

      /**
       *  Method that gets a vector with all the crystal stocks
       *
       *  @return std::string
       */
      std::vector<CrystalStock *> getStocks () ;

      /**
       *  Method that adds a crystal stock to the list of chrystal stocks
       *
       *  @param std::string name
       *  @return void
       */
      void addStock (CrystalStock *stock) ;

      /**
       * Method that returns the number of stocks
       *
       *  @return unsigned int
       */
      unsigned int nStocks () ;

      /**
       * Method that returns true if there are stocks and false otherwise
       *
       *  @return bool
       */
      bool haveStocks () ;

      /**
       *  Method that gets a crystal stock by name
       *
       *  @param std::string name
       *  @return void
       */
      CrystalStock *getStockByName (Glib::ustring stock_name) ;

      /**
       * Method that removes a crystal from list of chrystal stocks
       *
       * @param Crystal* crystal
       */
      void removeStock (CrystalStock *stock) ;

      /**
       * Method that reads chrystal stocks from users' data directory
       *
       * @return void
       */
      void readStocks () ;

      /**
       * Signal that notifies when a crystal was added to the stock
       *
       * @param Crystal*
       * @return void
       */
      sigc::signal<void, CrystalStock*> stock_added ;

    private:
      std::vector<CrystalStock*> _crystal_stocks ;
      Repository                 _repository ;
     
      /**
       * Method that writess a chrystal stock to the database environment
       *
       * @return void
       */
      void _writeNewStock (CrystalStock* stock) ;
      void _connectStockHandlers () ;
      void _onCrystalAdded (CrystalStock* crystal_stock, Crystal* crystal) ;
     };

  } // namespace LibChristel
}

#endif // LIBCHRISTEL_CRYSTAL_STOCK_MANAGER_HPP


