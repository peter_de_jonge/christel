/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef CHRISTEL_LIB_COMPUTE_ADAPTER_MANAGER_HPP
#define CHRISTEL_LIB_COMPUTE_ADAPTER_MANAGER_HPP

#include <ComputeAdapterModule.hpp>
#include <ComputeAdapter.hpp>
#include <sigc++/sigc++.h>

namespace Christel::Lib {

  class ComputeAdapterManager {
  public:
    void                               setActiveComputeAdapterModule (ComputeAdapterModule* compute_adapter_module) ;
    static ComputeAdapterManager*      getComputeAdapterManager () ;
    std::vector<ComputeAdapterModule*> getComputeAdapterModules () ;
    void                               getActiveComputeAdapter (ComputeAdapter*&     compute_adapter,
								ComputeAdapterMeta*& compute_adapter_meta) ;

    /******************************************************************************
     * Signal: active_adapter_changed
     *****************************************************************************/
    sigc::signal<void, ComputeAdapter*, ComputeAdapterMeta*> active_adapter_changed ;
    
  private:
    ComputeAdapter*                    _current_adapter ; 
    ComputeAdapterModule*              _current_module ;
    std::vector<ComputeAdapterModule*> _modules ;

    ComputeAdapterManager () ;
    void                    _resolveModules (boost::filesystem::path module_dir_path) ;
    boost::filesystem::path _getComputeAdaptersDirectoryPath () ;
    bool                    _isDynamicLoadLibrary (boost::filesystem::path module_dir_path) ;
  } ;
  
} // namespace Christel::Lib

#endif // CHRISTEL_LIB_COMPUTE_ADAPTER_MANAGER_HPP
