/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_MODULE_HPP
#define LIBCRISTEL_MODULE_HPP

#include <boost/filesystem.hpp>
#include <iostream>
#include <dlfcn.h>

namespace Christel::Lib {
 
  class Module {

  public:

    bool         isLoaded () ;
    
  protected:
    Module (boost::filesystem::path file_path) ;
    template <typename FunctionType>
    FunctionType* getFunction (const std::string function_name) ;
    virtual bool load () ;
    virtual bool unload () ;
    
  private:
    boost::filesystem::path _file_path ;
    void*                   _handle ;
    
   } ;


  /***************************************************************************
   *  Method: Module::getFunction
   **************************************************************************/  
  template <typename FunctionType>
  FunctionType* Module::getFunction (const std::string function_name) {
    const char* path = _file_path.string ().c_str () ;
    if (!_handle) {
      std::cerr << "Failed to get function " << function_name.c_str() << ", library " << path << "not loaded" << std::endl;
      return nullptr;
    }
    
    const auto sym = dlsym (_handle, function_name.c_str()) ;
    if (!sym) {
      std::cerr << "Failed te get function: " << function_name.c_str() << " from library" << path << std::endl ;
      std::cerr << dlerror () << std::endl ;
      return nullptr ;
    }
    return (FunctionType*) sym ;
  }
    
} // namespace Christel::Lib

#endif // LIBCRISTEL__HPP
