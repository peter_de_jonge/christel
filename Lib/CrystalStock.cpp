/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/
 
#include <CrystalStock.hpp>
#include <algorithm>

namespace Christel {
  namespace Lib {

    /*****************************************************************************
     * Method: CrystalStock::CrystalStock
     *****************************************************************************/
    CrystalStock::CrystalStock (std::string stock_name,
                                double 	    series_resonance_frequency) :
      Model (),
      _name (stock_name),
      _series_resonance_frequency (series_resonance_frequency) {
      _crystals = new std::vector<Crystal*> () ;
    }

    /*****************************************************************************
     * Method: CrystalStock::CrystalStock
     *****************************************************************************/
    CrystalStock::CrystalStock (unsigned char* uuid,
                                std::string    stock_name,
                                double 	       series_resonance_frequency, 
				std::vector<Crystal*>* crystals) :
      Model (uuid),
      _name (stock_name),
      _series_resonance_frequency (series_resonance_frequency) {
      _crystals = crystals ;
    }

    
    /*****************************************************************************
     * Method: CrystalStock::getName
     *****************************************************************************/
    std::string CrystalStock::getName () {
      return _name ;
    }

    /*****************************************************************************
     * Method: CrystalStock::setName
     *****************************************************************************/
    void CrystalStock::setName (std::string name) {
      _name = name ;
    }

    /*****************************************************************************
     * Method: CrystalStock::getCrystals
     *****************************************************************************/
    std::vector<Crystal *> CrystalStock::getCrystals () {
      return *_crystals ;
    }

    /*****************************************************************************
     * Method: CrystalStock::getCrystalByNumber
     *****************************************************************************/
    Crystal* CrystalStock::getCrystalByNumber (unsigned int crystal_number) {
      std::vector<Crystal*>::iterator _crystal_iterator ;
      for (_crystal_iterator = _crystals->begin (); _crystal_iterator != _crystals->end () ; _crystal_iterator++) {
        if ((*_crystal_iterator)->getNumber () == crystal_number) {
          return (*_crystal_iterator) ;
        }
      }
      return NULL ;			
    }

    /*****************************************************************************
     * Method: CrystalStock::getSeriesResonanceFrequency
     *****************************************************************************/
    double CrystalStock::getSeriesResonanceFrequency () {
      return _series_resonance_frequency ;
    }
				
    /*****************************************************************************
     * Method: CrystalStock::addCrystal
     *****************************************************************************/
    void CrystalStock::addCrystal (Crystal* crystal) {
      _crystals->push_back (crystal) ;
      crystal_added.emit (this, crystal) ;
    }

    /*****************************************************************************
     * Method: CrystalStock::removeCrystal
     *****************************************************************************/
    void CrystalStock::removeCrystal (Crystal* crystal) {

    }

    /*****************************************************************************
     * Method: CrystalStock::getNextFreeCrystalNumber
     *****************************************************************************/
    unsigned int CrystalStock::getNextFreeCrystalNumber (bool fill_gap_in_ids) {
      std::vector<Crystal*>::iterator _crystal_iterator ;
      unsigned int                    _crystal_number ;
      Crystal*                        _crystal ;
      std::sort (_crystals->begin (), _crystals->end (), Crystal::CompareByNumberALessThanB) ;
      if (_crystals->size () == 0) {
        return 1 ;
      } else {
        if (fill_gap_in_ids) {
          _crystal_iterator = _crystals->begin () ;
          _crystal          = *_crystal_iterator ;
          _crystal_number   = _crystal->getNumber () + 1 ;
					
          for (; _crystal_iterator != _crystals->end () ; _crystal_iterator++) {
            _crystal = *_crystal_iterator ;
            if  ((_crystal->getNumber () - 1) > _crystal_number) {
              return _crystal_number  ;
            } else {
              _crystal_number   = _crystal->getNumber () + 1 ;
            }
          }
          return _crystal_number  ;
        } else {
          return (*(_crystals->end ()))->getNumber () + 1 ;
        }
      }
    }
  }
}


