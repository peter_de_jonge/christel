/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <Common.hpp>
#include <Model.hpp>
#include <iostream>
#include <string.h>
#include <stdio.h>


namespace Christel {
  namespace Lib {

    /*************************************************************************
     *  Constructor: Model::Model
     ************************************************************************/
    Model::Model () {
      _uuid = (unsigned char*) malloc (sizeof (unsigned char)) ;
      uuid_generate_time_safe (_uuid) ;
    }

    /*************************************************************************
     *  Constructor: Model::Model
     ************************************************************************/
    Model::Model (unsigned char* uuid){
      _uuid = uuid ;
    }

    /*************************************************************************
     *  Constructor: Model::~Model
     ************************************************************************/
    Model::~Model (){
      free (_uuid) ;
    }  
    
    /*************************************************************************
     *  Constructor: Model::getUUID
     ************************************************************************/
    unsigned char* Model::getUUID () {
      std::cout << "Model::getUUID: " ;
      printUUID () ;
      return _uuid ;
    }

    /*************************************************************************
     *  Constructor: Model::printUUID
     ************************************************************************/
    void Model::printUUID () {
      printf("%p\n", (void*) _uuid) ;
      for (int i = 0; i < 16; i++) {
	std::cout << (unsigned int) _uuid[i] << " " ;
      }
      std::cout << std::endl ;
    }
    
  }
} // namespace LibChristel
