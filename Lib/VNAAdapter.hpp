/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_VNA_ADAPTER_HPP
#define LIBCRISTEL_VNA_ADAPTER_HPP

#include <string>

namespace Christel::Lib {

  
  /**************************************************************************
   * Class: VNAAdapter
   *************************************************************************/
  class VNAAdapter {
  public:
    virtual size_t doTransmissionLossMeasurement (unsigned int f_start,
						  unsigned int f_steps,
						  unsigned int f_stop,
						  double*&     frequencies,
						  double*&     losses,
						  double*&     phases,
						  double*&     series_resistances) = 0 ;
    void         setNAveragingCycles (unsigned int n_averaging_cycles) ;
    unsigned int getNAveragingCycles () ;
    void         setCalibrationFile (std::string calibration_filename) ;
    std::string  getCalibrationFile () ;
    void         setUSBPortPath (std::string vna_usb_port_path) ;
    std::string  getUSBPortPath () ;
    void         incUses () ;
    void         decUses () ;
    unsigned int getUses () ;
    bool         inUse () ;

  private:
    std::string  _calibration_filename ;
    unsigned int _n_averaging_cycles ;
    std::string  _vna_usb_port_path ;
    unsigned int _uses ;
  } ;

  /**************************************************************************
   * Structure: VnaAdapterMeta
   *************************************************************************/
  struct VNAAdapterMeta {
    std::string name ;
    std::string version ;
  } ;
}
  
#define DEFINE_CHRISTAL_VNA_ADAPTER_ENTRY_FUNCTIONS(ModuleType,VNAAdapterMetaVar) \
									\
  extern "C" const VNAAdapterMeta* getVNAAdapterMeta () {		\
    return &VNAAdapterMetaVar ;						\
  }									\
    									\
  extern "C" Christel::Lib::VNAAdapter* createVNAAdapter () {		\
    return new ModuleType () ;						\
  }									\
  									\
  extern "C" void destroyVNAAdapter (Christel::Lib::VNAAdapter* vna_adapter) { \
    delete vna_adapter ;						\
  }									\
									\



#endif // LIBCRISTEL_VNA_ADAPTER_HPP
