/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <TransmissionLossMeasurementResult.hpp>
#include <CrystalMeasurementController.hpp>
#include <CrystalStock.hpp>
#include <Path.hpp>
#include <cmath>

namespace Christel {
  namespace Lib {
		
    /************************************************************************
     * Contructor: CrystalMeasurementController::CrystalMeasurementController
     ************************************************************************/
    CrystalMeasurementController::CrystalMeasurementController (CrystalStock* crystal_stock,
								double        fixture_system_impedance) {
      _crystal_stock = crystal_stock ;
      _fixture_system_impedance = fixture_system_impedance ;
      _transmission_loss_measurement_controller = TransmissionLossMeasurementController::getTransmissionLossMeasurementController () ;
    }

    /*************************************************************************
     * Method: CrystalMeasurementController::doMeasure
     ************************************************************************/
    Crystal* CrystalMeasurementController::doMeasure (unsigned int crystal_number) {
      CrystalImpedanceVsFrequencyComputationResult* crystal_impedance_vs_frequency_computation_result ;
      double*                                       resonance_series_resistances ;
      Christel::Lib::CrystalMeasurementResult*      crystal_measurement_result ;
      double*                                       resonance_frequencies ;
      unsigned int                                  n_resonance_points ;
      unsigned int*                                 resonance_indexes ;
      double*                                       resonance_losses ;
      unsigned int                                  n_frequencies ;
      double*                                       frequencies ;
      Crystal*                                      crystal ;
      unsigned int                                  f_start ;
      unsigned int                                  f_steps ;
      unsigned int                                  f_stop ;
			
      _resolveFrequencyRange (_crystal_stock->getSeriesResonanceFrequency () * 1000000.0D, f_start, f_stop, f_steps) ;
      Christel::Lib::TransmissionLossMeasurementResult* _transmission_loss_measurement_result = _transmission_loss_measurement_controller->doMeasure (f_start,
																		      f_steps,
																		      f_stop) ;
      n_frequencies = _transmission_loss_measurement_result->getFrequencies (frequencies) ;
      n_resonance_points =_resolveResonancePoints (_transmission_loss_measurement_result,
						   resonance_indexes,
						   resonance_frequencies,
						   resonance_losses,
						   resonance_series_resistances) ;
      crystal_measurement_result = new CrystalMeasurementResult (_transmission_loss_measurement_result,
								 resonance_indexes,
								 resonance_frequencies,
								 resonance_losses,
								 resonance_series_resistances,
								 _fixture_system_impedance,
								 n_resonance_points) ;
      crystal = new Crystal (crystal_number, crystal_measurement_result) ;
      _crystal_stock->addCrystal (crystal) ;
      return crystal ;
    }



    
    /*************************************************************************
     * Method: CrystalMeasurementController::doMeasure
     ************************************************************************/
    Crystal* CrystalMeasurementController::doMeasure (bool fill_gap_in_ids) {
      unsigned int crystal_number = _crystal_stock->getNextFreeCrystalNumber (fill_gap_in_ids) ;
      
      return doMeasure (crystal_number) ;
    }
			
    /*************************************************************************
     * Method: CrystalMeasurementController::_resolveFrequencyRange
     ************************************************************************/
    void CrystalMeasurementController::_resolveFrequencyRange (unsigned int  approximate_series_resonance_frequency,
                                                               unsigned int& f_start,
                                                               unsigned int& f_stop,
                                                               unsigned int& f_steps) {
      unsigned int _df = (unsigned int) round (approximate_series_resonance_frequency / 100) ;
      f_start = approximate_series_resonance_frequency - _df ;
      f_stop  = approximate_series_resonance_frequency + _df ;
      f_steps = 10000 ; // TODO: Don't forget to set this back to 10000
    }
			
    /*************************************************************************
     * Method: CrystalMeasurementController::_resolveResonancePoints
     ************************************************************************/
    unsigned int CrystalMeasurementController::_resolveResonancePoints (TransmissionLossMeasurementResult* transmission_loss_measurement_result,
                                                                        unsigned int*& resonance_indexes,
                                                                        double*&       resonance_frequencies,
                                                                        double*&       resonance_losses,
                                                                        double*&       resonance_series_resistances) {

      double*       _frequencies ;
      double*       _losses ;
      double*       _phases ;
      double*       _series_resistances ;
      unsigned int* _peak_indices ;
      unsigned int  _n_peaks ;
      unsigned int  _n_samples = transmission_loss_measurement_result->getData (_frequencies,
                                                                                _losses,
										_phases,
                                                                                _series_resistances) ;
      unsigned int _i ;
      unsigned int _i_min  =  0 ;
      double       _f_min  = _frequencies[0] ;
      double       _l_min  = _losses[0] ;
      double       _sr_min = _series_resistances[0] ;
      unsigned int _i_max  =  0 ;
      double       _f_max  = _frequencies[0] ;
      double       _l_max  = _losses[0] ;
      double       _sr_max = _series_resistances[0] ;
			
      for (_i = 1; _i < _n_samples; _i++) {
        if (_losses[_i] < _l_max) {
          _i_max  = _i ;
          _l_max  = _losses[_i] ;
          _sr_max = _series_resistances[_i] ;
          _f_max  = _frequencies[_i] ;
        }
        if (_losses[_i] > _l_min) {
          _i_min  = _i ;
          _l_min  = _losses[_i] ;
          _sr_min = _series_resistances[_i] ;
          _f_min  = _frequencies[_i] ;
        }
      }
      resonance_indexes            = (unsigned int *)  malloc (2 * sizeof (unsigned int)) ;
      resonance_frequencies        = (double *) malloc (2 * sizeof (double)) ;
      resonance_losses             = (double *) malloc (2 * sizeof (double)) ;
      resonance_series_resistances = (double *) malloc (2 * sizeof (double)) ;

      resonance_indexes[0]            = _i_min ;
      resonance_frequencies[0]        = _f_min ;
      resonance_losses[0]             = _l_min ;
      resonance_series_resistances[0] = _sr_min ;
			
      resonance_indexes[1]            = _i_max ;
      resonance_frequencies[1]        = _f_max ;
      resonance_losses[1]             = _l_max ;
      resonance_series_resistances[1] = _sr_max ;
			
      return 2 ;
    }
   
  } //namepace Lib
} // namespace Christel

