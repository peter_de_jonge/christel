/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include<CrystalImpedanceVsFrequencyComputationResult.hpp>
#include <stdlib.h>

namespace Christel {
  namespace Lib {
    
    /*************************************************************************
     *  Contructor:   CrystalImpedanceVsFrequencyComputationResult::CrystalImpedanceVsFrequencyComputationResult
     ************************************************************************/
    CrystalImpedanceVsFrequencyComputationResult::CrystalImpedanceVsFrequencyComputationResult (double*       impedances,
												double*       frequencies,
												unsigned int  n_samples) :
      Model (),
      _impedances (impedances),
      _frequencies (frequencies),
      _n_samples (n_samples) {
      _resolveImpedanceParts () ;
    }

    /*************************************************************************
     *  Contructor:   CrystalImpedanceVsFrequencyComputationResult::CrystalImpedanceVsFrequencyComputationResult
     ************************************************************************/
    CrystalImpedanceVsFrequencyComputationResult::CrystalImpedanceVsFrequencyComputationResult (unsigned char*  uuid,
												double*         impedances,
												double*         frequencies,
												unsigned int    n_samples) :
      Model (uuid),
      _impedances (impedances),
      _frequencies (frequencies),
      _n_samples (n_samples) {
      _resolveImpedanceParts () ;
    }
    
    /*************************************************************************
     *  Method: CrystalImpedanceVsFrequencyComputationResult::getData
     ************************************************************************/
    unsigned int CrystalImpedanceVsFrequencyComputationResult::getData (double*& impedances,
									double*& frequencies) {
      impedances = _impedances ;
      frequencies = _frequencies ;
      return _n_samples ;
    }


    /*************************************************************************
     *  Method: CrystalImpedanceVsFrequencyComputationResult::getRealParts
     ************************************************************************/
    unsigned int CrystalImpedanceVsFrequencyComputationResult::getRealParts (double*& impedance_real_parts,
									     double*& frequencies) {
      impedance_real_parts = _impedance_real_parts ;
      frequencies = _frequencies ;
      return _n_samples ;
    }

    /*************************************************************************
     *  Method: CrystalImpedanceVsFrequencyComputationResult::getImaginaryParts
     ************************************************************************/
    unsigned int CrystalImpedanceVsFrequencyComputationResult::getImaginaryParts (double*& impedance_imaginary_parts,
										  double*& frequencies) {
      impedance_imaginary_parts = _impedance_imaginary_parts ;
      frequencies = _frequencies ;
      return _n_samples ;
    }

     /*************************************************************************
     *  Method: CrystalImpedanceVsFrequencyComputationResult::_resolveImpedanceParts ()
     ************************************************************************/
    void CrystalImpedanceVsFrequencyComputationResult::_resolveImpedanceParts () {
      _impedance_real_parts = (double*) malloc (_n_samples * sizeof (double)) ;
      _impedance_imaginary_parts = (double*) malloc (_n_samples * sizeof (double)) ;
      
      int n = 0 ;
      
      for (int i = 0; i < _n_samples * 2 ;) {
	_impedance_real_parts[n] = _impedances[i] ;
	i++ ;
	_impedance_imaginary_parts[n] = _impedances[i] ;
	i++ ;
	n++ ;
      }
    }
    
  } // namespace Lib 
} // namespace Christel
