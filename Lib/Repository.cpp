/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#include <Demarshall.hpp>
#include <Repository.hpp>
#include <Marshall.hpp>
#include <functional>
#include <Path.hpp>
#include <string.h>
#include <iostream>

#define CRYSTAL_IMPEDANCE_VS_FREQUENCY_COMPUTATION_RESULT_DATABASE_NAME  "CrystalImpedanceVsFrequencyComputationResults"
#define TRANSMISSION_LOSS_MEASUREMENT_RESULT_DATABASE_NAME               "TransmissionLossMeasurementResults"
#define CRYSTAL_MEASUREMENT_RESULT_DATABASE_NAME                         "CrystalMeasurementResults"
#define CRYSTAL_DATABASE_NAME                                            "Crystals"
#define STOCK_DATABASE_NAME                                              "Stocks"

namespace Christel {
  namespace Lib {

    /*************************************************************************
     *  Constructor: Repository::Repository
     ************************************************************************/
    Repository::Repository () {
      boost::filesystem::path        _stock_path             = Path::getDataDirectoryPath () ;
      boost::filesystem::file_status _stock_directory_status = status(_stock_path);

     
      if (!is_directory (_stock_directory_status)) {
        create_directory(_stock_path) ;
      }
      /* Create and open the LMDB environment: */
      mdb_env_create (&_env_handle) ;
      mdb_env_set_maxdbs (_env_handle, 32) ;
      mdb_env_set_mapsize (_env_handle, 1000000000) ;
      mdb_env_open (_env_handle, _stock_path.string ().c_str (), 0, 0664) ;
    }

    /*************************************************************************
     *  Constructor: Repository::~Repository
     ************************************************************************/
    Repository::~Repository () {
      mdb_env_close (_env_handle) ;
    }

    /*************************************************************************
     *  Method: Repository::getStocks
     ************************************************************************/
    void Repository::getStocks (std::vector<CrystalStock*>& stocks) {
      /* Fetch key/value pairs in a read-only transaction: */
    
      MDB_dbi     dbi ;
      MDB_cursor* cursor ;
      MDB_val     key ;
      MDB_val     value ;

      mdb_txn_begin (_env_handle, NULL, 0, &_txn) ;
      mdb_dbi_open (_txn, STOCK_DATABASE_NAME, MDB_CREATE, &dbi) ;
      mdb_cursor_open (_txn, dbi, &cursor) ;

      while (!mdb_cursor_get (cursor, &key, &value, MDB_NEXT)) {
        stocks.push_back (_resolveStock (key, value)) ;
      }

      mdb_cursor_close (cursor) ;
      mdb_txn_abort (_txn) ;
    }
       
    /*************************************************************************
     *  Method: Repository::writeCrystalStockRecord
     ************************************************************************/
    void Repository::writeCrystalStockRecord (CrystalStock* stock) {
      MDB_dbi  dbi ;
      MDB_val  key   = _resolveKey (stock->getUUID ()) ;
      MDB_val  value = marshall (stock) ;
      
      mdb_txn_begin (_env_handle, NULL, 0, &_txn) ;
      mdb_dbi_open (_txn, STOCK_DATABASE_NAME, MDB_CREATE, &dbi) ;
      mdb_put (_txn, dbi, &key, &value, 0) ;
      mdb_txn_commit (_txn) ;
    }
    
    /*************************************************************************
     *  Method: Repository::writeCrystalRecord
     ************************************************************************/
    void Repository::writeCrystalRecord (Crystal* crystal) {
  
      CrystalMeasurementResult*           crystal_measurement_result           = crystal->getCrystalMeasurementResult () ;
      TransmissionLossMeasurementResult*  transmission_loss_measurement_result = crystal_measurement_result->getTransmissionLossMeasurementResult () ;
      
      MDB_dbi transmission_loss_measurement_result_dbi ;
      MDB_val transmission_loss_measurement_result_key                = _resolveKey (transmission_loss_measurement_result->getUUID ()) ;
      MDB_val transmission_loss_measurement_result_value              = marshall (transmission_loss_measurement_result) ;
      
      MDB_dbi crystal_measurement_result_dbi ;
      MDB_val crystal_measurement_result_key                          = _resolveKey (crystal_measurement_result->getUUID ()) ;
      MDB_val crystal_measurement_result_value                        = marshall (crystal_measurement_result) ;
      
      MDB_dbi crystal_dbi ;
      MDB_val crystal_key                                             = _resolveKey (crystal->getUUID ()) ;
      MDB_val crystal_value                                           = marshall (crystal) ;


      // Start transaction
      mdb_txn_begin (_env_handle, NULL, 0, &_txn) ;
      
      // Save transmissionlossmeasurementresult
      mdb_dbi_open (_txn, TRANSMISSION_LOSS_MEASUREMENT_RESULT_DATABASE_NAME, MDB_CREATE, &transmission_loss_measurement_result_dbi) ;
      mdb_put (_txn, transmission_loss_measurement_result_dbi, &transmission_loss_measurement_result_key, &transmission_loss_measurement_result_value, 0) ;

      // Save crystalmeasurementresult
      mdb_dbi_open (_txn, CRYSTAL_MEASUREMENT_RESULT_DATABASE_NAME, MDB_CREATE, &crystal_measurement_result_dbi) ;
      mdb_put (_txn, crystal_measurement_result_dbi, &crystal_measurement_result_key, &crystal_measurement_result_value, 0) ;

      // Save crystal
      mdb_dbi_open (_txn, CRYSTAL_DATABASE_NAME, MDB_CREATE, &crystal_dbi) ;
      mdb_put (_txn, crystal_dbi, &crystal_key, &crystal_value, 0) ;

      // Commit
      mdb_txn_commit (_txn) ;    
    }
   
    /*************************************************************************
     *  Method: Repository::_resolveKey
     ************************************************************************/
    MDB_val Repository::_resolveKey (unsigned char* uuid) {
      MDB_val key ;
      key.mv_size = CHRISTEL_LIB_UUID_SIZE ;
      key.mv_data = malloc (CHRISTEL_LIB_UUID_SIZE) ;
      memcpy (key.mv_data, uuid, CHRISTEL_LIB_UUID_SIZE) ;
      return key ;
    }


    /*************************************************************************
     *  Method: Repository::_resolveStock
     ************************************************************************/
    CrystalStock* Repository::_resolveStock (MDB_val key, MDB_val value) {
      std::function<std::vector<Crystal*>*(char unsigned* uuids, size_t n_uuids)> getCrystals = [this] (char unsigned* uuids, size_t n_uuids) {
												  return _resolveCrystals (uuids, n_uuids) ;
												} ;
      return demarshall (key, value, getCrystals) ;
    }
    
    /*************************************************************************
     *  Method: Repository::_resolveCrystals
     ************************************************************************/
    std::vector<Crystal*>* Repository::_resolveCrystals (unsigned char* uuids, size_t n_uuids) {
      std::vector<Crystal*>* crystals     = new std::vector<Crystal*> () ;
      char unsigned*         uuid_pointer = uuids ;
      Crystal*               crystal ;
      
      for (size_t i = 0; i < n_uuids; i++) {
	crystal = _resolveCrystal (uuid_pointer) ;
	if (crystal) {
	  crystals->push_back (crystal) ;
	}
	uuid_pointer += CHRISTEL_LIB_UUID_SIZE ;
      }
      return crystals ;
    }

    /*************************************************************************
     *  Method: Repository::_resolveCrystal
     ************************************************************************/
    Crystal* Repository::_resolveCrystal (unsigned char* uuid) {
      std::function<CrystalMeasurementResult*(char unsigned* uuid)> getCrystalMeasurementResult = [this] (char unsigned* uuid) {
												    return _resolveCrystalMeasurementResult (uuid) ;
												  } ;
      MDB_val                            crystal_key = _resolveKey (uuid) ;
      MDB_val                            crystal_value ;
      MDB_dbi                            crystal_dbi ;
      int                                res ;
      
      // Load crystal data
      mdb_dbi_open (_txn, CRYSTAL_DATABASE_NAME, MDB_CREATE, &crystal_dbi) ;
      res = mdb_get (_txn, crystal_dbi , &crystal_key, &crystal_value) ;
      
      if (res == 0) {
	return demarshall (crystal_key, crystal_value, getCrystalMeasurementResult) ;
      } else {
	return NULL ;
      }
    }
    
    /*************************************************************************
     *  Method: Repository::_resolveCrystalMeasurementResult
     ************************************************************************/
    CrystalMeasurementResult* Repository::_resolveCrystalMeasurementResult (char unsigned* uuid) {
      std::function<TransmissionLossMeasurementResult*(char unsigned* uuid)> getTransmissionLossMeasurementResult = [this] (char unsigned* uuid) {
														      return _resolveTransmissionLossMeasurementResult (uuid) ;
														    } ;

      MDB_val                            crystal_measurement_result_key = _resolveKey (uuid) ;
      MDB_val                            crystal_measurement_result_value ;
      MDB_dbi                            crystal_measurement_result_dbi ;
      int                                res ;
      
      // Load crystalmeasurementresult data
      mdb_dbi_open (_txn, CRYSTAL_MEASUREMENT_RESULT_DATABASE_NAME, MDB_CREATE, &crystal_measurement_result_dbi) ;
      res = mdb_get (_txn, crystal_measurement_result_dbi , &crystal_measurement_result_key, &crystal_measurement_result_value) ;
      
      if (res == 0) {
	return demarshall (crystal_measurement_result_key, crystal_measurement_result_value, getTransmissionLossMeasurementResult) ;
      } else {
	return NULL ;
      }
    }

    /*************************************************************************
     *  Method: Repository::_resolveTransmissionLossMeasurementResult
     ************************************************************************/
    TransmissionLossMeasurementResult* Repository::_resolveTransmissionLossMeasurementResult (char unsigned* uuid) {
      
      MDB_val                             transmission_loss_measurement_result_key = _resolveKey (uuid) ;
      MDB_val                             transmission_loss_measurement_result_value ;
      MDB_dbi                             transmission_loss_measurement_result_dbi ;
      TransmissionLossMeasurementResult*  transmission_loss_measurement_result ;
      int                                 res ;
      
      // Load transmissionlossmeasurementresult data
      mdb_dbi_open (_txn, TRANSMISSION_LOSS_MEASUREMENT_RESULT_DATABASE_NAME, MDB_CREATE, &transmission_loss_measurement_result_dbi) ;
      res = mdb_get (_txn, transmission_loss_measurement_result_dbi, &transmission_loss_measurement_result_key, &transmission_loss_measurement_result_value) ;    
      
      if (res == 0) {
	demarshall (transmission_loss_measurement_result_key, transmission_loss_measurement_result_value, transmission_loss_measurement_result) ;
	return transmission_loss_measurement_result ;
      } else {
	return NULL ;
      }
    }



    /*************************************************************************
     *  Method: Repository::_resolveCrystalImpedanceVsFrequencyComputationResult
     ************************************************************************/
    CrystalImpedanceVsFrequencyComputationResult* Repository::_resolveCrystalImpedanceVsFrequencyComputationResult (char unsigned* uuid) {

      MDB_val                                        crystal_impedance_vs_frequency_computation_result_key   = _resolveKey (uuid) ;
      MDB_val                                        crystal_impedance_vs_frequency_computation_result_value ;
      MDB_dbi                                        crystal_impedance_vs_frequency_computation_result_dbi ;
      CrystalImpedanceVsFrequencyComputationResult*  crystal_impedance_vs_frequency_computation_result ;
      int                                            res ;
				     
      // Load crystalimpedancevsfrequencycomputationresult data
      mdb_dbi_open (_txn, CRYSTAL_IMPEDANCE_VS_FREQUENCY_COMPUTATION_RESULT_DATABASE_NAME, MDB_CREATE, &crystal_impedance_vs_frequency_computation_result_dbi) ;
      res = mdb_get (_txn, crystal_impedance_vs_frequency_computation_result_dbi, &crystal_impedance_vs_frequency_computation_result_key, &crystal_impedance_vs_frequency_computation_result_value) ;
      
      if (res == 0) {
	demarshall (crystal_impedance_vs_frequency_computation_result_key, crystal_impedance_vs_frequency_computation_result_value, crystal_impedance_vs_frequency_computation_result) ;
	return crystal_impedance_vs_frequency_computation_result ;
      } else {
	return NULL ;
      }
      
    }
    
  }
} // namespace LibChristel
