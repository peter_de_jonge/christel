/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_CRYSTAL_MEASUREMENT_RESULT_HPP
#define LIBCRISTEL_CRYSTAL_MEASUREMENT_RESULT_HPP

#include <TransmissionLossMeasurementResult.hpp>
#include <Model.hpp>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <string>

namespace Christel {
  namespace Lib {

    // Forward declaration CrystalMeasurement
    class CrystalMeasurement;

    class CrystalMeasurementResult : public Model {
      friend CrystalMeasurement;
    public:
      CrystalMeasurementResult (TransmissionLossMeasurementResult* transmission_loss_measurement_result,
                                unsigned int*                      resonance_indexes,
                                double*                            resonance_frequencies,
                                double*                            resonance_losses,
                                double*                            resonance_series_resistances,
				double                             fixture_system_impedance,
                                unsigned int                       n_resonance_points) ;
      
      CrystalMeasurementResult (unsigned char*                     uuid,
				TransmissionLossMeasurementResult* transmission_loss_measurement_result,
                                unsigned int*                      resonance_indexes,
                                double*                            resonance_frequencies,
                                double*                            resonance_losses,
                                double*                            resonance_series_resistances,
				double                             fixture_system_impedance,
                                unsigned int                       n_resonance_points) ;
      
      TransmissionLossMeasurementResult* getTransmissionLossMeasurementResult () ;		
      unsigned int                       getSeriesResonancePoints (unsigned int*& resonance_indexes,
                                                                   double*&       resonance_frequencies,
                                                                   double*&       resonance_losses,
                                                                   double*&       resonance_series_resistances) ;
      double                             getFixtureSystemImpedance () ;
    private:
      TransmissionLossMeasurementResult* _transmission_loss_measurement_result ;
      double                             _fixture_system_impedance ;
      unsigned int*                      _resonance_indexes ;
      double*                            _resonance_frequencies ;
      double*                            _resonance_losses ;
      double*                            _resonance_series_resistances ;
      unsigned int                       _n_resonance_points ;
    };

  } // Lib
} // namespace LibChristel

#endif // LIBCRISTEL_CRYSTAL_MEASUREMENT_RESULT_HPP
