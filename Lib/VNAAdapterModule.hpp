/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_VNA_ADAPTER_MODULE_HPP
#define LIBCRISTEL_VNA_ADAPTER_MODULE_HPP

#include <VNAAdapter.hpp>
#include <Module.hpp>

namespace Christel::Lib {
  class VNAAdapterManager ;
  
  class VNAAdapterModule : public Module {
    friend class VNAAdapterManager ;
  public:
    VNAAdapterMeta*  getVNAAdapterMeta () ;
  private:

    // A function pointer
    typedef Christel::Lib::VNAAdapter* (CreateVNAAdapterFunction) (void) ;
    
    // A function pointer
    typedef Christel::Lib::VNAAdapterMeta* (GetVNAAdapterMetaFunction) (void) ;

    
    VNAAdapter* _adapter ;
    
    VNAAdapterModule (boost::filesystem::path file_path) ;
    VNAAdapter*      createVNAAdapter () ;
    bool             unload () ;
    bool             load () ;
  } ;

} // namespace Christel::Lib

#endif // LIBCRISTEL_VNA_MODULE_ADAPTER_HPP
