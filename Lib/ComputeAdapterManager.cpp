/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <ComputeAdapterManager.hpp>
#include <Compute.hpp>
#include <iostream>
#include <Path.hpp>

namespace Christel::Lib {

  ComputeAdapterManager* _compute_adapter_manager = NULL ;
    
  /*************************************************************************
   *  Method: ComputeAdapterManager::getComputeAdapterManager
   ************************************************************************/
  ComputeAdapterManager* ComputeAdapterManager::getComputeAdapterManager () {
    if (!_compute_adapter_manager) {
      _compute_adapter_manager = new ComputeAdapterManager () ;
    } 
    return _compute_adapter_manager ;
  }

  /***************************************************************************
   *  Constructor: ComputeAdapterManager::ComputeAdapterManager
   **************************************************************************/
  ComputeAdapterManager::ComputeAdapterManager () {
    _current_adapter = nullptr ;
    _current_module  = nullptr ;
    _resolveModules (Path::getComputeAdaptersDirectoryPath ()) ;
    if (_modules.size () > 0) { // TEMPORARLY select portable adapter
      setActiveComputeAdapterModule (_modules[0]) ;
    }
  }

  /**************************************************************************
   *  Method:: ComputeAdapterManager::resolveModules
   *************************************************************************/
  void ComputeAdapterManager::_resolveModules (boost::filesystem::path module_dir_path) {
    std::cout << module_dir_path.string() << std::endl ;
    ComputeAdapterModule* module = nullptr ;
    
    if (boost::filesystem::is_directory (module_dir_path)) {
      boost::filesystem::directory_iterator end_iter ;
      
      for (boost::filesystem::directory_iterator dir_itr (module_dir_path);
	   dir_itr != end_iter;
	   ++dir_itr) {
	try {
	  if ((boost::filesystem::is_regular_file (dir_itr->status ())) &&
              (_isDynamicLoadLibrary (dir_itr->path ()))) {
	    // TODO: check if module is already loaded
	    std::cout << dir_itr->path ().string() << std::endl ;
	    module = new ComputeAdapterModule (dir_itr->path ()) ;
	    module->load () ; // TODO: load only on use
	    _modules.push_back (module) ;
	  }
	}
	catch (const std::exception & ex) {
	  std::cout << dir_itr->path ().filename () << " " << ex.what () << std::endl ;
	}
      }
    }
  }
  
  /**************************************************************************
   *  Method:: ComputeAdapterManager::_isDynamicLoadLibrary
   ***************************************************************************/
  bool ComputeAdapterManager::_isDynamicLoadLibrary (boost::filesystem::path module_dir_path) {
    std::string extension = boost::filesystem::extension (module_dir_path) ;
    bool is_dynamic_load_library ;
    is_dynamic_load_library = (extension == ".so") ;
    // TODO: implement magic check (libmagic)
    // TODO: implement solution for Windows
     
    return is_dynamic_load_library ;
  }
 

  /**************************************************************************
   *  Method:: ComputeAdapterManager::getComputeAdapterModules
   *************************************************************************/
  std::vector<ComputeAdapterModule*>  ComputeAdapterManager::getComputeAdapterModules () {
    return _modules ;
  }

  /**************************************************************************
   *  Method:: ComputeAdapterManager::setActiveComputeAdapterModule
   *************************************************************************/
  void ComputeAdapterManager::setActiveComputeAdapterModule (ComputeAdapterModule* compute_adapter_module) {
    Compute*            compute = Compute::getCompute () ;
    ComputeAdapterMeta* compute_adapter_meta = nullptr ;
    
    // TODO: make this thread safe/atomic
    if (_current_adapter) {
      if (_current_adapter->inUse ()) {
	return ;
      } else {
	compute->setComputeAdapter (nullptr) ;
	active_adapter_changed.emit (nullptr, nullptr) ;
	// TODO: destroy current adapter ;
	_current_adapter = nullptr ;
	_current_module = nullptr ;

      }
    }
    _current_module = compute_adapter_module ;
    compute_adapter_meta = _current_module->getComputeAdapterMeta () ;
    if (compute_adapter_meta != nullptr) {
      _current_adapter = _current_module->createComputeAdapter () ;
      active_adapter_changed.emit (_current_adapter, compute_adapter_meta) ;
      compute->setComputeAdapter (_current_adapter) ;
      active_adapter_changed.emit (nullptr, nullptr) ;
    }   
  }

  /**************************************************************************
   *  Method:: ComputeAdapterManager::getActiveComputeAdapter
   *************************************************************************/
  void ComputeAdapterManager::getActiveComputeAdapter (ComputeAdapter*&     compute_adapter,
						       ComputeAdapterMeta*& compute_adapter_meta) {
    compute_adapter = _current_adapter ;
    compute_adapter_meta = compute_adapter_meta = _current_module->getComputeAdapterMeta () ;
  }

  
} // namespace Christel::Lib
