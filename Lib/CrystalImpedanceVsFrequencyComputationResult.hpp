/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_CRYSTAL_IMPEDANCE_VS_FREQUENCY_COMPUTATION_RESULT_HPP
#define LIBCRISTEL_CRYSTAL_IMPEDANCE_VS_FREQUENCY_COMPUTATION_RESULT_HPP

#include <Model.hpp>

namespace Christel {
  namespace Lib {
    
    class CrystalImpedanceVsFrequencyComputationResult : public Model {
    public:
      CrystalImpedanceVsFrequencyComputationResult (double*       impedances,
						    double*       frequencies,
						    unsigned int  n_samples) ;


      CrystalImpedanceVsFrequencyComputationResult (unsigned char*  uuid,
						    double*         impedances,
						    double*         frequencies,
						    unsigned int    n_samples) ;
      
      unsigned int getData (double*& impedances,
			    double*& frequencies) ;

      unsigned int getRealParts (double*& impedance_real_parts,
				 double*& frequencies) ;

      unsigned int getImaginaryParts (double*& impedance_imaginary_parts,
				      double*& frequencies) ;

      
			    
    private:
      double*       _impedances ;
      double*       _impedance_real_parts ;
      double*       _impedance_imaginary_parts ;
      double*       _frequencies ;
      unsigned int  _n_samples ;

      void          _resolveImpedanceParts () ;

      
  } ;
    
  } // namespace Lib 
} // namespace Christel

#endif // LIBCRISTEL_CRYSTAL_IMPEDANCE_VS_FREQUENCY_COMPUTATION_RESULT_HPP
