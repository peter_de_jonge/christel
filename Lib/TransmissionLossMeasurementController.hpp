/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#ifndef LIBCRISTEL_TRANSMISSION_LOSS_MEASUREMENT_HPP
#define LIBCRISTEL_TRANSMISSION_LOSS_MEASUREMENT_HPP

#include <TransmissionLossMeasurementResult.hpp>
#include <sigc++/sigc++.h>
#include <VNAAdapter.hpp>
#include <Model.hpp>
#include <glibmm.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>


namespace Christel {
  namespace Lib {
    
    class VNAAdapterManager ;
    
    class TransmissionLossMeasurementController {
      friend class VNAAdapterManager ;
    public:
      static TransmissionLossMeasurementController *getTransmissionLossMeasurementController () ;
      TransmissionLossMeasurementResult* doMeasure (unsigned int  f_start,
						    unsigned int  f_stop,
						    unsigned int  f_steps) ;
						   
			
    private:
      VNAAdapter* _vna_adapter ;
      TransmissionLossMeasurementController () ;
      VNAAdapter* getVNAAdapter () ; 
      void        setVNAAdapter (VNAAdapter* vna_adapter) ;
    } ;
		
  } // namespace Lib
} // namespace Christel

#endif // LIBCRISTEL_TRANSMISSION_LOSS_MEASUREMENT_HPP
