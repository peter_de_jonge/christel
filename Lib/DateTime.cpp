/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#include "DateTime.hpp"

namespace Christel {
  namespace Lib {
    /*
      +-----------+------------------------------------------------------------------------+----------------
      | Specifier | Replaced By                                                            | Example
      +-----------+------------------------------------------------------------------------+----------------
      | %a        | Abbreviated weekday name 	                                           | Sun
      | %A        | Full weekday name 	                                                   | Sunday
      | %b        | Abbreviated month name                                                 | Mar
      | %B        | Full month name                                                        | March
      | %c        | Date and time representation 	                                   | Sun Aug 19 02:56:02 2012
      | %d        | Day of the month (01-31) 	                                           | 19
      | %H        | Hour in 24h format (00-23) 	                                           | 14
      | %I        | Hour in 12h format (01-12) 	                                           | 05
      | %j        | Day of the year (001-366) 	                                           | 231
      | %m        | Month as a decimal number (01-12)                                      | 08
      | %M        | Minute (00-59) 	                                                   | 55
      | %p        | AM or PM designation 	                                           | PM
      | %S        | Second (00-61) 	                                                   | 02
      | %U        | Week number with the first Sunday as the first day of week one (00-53) | 33
      | %w        | Weekday as a decimal number with Sunday as 0 (0-6) 	                   | 4
      | %W        | Week number with the first Monday as the first day of week one (00-53) | 34
      | %x        | Date representation 	                                           | 08/19/12
      | %X        | Time representation                                                    | 02:50:06
      | %y        | Year, last two digits (00-99) 	                                   | 01
      | %Y        | Year 	                                                           | 2012
      | %Z        | Timezone name or abbreviation 	                                   | CDT
      | %%        | A % sign                                                               | %
      +-----------+------------------------------------------------------------------------+----------------
    */
    DateTime::DateTime() {
      time(&_time);
      _format = "%Y-%m-%d  %H:%M:%S";
      _formatDateTime();
    }

    std::string DateTime::_formatDateTime() {
      struct tm *tm = localtime(&_time);
      char tmp_buffer[100];
      strftime(tmp_buffer, 100, _format.c_str(), tm);
      _formatted_date_time = std::string(tmp_buffer);
      return _formatted_date_time;
    }

    std::ostream &operator<<(std::ostream &os, const DateTime &datetime) {
      return os << datetime._formatted_date_time;
    }
  }
}
