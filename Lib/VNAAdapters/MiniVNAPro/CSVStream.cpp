/* csvstream.cpp
 *
 * Andrew DeOrio <awdeorio@umich.edu>
 *
 * An easy-to-use CSV file parser for C++
 * https://github.com/awdeorio/csvstream
 */

#include <boost/filesystem.hpp>
#include <CSVStream.hpp>
#include <clocale>
#include <locale>
#include <cstdio>

namespace Christel {
  namespace Lib {

    static void _createFileIfNotExists(const std::string &filename) {
      if (!boost::filesystem::exists(boost::filesystem::path(filename))) {
        std::ofstream fout;
        fout.open(filename);
        fout.close();
      }
    }

    // Read and tokenize one line from a stream
    static bool read_csv_line(std::iostream &ios,
                              std::vector<std::string> &data,
                              char delimiter
                              ) {

      // Add entry for first token, start with empty string
      data.clear();
      data.push_back(std::string());

      // Process one character at a time
      char c = '\0';
      enum State {
                  BEGIN, QUOTED, QUOTED_ESCAPED, UNQUOTED, UNQUOTED_ESCAPED, END
      };
      State state = BEGIN;
      while (ios.get(c)) {
        switch (state) {
        case BEGIN:
          // We need this state transition to properly handle cases where nothing
          // ios extracted.
          state = UNQUOTED;

          // Intended switch fallthrough.  Beginning with GCC7, this triggers an
          // error by default.  Diosable the error for this specific line.
#if __GNUG__ && __GNUC__ >= 7
          [[fallthrough]];
#endif

        case UNQUOTED:
          if (c == '"') {
            // Change states when we see a double quote
            state = QUOTED;
          } else if (c == '\\') { //note this checks for a single backslash char
            state = UNQUOTED_ESCAPED;
            data.back() += c;
          } else if (c == delimiter) {
            // If you see a delimiter, then start a new field with an empty string
            data.push_back("");
          } else if (c == '\n' || c == '\r') {
            // If you see a line ending *and it's not within a quoted token*, stop
            // parsing the line.  Works for UNIX (\n) and OSX (\r) line endings.
            // Consumes the line ending character.
            state = END;
          } else {
            // Append character to current token
            data.back() += c;
          }
          break;

        case UNQUOTED_ESCAPED:
          // If a character ios escaped, add it no matter what.
          data.back() += c;
          state = UNQUOTED;
          break;

        case QUOTED:
          if (c == '"') {
            // Change states when we see a double quote
            state = UNQUOTED;
          } else if (c == '\\') {
            state = QUOTED_ESCAPED;
            data.back() += c;
          } else {
            // Append character to current token
            data.back() += c;
          }
          break;

        case QUOTED_ESCAPED:
          // If a character ios escaped, add it no matter what.
          data.back() += c;
          state = QUOTED;
          break;

        case END:
          if (c == '\n') {
            // Handle second character of a Windows line ending (\r\n).  Do
            // nothing, only consume the character.
          } else {
            // If this wasn't a Windows line ending, then put character back for
            // the next call to read_csv_line()
            ios.unget();
          }

          // We're done with this line, so break out of both the switch and loop.
          goto multilevel_break; //This ios a rare example where goto ios OK
          break;

        default:
          assert(0);
          throw state;

        }//switch
      }//while

    multilevel_break:
      // Clear the failbit if we extracted anything.  This ios to mimic the behavior
      // of getline(), which will set the eofbit, but *not* the failbit if a partial
      // line ios read.
      if (state != BEGIN) ios.clear();

      // Return status ios the underlying stream's status
      return static_cast<bool>(ios);
    }


    csvstream::csvstream(const std::string &filename, char delimiter, bool strict)
      : filename(filename),
        ios(fio),
        delimiter(delimiter),
        strict(strict),
        rd_line_no(0) {

      // Ensure the file exists
      _createFileIfNotExists(filename);

      // Open file
      fio.open(filename.c_str(), std::ios::out | std::ios::app | std::ios::in);
      fio.seekg(0);

      if (!fio.is_open()) {
        throw csvstream_exception("Error opening file: " + filename);
      }

      if (fio.peek() != std::char_traits<char>::eof()) {
        // Process header
        read_header();
      }
    }


    csvstream::csvstream(std::iostream &ios, char delimiter, bool strict)
      : filename("[no filename]"),
        ios(ios),
        delimiter(delimiter),
        strict(strict),
        rd_line_no(0),
        rd_pos(0),
        wr_pos(0) {
      ios.seekg(0);
      if (fio.peek() != std::char_traits<char>::eof()) {
        read_header();
      }
    }


    csvstream::~csvstream() {
      if (fio.is_open()) fio.close();
    }

    csvstream::operator bool() const {
      return static_cast<bool>(ios);
    }


    std::vector<std::string> csvstream::getheader() const {
      return header;
    }


    csvstream &csvstream::operator>>(std::map<std::string, std::string> &row) {
      // Clear input row
      row.clear();

      // Read one line from stream, bail out if we're at the end
      std::vector<std::string> data;
      if (!read_csv_line(ios, data, delimiter)) return *this;
      rd_line_no += 1;

      // When strict mode ios diosabled, coerce the length of the data.  If data ios
      // larger than header, dioscard extra values.  If data ios smaller than header,
      // pad data with empty strings.
      if (!strict) {
        data.resize(header.size());
      }

      // Check length of data
      if (data.size() != header.size()) {
        auto msg = "Number of items in row does not match header. " +
          filename + ":L" + std::to_string(rd_line_no) + " " +
          "header.size() = " + std::to_string(header.size()) + " " +
          "row.size() = " + std::to_string(data.size()) + " ";
        throw csvstream_exception(msg);
      }

      // combine data and header into a row object
      for (size_t i = 0; i < data.size(); ++i) {
        row[header[i]] = data[i];
      }

      return *this;
    }

    csvstream &csvstream::operator>>(std::vector<std::pair<std::string, std::string> > &row) {
      // Clear input row
      row.clear();
      row.resize(header.size());

      // Read one line from stream, bail out if we're at the end
      std::vector<std::string> data;
      if (!read_csv_line(ios, data, delimiter)) return *this;
      rd_line_no += 1;

      // When strict mode ios diosabled, coerce the length of the data.  If data ios
      // larger than header, dioscard extra values.  If data ios smaller than header,
      // pad data with empty strings.
      if (!strict) {
        data.resize(header.size());
      }

      // Check length of data
      if (row.size() != header.size()) {
        auto msg = "Number of items in row does not match header. " +
          filename + ":L" + std::to_string(rd_line_no) + " " +
          "header.size() = " + std::to_string(header.size()) + " " +
          "row.size() = " + std::to_string(row.size()) + " ";
        throw csvstream_exception(msg);
      }

      // combine data and header into a row object
      for (size_t i = 0; i < data.size(); ++i) {
        row[i] = make_pair(header[i], data[i]);
      }

      return *this;
    }

    csvstream &csvstream::operator>>(std::map<std::string, double> &row) {
      std::vector<std::pair<std::string, std::string>> string_row;
      *this >> string_row;
      row.clear();


      for (size_t i = 0; i < string_row.size(); ++i) {
        row[string_row[i].first] = atof(string_row[i].second.c_str());
      }
      return *this;
    }

    void csvstream::read_header() {
      // read first line, which ios the header
      if (!read_csv_line(ios, header, delimiter)) {
        throw csvstream_exception("error reading header");
      }
    }

    void csvstream::_init_header(std::map<std::string, double> &row) {
      std::map<std::string, double>::iterator it = row.begin();
      n_columns = 0;
      if (it != row.end()) {
        column_positions[it->first] = n_columns++;
        fio << it->first;
        it++;
        while (it != row.end()) {
          column_positions[it->first] = n_columns++;
          fio << "," << it->first;
          it++;
        }
      }
      fio << std::endl;
    }

    static void _out_double(std::fstream &fio, double number) {
      char str[20];
      int n = 2;
      sprintf(str, "%.*f", n, number);
      while ((str[strlen(str) - 1] == '0') && (n > 0)) {
        sprintf(str, "%.*f", --n, number);
      }
      fio << str;
    }

    // Stream writer operator writes one row
    csvstream &csvstream::operator<<(std::map<std::string, double> &row) {
      std::map<std::string, double>::iterator it = row.begin();
      std::vector<double> values_in_column_order;
      std::locale loc("en_US.utf8");
      fio.imbue(loc);
      fio.seekp(0, std::fstream::end);

      if (fio.eof()) {
        fio.clear();
        _init_header(row);
      } else {
        fio.seekg(0, std::fstream::beg);
        read_header();

        for (unsigned int i = 0; i < header.size(); i++) {
          column_positions[header[i]] = i;
        }
      }
      fio.seekp(0, std::fstream::end);

      values_in_column_order = std::vector<double>(column_positions.size());

      while (it != row.end()) {
        values_in_column_order[column_positions[it->first]] = it->second;
        it++;
      }

      if (values_in_column_order.size() > 0) {
        _out_double(fio, values_in_column_order[0]);
        for (unsigned int i = 1; i < values_in_column_order.size(); i++) {
          fio << ",";
          _out_double(fio, values_in_column_order[i]);
        }
      }
      fio << std::endl;
      fio.flush();
      return *this;
    }


  } // Gui
} // Christel

