/* csvstream.h
 *
 * Andrew DeOrio <awdeorio@umich.edu>
 *
 * An easy-to-use CSV file parser for C++
 * https://github.com/awdeorio/csvstream
 */


#ifndef CSVSTREAM_H
#define CSVSTREAM_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <string>
#include <vector>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <regex>
#include <exception>

namespace Christel {
  namespace Lib {

    // A custom exception type
    class csvstream_exception : public std::exception {
    public:
      const char *what() const noexcept {
        return msg.c_str();
      }

      const std::string msg;

      csvstream_exception(const std::string &msg) : msg(msg) {};
    };


    // csvstream interface
    class csvstream {
    public:
      // Constructor from filename
      csvstream(const std::string &filename, char delimiter = ',', bool strict = true);

      // Constructor from stream
      csvstream(std::iostream &ios, char delimiter = ',', bool strict = true);

      // Destructor
      ~csvstream();

      // Return false if an error flag on underlying stream is set
      explicit operator bool() const;

      // Return header processed by constructor
      std::vector<std::string> getheader() const;

      // Stream extraction operator reads one row
      csvstream &operator>>(std::map<std::string, std::string> &row);

      // Stream extraction operator reads one row, keeping column order
      csvstream &operator>>(std::vector<std::pair<std::string, std::string> > &row);

      // Stream extraction operator reads one row
      csvstream &operator>>(std::map<std::string, double> &row);

      // Stream writer operator writes one row
      csvstream &operator<<(std::map<std::string, double> &row);


    private:
      // Filename.  Used for error messages.
      std::string filename;

      // File stream in CSV format, used when library is called with filename ctor
      std::fstream fio;

      // Stream in CSV format
      std::iostream &ios;

      // Delimiter between columns
      char delimiter;

      // Strictly enforce the number of values in each row.  Raise an exception if
      // a row contains too many values or too few compared to the header.  When
      // strict=false, ignore extra values and set missing values to empty string.
      bool strict;

      // Line no in file.  Used for error messages
      size_t rd_line_no;

      size_t rd_pos;

      size_t wr_pos;

      std::map<std::string, unsigned int> column_positions;
      unsigned int n_columns;

      // Store header column names
      std::vector<std::string> header;

      // Process header, the first line of the file
      void read_header();

      void _init_header(std::map<std::string, double> &row);

      // Disable copying because copying streams is bad!
      csvstream(const csvstream &);

      csvstream &operator=(const csvstream &);
    };

  } // Gui
} // Christel
#endif
