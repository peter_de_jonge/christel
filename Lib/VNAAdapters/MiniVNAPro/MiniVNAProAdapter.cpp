/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include "MiniVNAProAdapter.hpp"
#include <boost/filesystem.hpp>
#include "CSVStream.hpp"

namespace Christel::Lib::MiniVNAProAdapter {

  const VNAAdapterMeta AdapterMeta = {"miniVNA Pro", "0.0.0"} ;
			    
  DEFINE_CHRISTAL_VNA_ADAPTER_ENTRY_FUNCTIONS(MiniVNAProAdapter,AdapterMeta) ;

  /***************************************************************************
   *  Contructor: MiniVNAProAdapter::MiniVNAProAdapter
   **************************************************************************/
  MiniVNAProAdapter::MiniVNAProAdapter () {
  }

  
  /***************************************************************************
   *  Method: MiniVNAProAdapter::doTransmissionLossMeasurement
   **************************************************************************/
  size_t MiniVNAProAdapter::doTransmissionLossMeasurement (unsigned int f_start,
							   unsigned int f_steps,
							   unsigned int f_stop,
							   double*&     frequencies,
							   double*&     losses,
							   double*&     phases,
							   double*&     series_resistances) {
    
    std::string  temp_export_filename = tempnam("/tmp", "christel_minivna") ;
        
    // Do a fast scan first to resolve the strange quirk of the miniVNA device/command line tool
    // After te miniVNJA is plugged in the first measurement fails therefor
    // do a fast scan first to resolve the strange quirk vnaJ commandline tool
    _executeScanCommand (f_start,
			 f_stop,
			 100,
			 temp_export_filename) ;
      
    _executeScanCommand (f_start,
			 f_stop,
			 f_steps,
			 temp_export_filename) ;
    return _readScanResult (frequencies,
			    losses,
			    phases,
			    series_resistances,
			    temp_export_filename) ;
  }
 
  /*************************************************************************
   * Method: MiniVNAProAdapter::_executeScanCommand
   ************************************************************************/
  void MiniVNAProAdapter::_executeScanCommand(unsigned int f_start,
					      unsigned int f_stop,
					      unsigned int f_steps,				  
					      std::string  temp_export_filename) {
    unsigned int n_averaging_cycles   = getNAveragingCycles () ;
    std::string  calibration_filename = getCalibrationFile () ;
    std::string  vna_usb_port_path    = getUSBPortPath () ;
    char buffer[2048] ;
    
    sprintf(buffer,
	    "java -Dfstart=%d -Dfstop=%d -Dfsteps=%d -DdriverId=2 -Daverage=%d -DdriverPort=%s  -Dcalfile=%s -Dscanmode=TRAN -Dexports=csv -DexportDirectory=\"/\" -DexportFilename=\"%s\" -DkeepGeneratorOn -DnumberOfScans=1 -jar %s",
	    f_start,
	    f_stop,
	    f_steps,
	    n_averaging_cycles,
	    vna_usb_port_path.c_str (),
	    calibration_filename.c_str (),
	    temp_export_filename.c_str (),
	    _getVNAJarFilePath ().c_str ()) ;
    std::cout << buffer << std::endl ;
    system(buffer) ;
  }


  /*************************************************************************
   * Method: MiniVNAProAdapter::_readScanResult
   ************************************************************************/
  size_t MiniVNAProAdapter::_readScanResult (double*&    frequencies,
					     double*&    losses,
					     double*&    phases,
					     double*&    series_resistances,
					     std::string temp_export_filename) {
    csvstream                     csvin (std::string (temp_export_filename + ".csv").c_str ()) ;
    int                           old_precision = std::cout.precision () ;
    double*                       freq = NULL ;
    double*                       loss = NULL ;
    double*                       phase = NULL ;
    double*                       rs = NULL ;
    int                           count = 0 ;
    std::map<std::string, double> row ;
			
    setlocale (LC_ALL, "en_US.utf8") ; // Decimal separator is dot not comma
    while (csvin >> row) {
      count++ ;
      freq = (double *) realloc (freq, sizeof(double) * count) ;
      loss = (double *) realloc (loss, sizeof(double) * count) ;
      phase = (double *) realloc (phase, sizeof(double) * count) ;
      rs = (double *) realloc (rs, sizeof(double) * count) ;

      freq[count - 1] = row["Frequency(Hz)"] ;
      loss[count - 1] = row["Transmission Loss(dB)"] ;
      phase[count - 1] = row["Phase(deg)"] ;
      rs[count - 1] = row["Rs"] ;
    }
			
    setlocale (LC_ALL, "") ;         // Restore decimal separator

    frequencies = freq ;
    losses = loss ;
    phases = phase ;
    series_resistances = rs ;
    return count ;
  }   

  /*************************************************************************
   * Method: TransmissionLossMeasurementController::_getVNAJarFilePath
   ************************************************************************/
  std::string MiniVNAProAdapter::_getVNAJarFilePath () {
    const boost::filesystem::path path = boost::filesystem::path (VNA_JAR_HEADLESS_FILE) ;
    return path.string () ;
  }
  
} // namespace Christel::Lib::MiniVNAProAdapter 
