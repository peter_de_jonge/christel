/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef LIBCRISTEL_MINI_VNA_PRO_ADAPTER_HPP
#define LIBCRISTEL_MINI_VNA_PRO_ADAPTER_HPP

#include <VNAAdapter.hpp>

namespace Christel::Lib::MiniVNAProAdapter {
      
  class MiniVNAProAdapter : public Christel::Lib::VNAAdapter {
  public:
    MiniVNAProAdapter() ;
    size_t doTransmissionLossMeasurement (unsigned int f_start,
					  unsigned int f_stop,
					  unsigned int f_steps,
					  double*&     frequencies,
					  double*&     losses,
					  double*&     phases,
					  double*&     series_resistances) ;
      
  private:
    void  _executeScanCommand (unsigned int f_start,
			       unsigned int f_stop,
			       unsigned int f_steps,
			       std::string  temp_export_filename) ;

    size_t _readScanResult (double*&    frequencies,
			    double*&    losses,
			    double*&    phases,
			    double*&    series_resistances,
			    std::string temp_export_filename) ;
    
    std::string _getVNAJarFilePath () ;
  } ;
    
} // namespace Christel::Lib::MiniVNAProAdapter 

#endif // LIBCRISTEL_MINI_VNA_PRO_ADAPTER_HPP
