/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <Module.hpp>

namespace Christel::Lib {

  /***************************************************************************
   *  Module::Module::Module
   **************************************************************************/
  Module::Module (boost::filesystem::path file_path) :
    _file_path (file_path), _handle (nullptr) {
  }

 
  /***************************************************************************
   *  Module::Module::isLoaded
   **************************************************************************/
  bool Module::isLoaded () {
    return _handle != nullptr ;
  }
  
  /***************************************************************************
   *  Module::load
   **************************************************************************/
  bool Module::load () {
    const char* path = _file_path.string ().c_str () ;
    if (isLoaded ()) {
      std::cout << "Module " <<  path << " already loaded" <<  std::endl ;
    } else {
      _handle = dlopen (path, RTLD_LAZY) ;
      if (!_handle) {
        std::cerr << "Failed to load " <<  path << std::endl ;
        std::cerr << dlerror () << std::endl ;
        return false ;
      }
      
      std::cout << "Loaded: " <<  path << std::endl ;
    }
    return true ;
  }

  /***************************************************************************
   *  Module::unload
   **************************************************************************/
  bool Module::unload () {
    const char* path = _file_path.string ().c_str () ;
    char* error ;
    if (_handle) {
      dlclose (_handle) ;
      _handle = nullptr ;
      error = dlerror () ;
      if (strlen (error) > 0) { 
	std::cerr << dlerror () << std::endl;
	std::cerr << "Failed to unload " <<  path << std::endl ;
	return false ;
      }
    }
    return true ;
  }

  
  
} // namespace Christel::Lib
