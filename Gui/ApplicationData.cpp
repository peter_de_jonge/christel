/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <ApplicationData.hpp>

namespace Christel::Gui {
  
  /************************************************************************
   *  Constructor: ApplicationData::ApplicationData
   ***********************************************************************/
  ApplicationData::ApplicationData () {
    _compute_adapter_manager = Christel::Lib::ComputeAdapterManager::getComputeAdapterManager () ;
    _vna_adapter_manager = Christel::Lib::VNAAdapterManager::getVNAAdapterManager () ;
  }

  /************************************************************************
   *  Method: ApplicationData::getComputeAdapterManager
   ***********************************************************************/
  Christel::Lib::ComputeAdapterManager& ApplicationData::getComputeAdapterManager () {
    return *_compute_adapter_manager ;
  }
  
  /************************************************************************
   *  Method: ApplicationData::getVNAAdapterManager
   ***********************************************************************/
  Christel::Lib::VNAAdapterManager& ApplicationData::getVNAAdapterManager () {
     return *_vna_adapter_manager ;
  }
  
  /************************************************************************
   *  Method: ApplicationData::getStockManager
   ***********************************************************************/
  Christel::Lib::CrystalStockManager& ApplicationData::getStockManager () {
    return _stock_manager ;
  }

  /************************************************************************
   *  Method: ApplicationData::setCalibrationFilename
   ***********************************************************************/
  void ApplicationData::setCalibrationFilename (std::string calibration_filename) {
    _calibration_filename = calibration_filename ;
    calibration_filename_changed.emit (calibration_filename) ;
  }

  /************************************************************************
   *  Method: ApplicationData::getCalibrationFilename
   ***********************************************************************/
  std::string ApplicationData::getCalibrationFilename () {
    return _calibration_filename ;
  }

  /************************************************************************
   *  Method: ApplicationData::getStockListModelColumnRecord
   ***********************************************************************/
  StockListModelColumnRecord &ApplicationData::getStockListModelColumnRecord () {
    return _stock_list_model_column_record ;
  }

  /************************************************************************
   *  Method: ApplicationData::haveCalibrationFilename
   ***********************************************************************/
  bool ApplicationData::haveCalibrationFilename () {
    return _calibration_filename.length () != 0 ;
  }

} // namespace Christel::Gui
