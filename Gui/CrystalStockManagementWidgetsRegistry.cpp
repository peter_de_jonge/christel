/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <CrystalStockManagementWidgetsRegistry.hpp>


namespace Christel {
  namespace Gui {

    /*****************************************************************************
     *  Constructor: CrystalStockManagementWidgetsRegistry::CrystalStockManagementWidgetsRegistry
     *****************************************************************************/
    CrystalStockManagementWidgetsRegistry::CrystalStockManagementWidgetsRegistry () {
    }


    /************************************************************************
     * Method: CrystalStockManagementWidgetsRegistry::initPlotArea
     ***********************************************************************/
    void CrystalStockManagementWidgetsRegistry::init () {
      Gtk::Notebook* _crystal_measurement_notebook =getWidget<Gtk::Notebook> ("crystal_stock_management_crystal_figures_notebook") ;
      if (_crystal_measurement_notebook) {
	// Create measured transmissionloss plot area
        _measured_transmission_loss_plotting_area = new Plotmm::PlotArea () ;
        _measured_transmission_loss_plotting_area->show () ;
        _crystal_measurement_notebook->append_page ((*_measured_transmission_loss_plotting_area), "Measured Transmission Loss") ;
	
	// Create measured transmissionphase plot area
        _measured_transmission_phase_plotting_area = new Plotmm::PlotArea () ;
        _measured_transmission_phase_plotting_area->show () ;
        _crystal_measurement_notebook->append_page ((*_measured_transmission_phase_plotting_area), "Measured Transmission Phase") ;

	// Show all plot areas
	_crystal_measurement_notebook->show_all_children (true) ;
      }
    }


    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getNewStockButtonWidget
     ***********************************************************************/
    Gtk::Button *CrystalStockManagementWidgetsRegistry::getNewStockButtonWidget() {
      Gtk::Button *widget = getWidget<Gtk::Button>("new_stock_button");
      return widget;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getNewStockDialog
     ***********************************************************************/
    Gtk::Dialog *CrystalStockManagementWidgetsRegistry::getNewStockDialog() {
      Gtk::Dialog *widget = getWidget<Gtk::Dialog>("new_stock_dialog");
      return widget;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getNewStockNameEntr
     ***********************************************************************/
    Gtk::Entry *CrystalStockManagementWidgetsRegistry::getNewStockNameEntry () {
      Gtk::Entry *widget = getWidget<Gtk::Entry> ("new_stock_name_entry");
      return widget;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getNewStockSeriesResonanceFrequencySpinButton
     ***********************************************************************/
    Gtk::SpinButton* CrystalStockManagementWidgetsRegistry::getNewStockSeriesResonanceFrequencySpinButton () {
      Gtk::SpinButton* widget = getWidget<Gtk::SpinButton> ("new_stock_series_resonance_frequency_spin_button") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getNoStockNameErrorDialog
     ***********************************************************************/
    Gtk::Dialog *CrystalStockManagementWidgetsRegistry::getNoStockNameErrorDialog () {
      Gtk::Dialog* widget = getWidget<Gtk::Dialog> ("new_stock_no_stock_name_error_dialog") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getStockListComboBox
     ***********************************************************************/
    Gtk::ComboBox *CrystalStockManagementWidgetsRegistry::getStockListComboBox () {
      Gtk::ComboBox *widget = getWidget<Gtk::ComboBox> ("stock_list_combobox") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getStockSeriesResonanceFrequencyLabel
     ***********************************************************************/
    Gtk::Label* CrystalStockManagementWidgetsRegistry::getStockSeriesResonanceFrequencyLabel () {
      Gtk::Label* widget = getWidget<Gtk::Label> ("stock_series_resonance_frequency_label") ;
      return widget ;
    }
    
    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getgetMeasurementVNADeviceComboBox
     ***********************************************************************/
    Gtk::ComboBox* CrystalStockManagementWidgetsRegistry::getMeasurementVNADeviceComboBox () {
      Gtk::ComboBox* widget = getWidget<Gtk::ComboBox> ("crystal_stock_management_vna_device_combobox") ;
      return widget ;
    }
    
    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getMeasurementVNAPortComboBox
     ***********************************************************************/
    Gtk::ComboBox* CrystalStockManagementWidgetsRegistry::getMeasurementVNAPortComboBox () {
      Gtk::ComboBox* widget = getWidget<Gtk::ComboBox> ("crystal_stock_management_vna_port_combobox") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getVNAPortScanButton
     ***********************************************************************/
    Gtk::Button* CrystalStockManagementWidgetsRegistry::getVNAPortScanButton () {
      Gtk::Button* widget = getWidget<Gtk::Button> ("crystal_stock_management_vna_port_scan_button") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getMeasurementFixtureImpedanceSpinbutton () ;
     ***********************************************************************/
    Gtk::SpinButton* CrystalStockManagementWidgetsRegistry::getMeasurementFixtureImpedanceSpinbutton () {
      Gtk::SpinButton* widget = getWidget<Gtk::SpinButton> ("crystal_stock_management_measurement_fixture_impedance_spinbutton") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getCalibrationFileBrowseButton
     ***********************************************************************/
    Gtk::Button* CrystalStockManagementWidgetsRegistry::getCalibrationFileBrowseButton () {
      Gtk::Button* widget = getWidget<Gtk::Button> ("crystal_stock_management_measurement_calibration_file_browse_button") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getCalibrationFileEntry
     ***********************************************************************/
    Gtk::Entry* CrystalStockManagementWidgetsRegistry::getCalibrationFileEntry() {
      Gtk::Entry *widget = getWidget<Gtk::Entry>("crystal_stock_management_measurement_calibration_file_entry") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getSelectCalibrationFileDialog
     ***********************************************************************/
    Gtk::FileChooserDialog* CrystalStockManagementWidgetsRegistry::getSelectCalibrationFileDialog () {
      Gtk::FileChooserDialog* widget = getWidget<Gtk::FileChooserDialog> ("select_calibration_file_dialog") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getMeasurementNumberOfAveragingSpinbutton
     ***********************************************************************/
    Gtk::SpinButton* CrystalStockManagementWidgetsRegistry::getMeasurementNumberOfAveragingCyclesSpinbutton () {
      Gtk::SpinButton* widget = getWidget<Gtk::SpinButton> ("crystal_stock_management_measurement_number_of_averaging_cycles_spinbutton") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getMeasurementFillGapsInIdCheckButton
     ***********************************************************************/
    Gtk::CheckButton* CrystalStockManagementWidgetsRegistry::getMeasurementFillGapsInIdCheckButton () {
      Gtk::CheckButton* widget = getWidget<Gtk::CheckButton> ("crystal_stock_management_measurement_fill_gaps_in_id_checkbutton") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getMeasurementStartButton
     ***********************************************************************/
    Gtk::Button* CrystalStockManagementWidgetsRegistry::getMeasurementStartButton () {
      Gtk::Button* widget = getWidget<Gtk::Button> ("crystal_stock_management_measurement_start_button") ;
      return widget ;
    }

    /************************************************************************
     * Method: CrystalStockManagementWidgetsRegistry::getMeasurementStatusLabel () 
     ***********************************************************************/
    Gtk::Label* CrystalStockManagementWidgetsRegistry::getMeasurementStatusLabel () {
      Gtk::Label* widget = getWidget<Gtk::Label> ("crystal_stock_management_measurement_status_label") ;
      return widget ;
    }

    /************************************************************************
     * Method: CrystalStockManagementWidgetsRegistry::getMeasurementStatusSpinner () 
     ***********************************************************************/
    Gtk::Spinner* CrystalStockManagementWidgetsRegistry::getMeasurementStatusSpinner () {
      Gtk::Spinner* widget = getWidget<Gtk::Spinner> ("crystal_stock_management_measurement_status_spinner") ;
      return widget ;
    }
    
    /************************************************************************
     * Method: CrystalStockManagementWidgetsRegistry::getMeasurementMessagesLabel () 
     ***********************************************************************/
    Gtk::Label* CrystalStockManagementWidgetsRegistry::getMeasurementMessagesLabel () {
      Gtk::Label* widget = getWidget<Gtk::Label> ("crystal_stock_management_measurement_messages_label") ;
      return widget ;
    }
   
    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getXtalParametersTreeview
     ***********************************************************************/
    Gtk::TreeView* CrystalStockManagementWidgetsRegistry::getXtalParametersTreeview () {
      Gtk::TreeView* widget = getWidget<Gtk::TreeView> ("xtal_parameters_treeview") ;
      return widget ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getCrystalStockListStore
     ***********************************************************************/
    Glib::RefPtr<Gtk::ListStore> CrystalStockManagementWidgetsRegistry::getCrystalStockListStore () {
      Glib::RefPtr<Gtk::ListStore> object = getObject<Gtk::ListStore> ("crystal_stock_liststore") ;
      return object ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getCrystalParametersListStore
     ***********************************************************************/
    Glib::RefPtr<Gtk::ListStore> CrystalStockManagementWidgetsRegistry::getCrystalParametersListStore() {
      Glib::RefPtr<Gtk::ListStore> object = getObject<Gtk::ListStore>("xtal_parameters_liststore") ;
      return object ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getVNAPortsListStore
     ***********************************************************************/
    Glib::RefPtr<Gtk::ListStore> CrystalStockManagementWidgetsRegistry::getVNAPortsListStore() {
      Glib::RefPtr<Gtk::ListStore> object = getObject<Gtk::ListStore>("vna_ports_liststore") ;
      return object ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementWidgetsRegistry::getVNADevicesListStore
     ***********************************************************************/
    Glib::RefPtr<Gtk::ListStore> CrystalStockManagementWidgetsRegistry::getVNADevicesListStore() {
      Glib::RefPtr<Gtk::ListStore> object = getObject<Gtk::ListStore>("vna_devices_liststore") ;
      return object ;
    }
	
    /************************************************************************
     * Method: CrystalStockManagementWidgetsRegistry::getMeasuredTransmissionLossPlotArea
     ***********************************************************************/
    Plotmm::PlotArea* CrystalStockManagementWidgetsRegistry::getMeasuredTransmissionLossPlotArea () {
      return _measured_transmission_loss_plotting_area ;
    }

    /************************************************************************
     * Method: CrystalStockManagementWidgetsRegistry::getMeasuredTransmissionPhasePlotArea
     ***********************************************************************/
    Plotmm::PlotArea* CrystalStockManagementWidgetsRegistry::getMeasuredTransmissionPhasePlotArea () {
      return _measured_transmission_phase_plotting_area ;
    }
  } // namespace Gui
} // namespace Cristel
