/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef CRISTEL_GUI_CRYSTAL_STOCK_MANAGEMENT_CONTROLLER_HPP
#define CRISTEL_GUI_CRYSTAL_STOCK_MANAGEMENT_CONTROLLER_HPP

#include <CrystalStockManagementWidgetsRegistry.hpp>
#include <CrystalMeasurementResult.hpp>
#include <CrystalStockManager.hpp>
#include <VNAAdapterManager.hpp>
#include <Controller.hpp>
#include <Crystal.hpp>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include <iostream>
#include <gtkmm.h>
#include <vector>
#include <string>
#include <limits>

namespace Christel {
  namespace Gui {
		
    class CrystalStockManagementController : public Controller {
			
    public:

      static const int NEW_STOCK_RESPONSE_CANCEL               = 0 ;
      static const int NEW_STOCK_RESPONSE_CREATE               = 1 ;
      static const int NO_STOCK_NAME_ERROR_DIALOG_OK           = 0 ;
      static const int SELECT_CALIBRATION_FILE_RESPONSE_CANCEL = 0 ;
      static const int SELECT_CALIBRATION_FILE_RESPONSE_SELECT = 1 ;

      CrystalStockManagementController () ;
			
      virtual void init () ;

    protected:
      // Handlers
      void onStockAdded (Christel::Lib::CrystalStock*) ;
      void onStockSelected () ;
      void onNewStockButtonClicked () ;
      void onNewStockCancelButtonClicked () ;
      void onCrystalAddedToStock (Christel::Lib::CrystalStock* crystal_stock, Christel::Lib::Crystal* crystal) ;
      void onCrystalAddedToStockDispatch () ;
      void onMeasurementCompletedDispatch () ;
      void onVNADeviceSelected () ;
      void onVNAPortSelected () ;
      void onVNAPortScanButtonClicked () ;
      void onCalibrationFileBrowseButtonClicked () ;
      void onCalibrationFilenameChanged (std::string &calibration_filename) ;
      void onMeasurementNumberOfAveragingCyclesChanged () ;
      void onMeasurementStartButtonClicked () ;
      void onMeasurementFillGapsInIdCheckButtonClicked () ;
      void onXtalParametersTreeviewRowClicked (const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column) ;
      void onXtalParametersTreeviewCursorChanged () ;
    private:
      CrystalStockManagementWidgetsRegistry    _widget_registry ;
      StockListModelColumnRecord*              _stock_list_model_column_record ;
      Christel::Lib::CrystalStockManager*      _stock_manager ;
      Christel::Lib::CrystalStock*             _current_stock ;
      Christel::Lib::Crystal*                  _current_crystal ;
      Christel::Lib::CrystalMeasurementResult* _crystal_measurement_result ;
      sigc::connection                         _crystal_added_connection ;
      bool                                     _fill_gaps_of_ids_in_stock ;
      Glib::Dispatcher                         _on_crystal_added_to_stock_dispatcher ;
      Glib::Dispatcher                         _on_measurement_completed_dispatcher ;
      bool                                     _measurement_in_progress ;

      void                                     _resolveActiveVNAAdapter () ;
      void                                     _resolveMeasurementNumberOfAveragingCycles () ;
      void                                     _resolveVNADevicesList () ;
      void                                     _resolveVNAPortList () ;
      void                                     _newStock (Glib::ustring name, double serie_resonance_frequency) ;
      void                                     _setCurrentStock (Christel::Lib::CrystalStock* ) ;
      void                                     _resolveInitialStocksList () ;
      void                                     _resolveCrystalsList (Christel::Lib::CrystalStock* stock) ;
      void                                     _resolveMeasurementActives () ;
      void                                     _resolveMessages () ;
      bool                                     _haveVNADevices () ;
      bool                                     _haveVNAPorts () ;
      void                                     _startMeasurement () ;
      unsigned int                             _getNextFreeCrystalNr () ;
      void                                     _connectHandlers () ;
      void                                     _plotTransmisionResult () ;
      void                                     _plot () ;
      void                                     _resolve_x_tics (double  f_start,
                                                                double  f_stop,
                                                                double  f_steps,
                                                                double& x_tic_start,
                                                                double& x_tic_stop,
                                                                double& x_tic_increase,
                                                                int&    x_tics_n_decimals) ;
    } ;

  } // namespace Gui
} // namespace Christel

#endif // LIBCRISTEL_GUI_CRYSTAL_STOCK_MANAGEMENT_HPP
