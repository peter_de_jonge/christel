/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef CRISTEL_GUI_CRYSTAL_STOCK_MANAGEMENT_WIDGETS_REGISTRY_HPP
#define CRISTEL_GUI_CRYSTAL_STOCK_MANAGEMENT_WIDGETS_REGISTRY_HPP

#include <WidgetsRegistry.hpp>
#include <PlotArea.hpp>
#include <gtkmm.h>

namespace Christel {
  namespace Gui {
    class CrystalStockManagementWidgetsRegistry : public WidgetsRegistry {
    public:
      CrystalStockManagementWidgetsRegistry () ;
      void                         init () ;
      Gtk::Button*                 getNewStockButtonWidget () ;
      Gtk::Dialog*                 getNewStockDialog () ;
      Gtk::Entry*                  getNewStockNameEntry () ;
      Gtk::SpinButton*             getNewStockSeriesResonanceFrequencySpinButton () ;
      Gtk::Dialog*                 getNoStockNameErrorDialog () ;
      Gtk::ComboBox*               getStockListComboBox () ;
      Gtk::Label*                  getStockSeriesResonanceFrequencyLabel () ;
      Gtk::ComboBox*               getMeasurementVNADeviceComboBox () ;
      Gtk::ComboBox*               getMeasurementVNAPortComboBox () ;
      Gtk::Button*                 getVNAPortScanButton () ;
      Gtk::SpinButton*             getMeasurementFixtureImpedanceSpinbutton () ;
      Gtk::Button*                 getCalibrationFileBrowseButton () ;
      Gtk::Button*                 getCancelSelectCalibrationFileButton () ;
      Gtk::FileChooserDialog*      getSelectCalibrationFileDialog () ;
      Gtk::Entry*                  getCalibrationFileEntry () ;
      Gtk::SpinButton*             getMeasurementNumberOfAveragingCyclesSpinbutton () ;
      Gtk::CheckButton*            getMeasurementFillGapsInIdCheckButton () ;
      Gtk::Button*                 getMeasurementStartButton () ;
      Gtk::Label*                  getMeasurementStatusLabel () ;
      Gtk::Spinner*                getMeasurementStatusSpinner () ;
      Gtk::Label*                  getMeasurementMessagesLabel () ;
      Gtk::TreeView*               getXtalParametersTreeview () ;
      Glib::RefPtr<Gtk::ListStore> getCrystalStockListStore () ;
      Glib::RefPtr<Gtk::ListStore> getCrystalParametersListStore () ;
      Glib::RefPtr<Gtk::ListStore> getVNAPortsListStore () ;
      Glib::RefPtr<Gtk::ListStore> getVNADevicesListStore () ;
      Plotmm::PlotArea*            getMeasuredTransmissionLossPlotArea () ;
      Plotmm::PlotArea*            getMeasuredTransmissionPhasePlotArea () ;
      Plotmm::PlotArea*            getComputedTransmissionLossPlotArea () ;
			
    private:
      Plotmm::PlotArea*            _measured_transmission_loss_plotting_area ; 
      Plotmm::PlotArea*            _measured_transmission_phase_plotting_area ;
      Plotmm::PlotArea*            _crystal_impedance_vs_frequency_computation_result_plotting_area ;
    } ;
   
  } // namespace Gui
} // namespace Christel

#endif // LIBCRISTEL_GUI_CRYSTAL_STOCK_MANAGEMENT_WIDGETS_REGISTRY_HPP

