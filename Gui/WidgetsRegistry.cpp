/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <WidgetsRegistry.hpp>
#include <iostream>
#include <Path.hpp>
#include "Gui.hpp"

namespace Christel {
  namespace Gui {

    /*****************************************************************************
     *  Initialization: WidgetsRegistry::_builder
     *****************************************************************************/
    Glib::RefPtr<Gtk::Builder> WidgetsRegistry::_builder (nullptr) ;
		
    /****************************************************************************
     *  Constructor WidgetsRegistry::WidgetsRegistry
     ***************************************************************************/
    WidgetsRegistry::WidgetsRegistry() { }

    /****************************************************************************
     *  Method: WidgetsRegistry::getMainWindow
     ***************************************************************************/
    Gtk::Window *WidgetsRegistry::getMainWindow () {
			
      Gtk::Window*                    window = getWidget<Gtk::Window> ("christel_main_window");
      Glib::RefPtr<Gtk::CssProvider>  css = Gtk::CssProvider::create () ;
      css->load_from_data (GUI_STYLE) ;
      window->get_style_context()->add_provider_for_screen (Gdk::Screen::get_default(), 
							    css, 
							    GTK_STYLE_PROVIDER_PRIORITY_APPLICATION) ;
      return window;
    }

    /****************************************************************************
     *  Method:  WidgetsRegistry::_init
     ***************************************************************************/
    void WidgetsRegistry::_init () {
      if (!_builder) {
	WidgetsRegistry::_builder = Gtk::Builder::create_from_string (Glib::ustring (GUI_DEFINITION)) ;
      }
    }
		
    /****************************************************************************
     *  Method:  WidgetsRegistry::_getBuilder
     ***************************************************************************/
    Glib::RefPtr<Gtk::Builder> WidgetsRegistry::_getBuilder() {
      return  WidgetsRegistry::_builder;
    }
  }
}
