/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#ifndef CHRISTEL_GUI_WIDGETS_REGISTRY_HPP
#define CHRISTEL_GUI_WIDGETS_REGISTRY_HPP

#include <string>
#include <iostream>
#include <gtkmm.h>

namespace Christel {
  namespace Gui {

    class Application ;
    class WidgetsRegistry {
      friend Application ;
			
    public:
      WidgetsRegistry();

      virtual Gtk::Window* getMainWindow();

    protected:
      template<typename widgetType = Gtk::Widget>
      widgetType *getWidget(const char *id) {
	widgetType *widget = nullptr ;
	_getBuilder()->get_widget(id, widget) ;
	return widget ;
      }
			
      template<typename objectType = Glib::Object>
      Glib::RefPtr<objectType> getObject (const char *id) {
	Glib::RefPtr<Glib::Object> object = _getBuilder()->get_object (id) ;
	return Glib::RefPtr<objectType>::cast_static (object) ;
      }

    private:
      static Glib::RefPtr<Gtk::Builder> _builder ;
      static void                       _init () ;
      Glib::RefPtr<Gtk::Builder>        _getBuilder () ;
    } ;
		
  } // namespace Gui
} // namespace Christel

#endif // CHRISTEL_GUI_WIDGETS_REGISTRY_HPP


