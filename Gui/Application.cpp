/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/


#include <Application.hpp>

namespace Christel {
  namespace Gui {

    /****************************************************************************
     *  Constructor: Application::Application
     ***************************************************************************/
    Application::Application (int argc, char **argv) {
      _gtk_app     = Gtk::Application::create (argc, argv, "nu.matroos.christel") ;
      init () ;
    }

    /****************************************************************************
     *  Constructor: Application::init
     ***************************************************************************/
    void Application::init () {
      _widgets_registry._init () ;
      _main_window = _widgets_registry.getMainWindow () ;
      _main_controller.init () ;
    }


    /****************************************************************************
     *  Destructor: Application::~Application
     ***************************************************************************/
    Application::~Application() {
      delete _main_window ;
    }

    /****************************************************************************
     *  Method: Application::run
     ***************************************************************************/
    int Application::run () {
      int app_return_value;
      if (_main_window != nullptr) {
        app_return_value = _gtk_app->run (*_main_window) ;
        return 0 ;
      } else {
        return -1 ;
      }
    }

  } // namespace Gui
} // namespace Christel


