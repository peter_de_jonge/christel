/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <MainController.hpp>
#include <iostream>

namespace Christel::Gui {

  /*****************************************************************************
   *  Constructor: MainController::MainController
   *****************************************************************************/
  MainController::MainController () {
  }

  /*****************************************************************************
   *  Method: MainController::init ()
   *****************************************************************************/
  void MainController::init () {
    ApplicationData&                      application_data        = _getApplicationData () ;
    Christel::Lib::ComputeAdapterManager& compute_adapter_manager = application_data.getComputeAdapterManager () ;
    Christel::Lib::ComputeAdapterMeta*    compute_adapter_meta ;
    Christel::Lib::ComputeAdapter*        compute_adapter ;
				
    compute_adapter_manager.active_adapter_changed.connect (sigc::mem_fun (*this, &MainController::_onActiveComputeAdapterChanged)) ;
    compute_adapter_manager.getActiveComputeAdapter (compute_adapter, compute_adapter_meta) ;
    _resolveCurrentComputeEngineStatusMessages (compute_adapter_meta) ;
    _crystal_stock_management_controller.init () ;
  }

  /*****************************************************************************
   *  Method: MainController::_onActiveComputeAdapterChanged
   *****************************************************************************/
  void MainController::_onActiveComputeAdapterChanged (Christel::Lib::ComputeAdapter*     _compute_adapter,
						       Christel::Lib::ComputeAdapterMeta* _compute_adapter_meta) {
   
  }
  
  /*****************************************************************************
   *  Method: MainController::_resolveCurrentComputeEngineStatusMessages
   *****************************************************************************/
  void MainController::_resolveCurrentComputeEngineStatusMessages (Christel::Lib::ComputeAdapterMeta* compute_adapter_meta) {
    Gtk::Label* main_status_current_compute_adapter_label =  _widget_registry.getMainStatusCurrentComputeAdapterLabel () ;
    if (compute_adapter_meta == nullptr) {
      main_status_current_compute_adapter_label->set_label ("None") ;
    } else {
      main_status_current_compute_adapter_label->set_label (compute_adapter_meta->name) ;
    }

  }
  
} // namespace Christel::Gui
