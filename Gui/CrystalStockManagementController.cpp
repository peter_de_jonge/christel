/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#include <TransmissionLossMeasurementResult.hpp>
#include <CrystalStockManagementController.hpp>
#include <CrystalMeasurementController.hpp>
#include <boost/regex.hpp>
#include <Plotmm.hpp>
#include <sstream>
#include <iomanip>
#include <thread>

namespace Christel {
  namespace Gui {

    /*****************************************************************************
     *  Constructor: CrystalStockManagementController::CrystalStockManagementController
     *****************************************************************************/
    CrystalStockManagementController::CrystalStockManagementController () { }

    /************************************************************************
     *  Method: CrystalStockManagementController::init
     ***********************************************************************/
    void CrystalStockManagementController::init () {
		
      _stock_manager                  = &(_getApplicationData ().getStockManager ()) ;
      _stock_list_model_column_record = &(_getApplicationData ().getStockListModelColumnRecord ()) ;
      _connectHandlers () ;
      _stock_manager->readStocks () ;
      _resolveInitialStocksList () ;
      _resolveVNADevicesList () ;
      _resolveVNAPortList () ;
      _widget_registry.init () ;
      _measurement_in_progress = false ;
      _resolveMeasurementActives () ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::onNewStockButtonClicked
     ***********************************************************************/
    void CrystalStockManagementController::onNewStockButtonClicked () {
      Gtk::Dialog*     new_stock_dialog     =                            _widget_registry.getNewStockDialog () ;
      Gtk::Entry*      new_stock_name_entry =                            _widget_registry.getNewStockNameEntry () ;
      Gtk::SpinButton* new_stock_serie_resonance_frequency_spin_button = _widget_registry.getNewStockSeriesResonanceFrequencySpinButton () ;
      double           serie_resonance_frequency ; 
      Glib::ustring    stock_name ;
      int              result ;
      
      if (new_stock_dialog) {
        new_stock_dialog->set_default_response (CrystalStockManagementController::NEW_STOCK_RESPONSE_CANCEL) ;
        result = new_stock_dialog->run () ;
        stock_name = new_stock_name_entry->get_text () ;
        serie_resonance_frequency = new_stock_serie_resonance_frequency_spin_button->get_value () ;
        switch (result) {
        case (CrystalStockManagementController::NEW_STOCK_RESPONSE_CREATE):
          new_stock_dialog->hide () ;
          _newStock (stock_name, serie_resonance_frequency) ;
          break;
        case (CrystalStockManagementController::NEW_STOCK_RESPONSE_CANCEL):
          new_stock_dialog->hide () ;
          break;
        }
      }
      if (stock_name.length() > 0) {
        new_stock_name_entry->delete_text(0, -1);
        new_stock_serie_resonance_frequency_spin_button->set_value (0.0D) ;
      }
    }

    /**********************************************************************
     *  Method: CrystalStockManagementController::_newStock
     **********************************************************************/
    void CrystalStockManagementController::_newStock (Glib::ustring name, double serie_resonance_frequency) {
      Gtk::Dialog*                 no_stock_name_error_dialog ;
      Christel::Lib::CrystalStock*  crystal_stock ;
			
      if (name.length () == 0) {
        no_stock_name_error_dialog = _widget_registry.getNoStockNameErrorDialog () ;
        no_stock_name_error_dialog->set_default_response (CrystalStockManagementController::NO_STOCK_NAME_ERROR_DIALOG_OK) ;
        no_stock_name_error_dialog->run () ;
        no_stock_name_error_dialog->hide () ;
      } else {
        crystal_stock = new Christel::Lib::CrystalStock (name, serie_resonance_frequency) ;
        _stock_manager->addStock (crystal_stock) ;
        _resolveMeasurementActives () ;
      }
    }
		
    /************************************************************************
     *  Method: CrystalStockManagementController::onStockAdded
     ***********************************************************************/
    void CrystalStockManagementController::onStockAdded (Christel::Lib::CrystalStock* stock) {
      Gtk::ComboBox*                stock_list_combobox = _widget_registry.getStockListComboBox () ;
      Glib::RefPtr<Gtk::ListStore>  stock_list_store    = _widget_registry.getCrystalStockListStore () ;
      Gtk::TreeModel::iterator      iter ;
      Gtk::TreeModel::Row           row ;

      if (stock_list_store) {
        iter = stock_list_store->append () ;
        row = *iter ;
        row.set_value (0, stock->getName ()) ;
        stock_list_combobox->set_active (iter) ;
      }
      _setCurrentStock (stock) ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::setCurrentStock
     ***********************************************************************/
    void CrystalStockManagementController::_setCurrentStock (Christel::Lib::CrystalStock *stock) {
      Gtk::Label*   _stock_series_resonance_frequency_label = _widget_registry.getStockSeriesResonanceFrequencyLabel () ;			
      double        _series_resonance_frequency ;
      Glib::ustring _series_resonance_frequency_text ;
      if (_stock_series_resonance_frequency_label) {
        if (_current_stock) {
          _crystal_added_connection.disconnect () ;
        }
        _current_stock                   = stock ;
        _crystal_added_connection        = _current_stock->crystal_added.connect (sigc::mem_fun (*this, &CrystalStockManagementController::onCrystalAddedToStock)) ;
        _series_resonance_frequency      = _current_stock->getSeriesResonanceFrequency () ;
        _series_resonance_frequency_text = Glib::ustring::format (std::fixed, std::setprecision (6), _series_resonance_frequency) ;
        _stock_series_resonance_frequency_label->set_text (_series_resonance_frequency_text) ;
        _resolveCrystalsList (_current_stock) ;
      }
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::_resolveCrystalsList
     ***********************************************************************/
    void CrystalStockManagementController::_resolveCrystalsList (Christel::Lib::CrystalStock *stock) {
      Glib::RefPtr<Gtk::ListStore>              crystal_parameters_list_store = _widget_registry.getCrystalParametersListStore () ;
      std::vector<Christel::Lib::Crystal*>      crystals                      = stock->getCrystals () ;
      Gtk::TreeModel::iterator                  store_iter ;
      Christel::Lib::Crystal*                   crystal ;
      Gtk::TreeModel::Row                       row ;
      double                                    L_m ;
      double                                    C_m ;
      double                                    R_m ;
      double                                    C_p ;

      if (crystal_parameters_list_store) {
        crystal_parameters_list_store->clear () ;
        if (stock) {
          for (auto crystal_iter = crystals.begin () ;
               crystal_iter != crystals.end () ;
               ++crystal_iter) {
            crystal = *crystal_iter ;
	    crystal->computeParameters (Christel::Lib::Crystal::ParameterComputationMethod3DB, L_m, C_m, R_m, C_p) ;
            store_iter = crystal_parameters_list_store->append () ;
            row = *store_iter ;
            row.set_value (0, crystal->getNumber ()) ;
            row.set_value (1, L_m * 1000.0D) ;
            row.set_value (2, C_m * 1000000000000000.0D);
            row.set_value (3, R_m ) ;
            row.set_value (4, C_p * 1000000000000.0D) ;
            row.set_value (5, crystal->getFSerRes ()) ;
            row.set_value (6, crystal->getFParRes ()) ;
          }
        }
      } else {

        //TODO: throw exception/error
      }
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::onStockSelected
     ***********************************************************************/
    void CrystalStockManagementController::onStockSelected () {
      Gtk::ComboBox*           stock_list_combobox = _widget_registry.getStockListComboBox () ;
      Gtk::TreeModel::iterator iter                = stock_list_combobox->get_active () ;
      Gtk::TreeModel::Row      row                 = *iter ;
      Glib::ustring            stock_name ;
      row->get_value (0, stock_name);
      _setCurrentStock (_stock_manager->getStockByName (stock_name)) ;
    }
		
    /************************************************************************
     *  Method: CrystalStockManagementController::onStockSelected
     ***********************************************************************/
    void CrystalStockManagementController::onCrystalAddedToStock (Christel::Lib::CrystalStock* crystal_stock,
								  Christel::Lib::Crystal* crystal) {
      _on_crystal_added_to_stock_dispatcher.emit () ;
    }


    /************************************************************************
     *  Method: CrystalStockManagementController::onStockSelected
     ***********************************************************************/
    void CrystalStockManagementController::onCrystalAddedToStockDispatch () {
      _resolveCrystalsList (_current_stock) ;	
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::_resolveMeasurementActives
     ***********************************************************************/
    void CrystalStockManagementController::_resolveMeasurementActives () {
      bool have_calibration_file = _getApplicationData ().haveCalibrationFilename () ;
      bool have_stocks           = _stock_manager->haveStocks () ;
      bool have_devices          = _haveVNADevices () ;
      bool have_ports            = _haveVNAPorts () ;

      if (_measurement_in_progress) {
	_widget_registry.getMeasurementVNADeviceComboBox ()->set_sensitive (false) ;
        _widget_registry.getVNAPortScanButton ()->set_sensitive (false) ;
        _widget_registry.getMeasurementVNAPortComboBox ()->set_sensitive (false) ;
        _widget_registry.getMeasurementNumberOfAveragingCyclesSpinbutton ()->set_sensitive (false) ;
        _widget_registry.getCalibrationFileEntry ()->set_sensitive (false) ;
        _widget_registry.getCalibrationFileBrowseButton ()->set_sensitive (false) ;
        _widget_registry.getMeasurementFillGapsInIdCheckButton ()->set_sensitive (false) ;
        _widget_registry.getMeasurementStartButton ()->set_sensitive (false) ;
      } else {
	_widget_registry.getMeasurementVNADeviceComboBox ()->set_sensitive (true) ;
        _widget_registry.getVNAPortScanButton ()->set_sensitive (true) ;
        _widget_registry.getMeasurementVNAPortComboBox ()->set_sensitive (true) ;
        _widget_registry.getMeasurementNumberOfAveragingCyclesSpinbutton ()->set_sensitive (true) ;
        _widget_registry.getCalibrationFileEntry ()->set_sensitive (true) ;
        _widget_registry.getCalibrationFileBrowseButton ()->set_sensitive (true) ;
        _widget_registry.getMeasurementFillGapsInIdCheckButton ()->set_sensitive (true) ;
        if (have_devices && have_ports && have_calibration_file && have_stocks) {
          _widget_registry.getMeasurementStartButton ()->set_sensitive (true) ;
        } else {
          _widget_registry.getMeasurementStartButton ()->set_sensitive (false) ;
        }
      }
      _resolveMessages () ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::_resolveMessages
     ***********************************************************************/
    void CrystalStockManagementController::_resolveMessages () {
      bool          have_calibration_file            = _getApplicationData ().haveCalibrationFilename () ;
      Gtk::Label*   messages_label                   = _widget_registry.getMeasurementMessagesLabel () ;
      bool          have_stocks                      = _stock_manager->haveStocks () ;
      bool          have_devices                     = _haveVNADevices () ;
      bool          have_ports                       = _haveVNAPorts () ;

      Glib::ustring messages ;


      
      if (!have_ports || !have_stocks || !have_calibration_file) {
        messages.append ("<span foreground=\"red\" size=\"medium\" allow_breaks=\"true\">To enable measurement perform the following actions:.</span>\r") ;
      }
      if (!have_ports) {
        messages.append ("<span foreground=\"red\" size=\"medium\" allow_breaks=\"true\">   *   Perform a port scan and select the port with the connected miniVNA</span>\r") ;
      }
      if (!have_stocks) {
        messages.append ("<span foreground=\"red\" size=\"medium\" allow_breaks=\"true\">   *   Create a crystal stock</span>\r") ;
      }
      if (!have_calibration_file) {
        messages.append ("<span foreground=\"red\" size=\"medium\" allow_breaks=\"true\">   *   Select an calibration file</span>\r") ;
      }
       
      messages_label->set_label (messages) ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::_resolveActiveVNAAdapter
     ***********************************************************************/
    void CrystalStockManagementController::_resolveActiveVNAAdapter () {
      Gtk::ComboBox*                    vna_device_combobox = _widget_registry.getMeasurementVNADeviceComboBox () ;
      Gtk::ComboBox*                    vna_port_combobox   = _widget_registry.getMeasurementVNAPortComboBox () ;
      Christel::Lib::VNAAdapterManager& vna_adapter_manager = _getApplicationData().getVNAAdapterManager () ;
      Gtk::TreeModel::const_iterator 	active_record       = vna_device_combobox->get_active () ;
      Christel::Lib::VNAAdapterModule*  vna_adapter_module ;
      Christel::Lib::VNAAdapterMeta*    vna_adapter_meta ;
      Christel::Lib::VNAAdapter*        vna_adapter ;
      active_record = vna_device_combobox->get_active () ;
      active_record->get_value (1, vna_adapter_module) ;
      vna_adapter_manager.setActiveVNAAdapterModule (vna_adapter_module) ;
      vna_adapter_manager.getActiveVNAAdapter (vna_adapter, vna_adapter_meta) ;
      _resolveMeasurementNumberOfAveragingCycles () ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::_haveVNADevices
     ***********************************************************************/
    bool CrystalStockManagementController::_haveVNADevices () {
      Glib::RefPtr<Gtk::ListStore> vna_devices_liststore = _widget_registry.getVNADevicesListStore () ;
      if (vna_devices_liststore->children().size() == 0) {
        return false;
      } else {
        return true;
      }
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::_resolveVNADevicesList
     ***********************************************************************/
    void CrystalStockManagementController::_resolveVNADevicesList () {
      Christel::Lib::VNAAdapterManager&                         vna_adapter_manager       =_getApplicationData().getVNAAdapterManager () ;
      std::vector<Christel::Lib::VNAAdapterModule*>             vna_adapter_modules       = vna_adapter_manager.getVNAAdapterModules () ;
      Gtk::ComboBox*                                            vna_devices_list_combobox = _widget_registry.getMeasurementVNADeviceComboBox () ;
      Glib::RefPtr<Gtk::ListStore>                              vna_devices_liststore     = _widget_registry.getVNADevicesListStore () ;
      Christel::Lib::VNAAdapterMeta*                            active_vna_adapter_meta ;
      Christel::Lib::VNAAdapter*                                active_vna_adapter ;
      std::vector<Christel::Lib::VNAAdapterModule*>::iterator   module_iter ;      
      Gtk::TreeModel::iterator                                  store_iter ;
      Gtk::TreeModel::Row                                       row ;

      vna_adapter_manager.getActiveVNAAdapter (active_vna_adapter, active_vna_adapter_meta) ;
      if (vna_devices_liststore) {
        vna_devices_liststore->clear () ;
      	for (module_iter = vna_adapter_modules.begin () ;
      	     module_iter != vna_adapter_modules.end () ;
      	     ++module_iter) {
	  std::cout << (*module_iter)->getVNAAdapterMeta ()->name ;
      	  store_iter = vna_devices_liststore->append () ;
      	  row = *store_iter ;
      	  row.set_value (0, (*module_iter)->getVNAAdapterMeta ()->name) ;
      	  row.set_value (1, (*module_iter)) ;
	  if  ((*module_iter)->getVNAAdapterMeta () == active_vna_adapter_meta) {
	    vna_devices_list_combobox->set_active (store_iter) ;
      	  }
      	}
      }
      if ((active_vna_adapter_meta == nullptr) &&
	  (vna_devices_liststore->children ().size () != 0)) {
	vna_adapter_manager.setActiveVNAAdapterModule (*vna_adapter_modules.begin ()) ;
	vna_devices_list_combobox->set_active (0) ;
	
      }
    }
    
    /************************************************************************
     *  Method: CrystalStockManagementController::_haveVNAPorts
     ***********************************************************************/
    bool CrystalStockManagementController::_haveVNAPorts () {
      Glib::RefPtr<Gtk::ListStore> vna_ports_liststore = _widget_registry.getVNAPortsListStore () ;
      if (vna_ports_liststore->children().size() == 0) {
        return false;
      } else {
        return true;
      }
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::_resolveVNAPortList
     ***********************************************************************/
    void CrystalStockManagementController::_resolveVNAPortList () {
      Glib::RefPtr<Gtk::ListStore> vna_ports_liststore    = _widget_registry.getVNAPortsListStore () ;
      Gtk::ComboBox*               vna_port_list_combobox = _widget_registry.getMeasurementVNAPortComboBox () ;
      boost::filesystem::path      device_path            = boost::filesystem::path ("/dev") ;
      Gtk::TreeModel::iterator     store_iter ;
      Gtk::TreeModel::Row          row ;
      if (vna_ports_liststore) {
        vna_ports_liststore->clear () ;
        try {
          boost::filesystem::file_status s = status (device_path);
          if (is_directory (s)) {
            for (auto it = boost::filesystem::directory_iterator (device_path) ;
                 it != boost::filesystem::directory_iterator () ;
                 ++it) {
              boost::regex expr ("/dev/ttyUSB.*") ;
              if (boost::regex_match (it->path ().native (), expr)) {
                store_iter = vna_ports_liststore->append () ;
                row = *store_iter ;
                row.set_value (0, it->path ().filename ().string ()) ;
              }
            }
            if (vna_ports_liststore->children ().size () != 0) {
              vna_port_list_combobox->set_active (0) ;
            }

          }
        } catch (boost::filesystem::filesystem_error &e) {
          std::cerr << e.what() << '\n' ;
        }
        _resolveMeasurementActives () ;
      }
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::onVNADeviceSelected
     ***********************************************************************/
    void CrystalStockManagementController::onVNADeviceSelected () {
      _resolveActiveVNAAdapter () ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::onVNAPortSelected
     ***********************************************************************/
    void CrystalStockManagementController::onVNAPortSelected () {
      _resolveActiveVNAAdapter () ;
      Gtk::ComboBox*                    vna_port_combobox   = _widget_registry.getMeasurementVNAPortComboBox () ;
      Christel::Lib::VNAAdapterManager& vna_adapter_manager = _getApplicationData().getVNAAdapterManager () ;
      Gtk::TreeModel::const_iterator 	active_record       = vna_port_combobox->get_active () ;
      Christel::Lib::VNAAdapterMeta*    active_vna_adapter_meta ;
      Christel::Lib::VNAAdapter*        active_vna_adapter ;
      std::string                       vna_usb_port_path ;
      vna_adapter_manager.getActiveVNAAdapter (active_vna_adapter, active_vna_adapter_meta) ;
      active_record = vna_port_combobox->get_active () ;
      active_record->get_value (0, vna_usb_port_path) ;
      active_vna_adapter->setUSBPortPath (vna_usb_port_path) ;
    }
    
    /************************************************************************
     *  Method: CrystalStockManagementController::onVNAPortScanButtonClicked
     ***********************************************************************/
    void CrystalStockManagementController::onVNAPortScanButtonClicked () {
      _resolveVNAPortList ();
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::onCalibrationFileBrowseButtonClicked
     ***********************************************************************/
    void CrystalStockManagementController::onCalibrationFileBrowseButtonClicked () {
      Gtk::FileChooserDialog* dialog = _widget_registry.getSelectCalibrationFileDialog () ;
      int                     result ;
      std::string             calibration_filename ;

      if (dialog) {
        dialog->set_default_response (Gtk::RESPONSE_CANCEL);
        result = dialog->run () ;
        calibration_filename = dialog->get_filename () ;
        dialog->hide () ;
        if (result == Gtk::RESPONSE_ACCEPT) {
          _getApplicationData ().setCalibrationFilename (calibration_filename) ;
        }
      }
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::onCalibrationFilenameChanged
     ***********************************************************************/
    void CrystalStockManagementController::onCalibrationFilenameChanged (std::string& calibration_filename) {
      Christel::Lib::VNAAdapterManager& vna_adapter_manager = _getApplicationData().getVNAAdapterManager () ;
      Gtk::Entry*                       entry               = _widget_registry.getCalibrationFileEntry () ;
      Christel::Lib::VNAAdapterMeta*    active_vna_adapter_meta ;
      Christel::Lib::VNAAdapter*        active_vna_adapter ;
      std::string                       vna_usb_port_path ;
      vna_adapter_manager.getActiveVNAAdapter (active_vna_adapter, active_vna_adapter_meta) ;
      active_vna_adapter->setCalibrationFile (calibration_filename) ;
      entry->set_text (calibration_filename) ;
      _resolveMeasurementActives () ;
    }
    
    /************************************************************************
     *  Method: CrystalStockManagementController::onMeasurementNumberOfAveragingCyclesChanged
     ***********************************************************************/
    void CrystalStockManagementController::onMeasurementNumberOfAveragingCyclesChanged () {
      _resolveMeasurementNumberOfAveragingCycles () ;
    }
    
    /*************************************************************************
     * Method: CrystalStockManagementController::_resolveMeasurementNumberOfAveragingCycles
     ************************************************************************/
    void CrystalStockManagementController::_resolveMeasurementNumberOfAveragingCycles () {
      Christel::Lib::VNAAdapterManager& vna_adapter_manager = _getApplicationData().getVNAAdapterManager () ;
      Gtk::SpinButton*                  spinbutton          = _widget_registry.getMeasurementNumberOfAveragingCyclesSpinbutton () ;
      unsigned int                      n_averaging_cycles  = spinbutton->get_value_as_int () ;
      Christel::Lib::VNAAdapterMeta*    active_vna_adapter_meta ;
      Christel::Lib::VNAAdapter*        active_vna_adapter ;
      
      vna_adapter_manager.getActiveVNAAdapter (active_vna_adapter, active_vna_adapter_meta) ;
      active_vna_adapter->setNAveragingCycles (n_averaging_cycles) ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::_startMeasurement
     ***********************************************************************/
    void CrystalStockManagementController::_startMeasurement () {
      Gtk::SpinButton*                             _measurement_fixture_impedance_spin_button  = _widget_registry.getMeasurementFixtureImpedanceSpinbutton () ;
      Gtk::ComboBox*                               _vna_port_list_combobox                     = _widget_registry.getMeasurementVNAPortComboBox () ;
      Gtk::Label*                                  _measurement_status_label                   = _widget_registry.getMeasurementStatusLabel () ;
      Gtk::Spinner*                                _measurement_status_spinner                 = _widget_registry.getMeasurementStatusSpinner () ;
      double                                       _approximate_series_resonance_frequency ;
      Christel::Lib::CrystalMeasurementController* _crystal_measurement_controller ;
      double                                       _fixture_system_impedance  ;
      std::string                                  _vna_calibration_filename ;
      unsigned int                                 _n_averaging_cycles ;
      Gtk::TreeModel::iterator                     _vna_port_list_iter ;
      Gtk::TreeModel::Row                          _vna_port_list_row ; 
      std::string                                  _vna_port_name ;

      auto                                         _doMeasure = [] (Christel::Lib::CrystalMeasurementController* crystal_measurement_controller,
								    bool                                         fill_gaps_of_ids_in_stock,
								    Glib::Dispatcher*                            measurement_completed_dispatcher) {
								  crystal_measurement_controller->doMeasure (fill_gaps_of_ids_in_stock) ;
								  measurement_completed_dispatcher->emit () ;
								} ;
      _measurement_in_progress = true ;
      _resolveMeasurementActives () ;
      if (_current_stock) {
	_fixture_system_impedance               = _measurement_fixture_impedance_spin_button->get_value () ;
	/*_vna_calibration_filename               = _getApplicationData ().getCalibrationFilename () ;
	_vna_port_list_iter                     = _vna_port_list_combobox->get_active () ;
	_vna_port_list_row                      = *_vna_port_list_iter ;
	_vna_port_list_row->get_value (0, _vna_port_name) ;
	*/
				

	_crystal_measurement_controller = new Christel::Lib::CrystalMeasurementController (_current_stock,
											   _fixture_system_impedance) ;
                                                                                           
        
	  //_measurement_status_label->set_attributes (
	_measurement_status_label->set_label ("<span foreground=\"green\" size=\"x-large\">Measurement running...</span>") ;
	_measurement_status_spinner->start () ;
        
	new std::thread (_doMeasure,
			 _crystal_measurement_controller,
			 _fill_gaps_of_ids_in_stock,
			 &_on_measurement_completed_dispatcher) ;
      }
    }

    /*************************************************************************
     *  Method: CrystalStockManagementController::onMeasurementCompletedDispatch
     ************************************************************************/
    void CrystalStockManagementController::onMeasurementCompletedDispatch () {
      Gtk::Label*   _measurement_status_label    = _widget_registry.getMeasurementStatusLabel () ;
      Gtk::Spinner* _measurement_status_spinner  = _widget_registry.getMeasurementStatusSpinner () ;

      _measurement_status_spinner->stop () ;
      _measurement_status_label->set_label ("<span foreground=\"green\" size=\"x-large\">Measurement completed.</span>") ;
      _measurement_in_progress = false ;
      _resolveMeasurementActives () ;
    }
    
    /*************************************************************************
     * Method: CrystalStockManagementController::_plotTransmisionResult () ;
     ************************************************************************/
    void CrystalStockManagementController::_plotTransmisionResult () {
      Plotmm::PlotArea*                                 measured_transmission_phase_plotting_area = _widget_registry.getMeasuredTransmissionPhasePlotArea () ;
      Plotmm::PlotArea*                                 measured_transmission_loss_plotting_area  = _widget_registry.getMeasuredTransmissionLossPlotArea () ;
      Christel::Lib::TransmissionLossMeasurementResult* _transmission_loss_measurement_result ;
      Christel::Lib::CrystalMeasurementResult*          _crystal_measurement_result ;
      Plotmm::DataSet*                                  _measured_loss_data_set ;
      Plotmm::PeakSet*                                  _measured_loss_peak_set  ;
      Plotmm::LinearAxis*                               _measured_loss_x_axis ;
      Plotmm::LinearAxis*                               _measured_loss_y_axis ;
      Plotmm::D2LinePlot*                               _measured_loss_plot ;
      Plotmm::DataSet*                                  _measured_phase_data_set ;
      Plotmm::LinearAxis*                               _measured_phase_x_axis ;
      Plotmm::LinearAxis*                               _measured_phase_y_axis ;
      Plotmm::D2LinePlot*                               _measured_phase_plot ;
      Plotmm::DataSet*                                  _computed_loss_data_set ;
      Plotmm::PeakSet*                                  _computed_loss_peak_set ;
      Plotmm::LinearAxis*                               _computed_loss_x_axis ;
      Plotmm::LinearAxis*                               _computed_loss_y_axis ;
      Plotmm::D2LinePlot*                               _computed_loss_plot ;
      double*                                           _measured_frequencies ;
      double*                                           computed_frequencies ;
      double*                                           _measured_series_resistance ;
      double*                                           _measured_losses ;
      double*                                           _measured_phases ;
      double*                                           computed_losses ;
      //    double*                                      computed_phases ;

      unsigned int*                                     _i_res ;
      double*                                           _f_res ;
      double*                                           _loss_res ;
      double*                                           _rs_res ;
      unsigned int                                      _f_start ;
      unsigned int                                      _f_stop ;
      unsigned int                                      _f_steps ;
      double                                            _x_tics_start ;
      double                                            _x_tics_stop ;
      double                                            _x_tics_increase ;
      int                                               _x_tics_n_decimals ;
      int                                               _n_peaks ;
      int                                               _n_samples ;
     

			
      if (_current_crystal) {
	_crystal_measurement_result           = _current_crystal->getCrystalMeasurementResult () ;
	_transmission_loss_measurement_result = _crystal_measurement_result->getTransmissionLossMeasurementResult () ;
	_n_peaks   = _crystal_measurement_result->getSeriesResonancePoints (_i_res, _f_res, _loss_res, _rs_res) ;
	_n_samples = _transmission_loss_measurement_result->getData (_measured_frequencies, _measured_losses, _measured_phases, _measured_series_resistance) ;
	_transmission_loss_measurement_result->getFrequencyRange(_f_start, _f_stop, _f_steps) ;
	_resolve_x_tics (_f_start, _f_stop, _f_steps, _x_tics_start, _x_tics_stop, _x_tics_increase, _x_tics_n_decimals) ;
	
	
	if (_transmission_loss_measurement_result) {
	  
	  // Measured transmissionloss plot
	  if (measured_transmission_loss_plotting_area) {
	    _measured_loss_data_set  = new Plotmm::DataSet (_measured_frequencies, _measured_losses, _n_samples) ;
	    _measured_loss_peak_set  = new Plotmm::PeakSet (_f_res, _loss_res, _n_peaks) ;
	    _measured_loss_x_axis    = new Plotmm::LinearAxis (Plotmm::AXIS_DIR_X,
							       _x_tics_start,
							       _x_tics_increase,
							       _x_tics_stop,
							       1000000.0D,
							       _x_tics_n_decimals,
							       4) ;
	    _measured_loss_y_axis = new Plotmm::LinearAxis (Plotmm::AXIS_DIR_Y,
							    -120.0D,
							    10.0D,
							    0.0D,
							    1.0D,
							    0,
							    3) ;
	    _measured_loss_plot      = new Plotmm::D2LinePlot (_measured_loss_x_axis,
							       _measured_loss_y_axis,
							       _measured_loss_data_set,
							       _measured_loss_peak_set) ;
	    _measured_loss_data_set->set_color (0.0D, 0.0D, 1.0D) ;
	    _measured_loss_peak_set->set_color (1.0D, 0.0D, 0.0D) ;
	    
#ifdef DEBUG_MODE
	    // BEGIN: DEBUGGING
	    // Computed transmissionloss plot
	    computed_frequencies = _measured_frequencies ; // FOR TESTING, TODO: REMOVE THIS
	    _current_crystal->computeTransmissionLoss (Christel::Lib::Crystal::ParameterComputationMethod3DB,
						       computed_losses,
						       computed_frequencies)  ;
	    
	    if (computed_losses) {
	      _computed_loss_data_set  = new Plotmm::DataSet (computed_frequencies, computed_losses, _n_samples) ;
	      _computed_loss_data_set->set_color (1.0D, 0.0D, 1.0D) ;
	      _measured_loss_plot->addDataSet (_computed_loss_data_set) ;
	    }
	
	    // END: DEBUGGING
#endif
	    
	    measured_transmission_loss_plotting_area->set_plot (_measured_loss_plot) ;
	  }

	  // Measured transmissionphase plot
	  if (measured_transmission_phase_plotting_area) {

	    _measured_phase_data_set  = new Plotmm::DataSet (_measured_frequencies, _measured_phases, _n_samples) ;
	    _measured_phase_x_axis    = new Plotmm::LinearAxis (Plotmm::AXIS_DIR_X,
								_x_tics_start,
								_x_tics_increase,
								_x_tics_stop,
								1000000.0D,
								_x_tics_n_decimals,
								4) ;
	    _measured_phase_y_axis = new Plotmm::LinearAxis (Plotmm::AXIS_DIR_Y,
							     -180.0D,
							     45.0D,
							     180.0D,
							     1.0D,
							     0,
							     4) ;
	    _measured_phase_plot = new Plotmm::D2LinePlot (_measured_phase_x_axis,
							   _measured_phase_y_axis,
							   _measured_phase_data_set,
							   (Plotmm::PeakSet*) NULL) ;
	    _measured_phase_data_set->set_color (0.0D, 0.0D, 1.0D) ;
	    
	    measured_transmission_phase_plotting_area->set_plot (_measured_phase_plot) ;	  
	  }
	}
      }
    }
    
    /*************************************************************************
     * Method: CrystalStockManagementController::_plot
     ************************************************************************/
    void CrystalStockManagementController::_plot () {
      _plotTransmisionResult () ;
    }

    /*************************************************************************
     * Method: CrystalStockManagementController::_resolve_x_tics
     ************************************************************************/
    void CrystalStockManagementController::_resolve_x_tics (double  f_start,
							    double  f_stop,
							    double  f_steps,
							    double& x_tics_start,
							    double& x_tics_stop,
							    double& x_tics_increase,
							    int&    x_tics_n_decimals) {
      unsigned int _diff           = (unsigned int) floor (f_stop - f_start) ;
      unsigned int _diff_exponent  = floor (log10 (_diff)) ;
      unsigned int _diff_base      = (unsigned int) round (pow (10.0D, _diff_exponent)) ;
      unsigned int _diff_remainder = fmod (_diff, _diff_base) ;
				
      x_tics_start      = f_start + _diff_remainder / 2 ;
      x_tics_stop       = f_stop - _diff_remainder / 2 ;
      x_tics_increase   = (x_tics_stop - x_tics_start) / 10 ;
      x_tics_n_decimals = _diff_exponent - 1 ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::onMeasurementStartButtonClicked
     ***********************************************************************/
    void CrystalStockManagementController::onMeasurementStartButtonClicked () {
      _startMeasurement () ;
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::onMeasurementFillGapsInIdCheckButtonClicked
     ***********************************************************************/
    void CrystalStockManagementController::onMeasurementFillGapsInIdCheckButtonClicked () {
      Gtk::CheckButton* checkbutton = _widget_registry.getMeasurementFillGapsInIdCheckButton () ;
      if (checkbutton) {
	_fill_gaps_of_ids_in_stock = checkbutton->get_active () ;
      }
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::onXtalParametersTreeviewRowClicked
     ***********************************************************************/
    void CrystalStockManagementController::onXtalParametersTreeviewRowClicked (const Gtk::TreeModel::Path&  path,
									       Gtk::TreeViewColumn*         column) {

      Glib::RefPtr<Gtk::ListStore> crystal_parameters_list_store = _widget_registry.getCrystalParametersListStore () ;
      Gtk::TreeModel::iterator     iterator                      = crystal_parameters_list_store->get_iter (path) ;
      Gtk::TreeModel::Row          row                           = *iterator ;
      guint                        crystal_number ;
      Christel::Lib::Crystal*      current_crystal ;
			
      row.get_value (0, crystal_number) ;
      current_crystal = _current_stock->getCrystalByNumber (crystal_number) ;
      if (current_crystal) {
	_current_crystal = current_crystal ;
	_plot () ;
      }

    }

    /************************************************************************
     *  Method: CrystalStockManagementController::onXtalParametersTreeviewCursorChanged
     ***********************************************************************/
    void CrystalStockManagementController::onXtalParametersTreeviewCursorChanged () {
      Gtk::TreeView*        treeview = _widget_registry.getXtalParametersTreeview () ;
      Gtk::TreeViewColumn*  focus_column ;
      Gtk::TreeModel::Path  path ;
			
      if (treeview) {
	treeview->get_cursor (path, focus_column) ;
	treeview->row_activated (path, *focus_column) ;
      }
    }

    /************************************************************************
     *  Method: CrystalStockManagementController::_resolveInitialStocksList
     ***********************************************************************/
    void CrystalStockManagementController::_resolveInitialStocksList () {
      std::vector<Christel::Lib::CrystalStock *>  stocks = _stock_manager->getStocks () ;
      Gtk::ComboBox*               stock_list_combobox = _widget_registry.getStockListComboBox () ;
      Glib::RefPtr<Gtk::ListStore> stock_list_store    = _widget_registry.getCrystalStockListStore () ;
      Gtk::TreeModel::iterator     store_iter ;
      Christel::Lib::CrystalStock* stock ;
      Gtk::TreeModel::Row          row ;

      if (stock_list_store) {
	stock_list_store->clear () ;
	for (auto stock_iter = stocks.begin () ;
	     stock_iter != stocks.end () ;
	     ++stock_iter) {
	  stock = *stock_iter ;
	  store_iter = stock_list_store->append () ;
	  row = *store_iter ;
	  row.set_value (0, stock->getName ()) ;
	  stock_list_combobox->set_active (0) ;
	  _setCurrentStock (*(stocks.begin ())) ;
	}
      } else {

	//TODO: throw exception/error
      }
    }
		
    /************************************************************************
     *  Method: CrystalStockManagementController::connectHandlers
     ***********************************************************************/
    void CrystalStockManagementController::_connectHandlers () {
      Gtk::CheckButton* checkbutton ;
      Gtk::SpinButton*  spinbutton ;
      Gtk::ComboBox*    combobox ;
      Gtk::TreeView*    treeview ;
      Gtk::Button*      button ;

      _on_measurement_completed_dispatcher.connect (sigc::mem_fun (*this, &CrystalStockManagementController::onMeasurementCompletedDispatch)) ;
      
      _on_crystal_added_to_stock_dispatcher.connect (sigc::mem_fun (*this, &CrystalStockManagementController::onCrystalAddedToStockDispatch)) ;
			
      _stock_manager->stock_added.connect (sigc::mem_fun (*this, &CrystalStockManagementController::onStockAdded)) ;

      _getApplicationData ().calibration_filename_changed.connect (sigc::mem_fun (*this, &CrystalStockManagementController::onCalibrationFilenameChanged)) ;

      button = _widget_registry.getNewStockButtonWidget () ;
      if (button) {
	button->signal_clicked ().connect (sigc::mem_fun (*this, &CrystalStockManagementController::onNewStockButtonClicked)) ;
      }

      combobox = _widget_registry.getMeasurementVNADeviceComboBox () ;
      if (combobox) {
	combobox->signal_changed ().connect (sigc::mem_fun (*this, &CrystalStockManagementController::onVNADeviceSelected)) ;
      }

      combobox = _widget_registry.getMeasurementVNAPortComboBox () ;
      if (combobox) {
	combobox->signal_changed ().connect (sigc::mem_fun (*this, &CrystalStockManagementController::onVNAPortSelected)) ;
      }
      
      button = _widget_registry.getVNAPortScanButton () ;
      if (button) {
	button->signal_clicked ().connect (sigc::mem_fun (*this, &CrystalStockManagementController::onVNAPortScanButtonClicked)) ;
      }

      button = _widget_registry.getCalibrationFileBrowseButton () ;
      if (button) {
	button->signal_clicked ().connect (sigc::mem_fun(*this, &CrystalStockManagementController::onCalibrationFileBrowseButtonClicked)) ;
      }

      spinbutton = _widget_registry.getMeasurementNumberOfAveragingCyclesSpinbutton () ;
      if (spinbutton) {
	spinbutton->signal_changed ().connect (sigc::mem_fun (*this, &CrystalStockManagementController::onMeasurementNumberOfAveragingCyclesChanged)) ;
      }
      
      button = _widget_registry.getMeasurementStartButton () ;
      if (button) {
	button->signal_clicked ().connect (sigc::mem_fun (*this, &CrystalStockManagementController::onMeasurementStartButtonClicked)) ;
      }

      checkbutton = _widget_registry.getMeasurementFillGapsInIdCheckButton () ;
      if (checkbutton) {
	checkbutton->signal_clicked ().connect (sigc::mem_fun (*this, &CrystalStockManagementController::onMeasurementFillGapsInIdCheckButtonClicked)) ;
      }
      
      treeview = _widget_registry.getXtalParametersTreeview () ;
      if (treeview) {
	treeview->signal_row_activated ().connect (sigc::mem_fun (*this, &CrystalStockManagementController::onXtalParametersTreeviewRowClicked)) ;
	treeview->signal_cursor_changed ().connect (sigc::mem_fun (*this, &CrystalStockManagementController::onXtalParametersTreeviewCursorChanged)) ;
      }
			
    }

  } // namespace Gui
} // namespace Cristel
