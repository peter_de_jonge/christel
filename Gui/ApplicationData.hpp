/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef CHRISTEL_GUI_APPLICATION_DATA_HPP
#define CHRISTEL_GUI_APPLICATION_DATA_HPP

#include <StockListModelColumnRecord.hpp>
#include <ComputeAdapterManager.hpp>
#include <CrystalStockManager.hpp>
#include <VNAAdapterManager.hpp>
#include <Project.hpp>
#include <gtkmm.h>
#include <string>

namespace Christel {
  namespace Gui {

    class ApplicationData {

    public:

      enum class MeasurementStatus {
        idle,
        crystal
      } ;
      
      ApplicationData () ;
      Christel::Lib::ComputeAdapterManager& getComputeAdapterManager () ;
      Christel::Lib::VNAAdapterManager&     getVNAAdapterManager () ;
      Christel::Lib::CrystalStockManager&   getStockManager () ;
      StockListModelColumnRecord&           getStockListModelColumnRecord () ;
      void                                  setCalibrationFilename (std::string calibration_filename) ;
      std::string                           getCalibrationFilename () ;
      bool                                  haveCalibrationFilename () ;

      /**
       * Signal that notifies when the calibration_filename
       *
       * @param Crystal*
       * @return void
       */
      sigc::signal<void, std::string &> calibration_filename_changed;

    private:
      Christel::Lib::Project*               _project ;
      Christel::Lib::CrystalStockManager    _stock_manager ;
      Christel::Lib::ComputeAdapterManager* _compute_adapter_manager ;
      Christel::Lib::VNAAdapterManager*     _vna_adapter_manager ;
      StockListModelColumnRecord            _stock_list_model_column_record ;
      std::string                           _calibration_filename ;
    } ;

  } // namespace Gui
} // namespace Christel


#endif // CHRISTEL_GUI_APPLICATION_DATA_HPP
