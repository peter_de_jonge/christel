/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef CHRISTEL_GUI_MAIN_CONTROLLER_HPP
#define CHRISTEL_GUI_MAIN_CONTROLLER_HPP

#include <CrystalStockManagementController.hpp>
#include <MainWidgetsRegistry.hpp>
#include <ComputeAdapter.hpp>
#include <Controller.hpp>

namespace Christel::Gui {

  class MainController : public Controller {
  public:
    MainController () ;
    void init () ;
  private:
    CrystalStockManagementController _crystal_stock_management_controller ;
    MainWidgetsRegistry              _widget_registry ;

    void _onActiveComputeAdapterChanged (Christel::Lib::ComputeAdapter*     _compute_adapter,
					 Christel::Lib::ComputeAdapterMeta* _compute_adapter_meta) ;
    void _resolveCurrentComputeEngineStatusMessages (Christel::Lib::ComputeAdapterMeta* compute_adapter_meta) ;
  } ;
    
} // namespace Christel::Gui

#endif // CHRISTEL_GUI_MAIN_CONTROLLER_HPP
