/*****************************************************************************
 *                                                                           *
 *   Christel: a program for crystal filter design                           *
 *   Copyright (C) 2022 Peter de Jonge <pa1pdy@kpnmail.nl                    *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *                                                                           *
 *   GNU General Public License for more details.                            *
 *   You should have received a copy of the GNU General Public License along *
 *   with this program; if not, write to the Free Software Foundation, Inc., *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/

#ifndef CHRISTEL_GUI_STOCK_LIST_MODEL_COLUMN_RECORD_HPP
#define CHRISTEL_GUI_STOCK_LIST_MODEL_COLUMN_RECORD_HPP

#include <gtkmm.h>
#include <WidgetsRegistry.hpp>

namespace Christel {
    namespace Gui {

        class StockListModelColumnRecord : public Gtk::TreeModelColumnRecord {
        public:

            StockListModelColumnRecord() {
                add(m_col_text);
                add(m_col_number);
            }

            Gtk::TreeModelColumn<Glib::ustring> m_col_text;
            Gtk::TreeModelColumn<int> m_col_number;
        };

    } // namespace Gui
} // namespace Christel


#endif // CHRISTEL_GUI_STOCK_LIST_MODEL_COLUMN_RECORD_HPP
